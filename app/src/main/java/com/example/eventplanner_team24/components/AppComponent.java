package com.example.eventplanner_team24.components;

import com.example.eventplanner_team24.authentication.Presentation.RegistrationFragment;
import com.example.eventplanner_team24.authentication.Presentation.Registration_pup_v_fragment;
import com.example.eventplanner_team24.category_manager.Presentation.CategoryManagerFragment;
import com.example.eventplanner_team24.category_manager.Presentation.FragmentCreateCategory;
import com.example.eventplanner_team24.category_manager.Presentation.FragmentCreateSubCategory;
import com.example.eventplanner_team24.category_manager.Presentation.adapters.CategoryAdapter;
import com.example.eventplanner_team24.category_manager.Presentation.adapters.SubCategoryAdapter;
import com.example.eventplanner_team24.event_manager.Presentation.AddEventTypeFragment;
import com.example.eventplanner_team24.event_manager.Presentation.EventTypeManager;
import com.example.eventplanner_team24.event_manager.Presentation.adapters.EventTypeAdapter;
import com.example.eventplanner_team24.modules.RepositoryModule;
import com.example.eventplanner_team24.modules.ServiceModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {RepositoryModule.class, ServiceModule.class})
public interface AppComponent {
    void inject(Registration_pup_v_fragment fragment);
    void inject(RegistrationFragment fragment);
    void inject(FragmentCreateCategory fragment);
    void inject(FragmentCreateSubCategory fragment);
    void inject(CategoryManagerFragment fragment);
    void inject(CategoryAdapter adapter);
    void inject(SubCategoryAdapter adapter);
    void inject(AddEventTypeFragment fragment);
    void inject(EventTypeManager fragment);
    void inject(EventTypeAdapter adapter);
}
