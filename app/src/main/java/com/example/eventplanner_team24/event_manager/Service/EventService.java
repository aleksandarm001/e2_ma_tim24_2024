package com.example.eventplanner_team24.event_manager.Service;
import com.example.eventplanner_team24.event_manager.Data.EventCallback;
import com.example.eventplanner_team24.event_manager.Data.IEventRepository;
import com.example.eventplanner_team24.event_manager.Service.IServices.IEventService;
import com.example.eventplanner_team24.model.EventType;

import javax.inject.Inject;

public class EventService implements IEventService{
    private final IEventRepository eventRepository;

    @Inject
    public EventService(IEventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public void getEvents(EventCallback callback) {
        eventRepository.getEvents(callback);
    }

    @Override
    public boolean saveEvent(EventType eventType) {
        return eventRepository.saveEvent(eventType);
    }

    @Override
    public void deleteEvent(EventType eventType) {

    }

    @Override
    public void updateEvent(EventType eventType) {
        eventRepository.updateEvent(eventType);
    }
}
