package com.example.eventplanner_team24.event_manager.Presentation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentEventTypeListBinding;
import com.example.eventplanner_team24.event_manager.Presentation.adapters.EventTypeAdapter;
import com.example.eventplanner_team24.model.EventType;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventTypeListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventTypeListFragment extends Fragment {

    private static final String ARG_PARAM = "param";

    private ArrayList<EventType> mEventTypeTypes;
    private FragmentEventTypeListBinding binding;
    private EventTypeAdapter adapter;

    public EventTypeListFragment() {
        // Required empty public constructor
    }

    public static EventTypeListFragment newInstance(ArrayList<EventType> eventTypeTypes) {
        EventTypeListFragment fragment = new EventTypeListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, eventTypeTypes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("EvenetApp", "onCreate Products List Fragment");
        if (getArguments() != null) {
            mEventTypeTypes = getArguments().getParcelableArrayList(ARG_PARAM);
            adapter = new EventTypeAdapter(getActivity(), mEventTypeTypes);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = binding.inflate(inflater,container,false);
        View root = binding.getRoot();
        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        binding.addEventButton.setOnClickListener(v->{
            FragmentTransition.to(AddEventTypeFragment.newInstance(null), getActivity(), true, R.id.event_manager_container);
        });

        return root;
    }
}