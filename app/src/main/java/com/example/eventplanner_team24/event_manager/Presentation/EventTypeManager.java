package com.example.eventplanner_team24.event_manager.Presentation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.databinding.FragmentEventManagerBinding;
import com.example.eventplanner_team24.event_manager.Data.EventCallback;
import com.example.eventplanner_team24.event_manager.Service.EventService;
import com.example.eventplanner_team24.model.EventType;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventTypeManager#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventTypeManager extends Fragment implements EventCallback {

    private FragmentEventManagerBinding binding;
    private ArrayList<EventType> eventTypeTypes;
    @Inject
    EventService eventService;
    public EventTypeManager() {
        // Required empty public constructor
    }

    public static EventTypeManager newInstance() {
        EventTypeManager fragment = new EventTypeManager();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventManagerBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        DaggerAppComponent.create().inject(this);
        eventService.getEvents(this);
        return root;
    }

    @Override
    public void onEventsLoaded(List<EventType> eventTypeTypes) {
        this.eventTypeTypes = (ArrayList<EventType>) eventTypeTypes;
        FragmentTransition.to(EventTypeListFragment.newInstance(this.eventTypeTypes), getActivity(), false, R.id.event_manager_container);
    }
    @Override
    public void onError(Exception e) {

    }
}