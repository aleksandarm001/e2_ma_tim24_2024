package com.example.eventplanner_team24.event_manager.Presentation;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.SubCategoryCallback;
import com.example.eventplanner_team24.category_manager.Presentation.CategoryManagerFragment;
import com.example.eventplanner_team24.category_manager.Service.SubCategoryService;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.databinding.FragmentAddEventBinding;
import com.example.eventplanner_team24.event_manager.Service.EventService;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEventTypeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEventTypeFragment extends Fragment implements SubCategoryCallback {

    private static final String ARG_PARAM = "param";
    private EventType eventType;
    private FragmentAddEventBinding binding;
    @Inject
    EventService eventService;
    @Inject
    SubCategoryService subCategoryService;

    MaterialCardView selectCard;
    TextView serviceSubCategoryTextView;
    Map<String, SubCategory> subCategories = new HashMap<String, SubCategory>();
    ArrayList<SubCategory> selectedSubCategories_Category;
    boolean [] selectedSubCategories;
    ArrayList<Integer> subCategoriesList = new ArrayList<>();
    String [] subCategoiesAray = new ArrayList<String>().toArray(new String[0]);


    public AddEventTypeFragment() {
        // Required empty public constructor
    }

    public static AddEventTypeFragment newInstance(EventType eventType) {
        AddEventTypeFragment fragment = new AddEventTypeFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, eventType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventType = getArguments().getParcelable(ARG_PARAM);
        }
    }

    private int getIndex(SubCategory sc) {
        for(int i = 0; i < subCategoiesAray.length; i++){
            if(subCategoiesAray[i].equals(sc.getName())){
                subCategoriesList.add(i);
                return i;
            }
        }
        return -1;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAddEventBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        Initialization();


        EditText nametextView = binding.eventNameEditText;
        EditText description = binding.eventDescriptionEditText;

        if(eventType != null){
            nametextView.setText(eventType.getName());
            description.setText(eventType.getDescription());
        }


        selectCard.setOnClickListener(v -> {
            showSubCategoriesDialog();
        });

        binding.addEventTypeButton.setOnClickListener(v -> {
            EventType newEventTypeType = new EventType();
            if(eventType != null){
                newEventTypeType.setName(nametextView.getText().toString());
                newEventTypeType.setDescription(description.getText().toString());
                newEventTypeType.setId(eventType.getId());

            }else{
                newEventTypeType.setName(nametextView.getText().toString());
                newEventTypeType.setDescription(description.getText().toString());
                newEventTypeType.setSuggestedSubCategories(selectedSubCategories_Category);
            }
            getSelectedSubcategories();
            newEventTypeType.setSuggestedSubCategories(selectedSubCategories_Category);
            boolean result = eventService.saveEvent(newEventTypeType);
            if(result){
                Toast.makeText(getContext(), "Event added successfully.", Toast.LENGTH_SHORT).show();
                FragmentTransition.to(EventTypeManager.newInstance(), getActivity(),
                        true, R.id.event_manager_container);
            }else{
                Toast.makeText(getContext(), "Saving failed!", Toast.LENGTH_SHORT).show();
                FragmentTransition.to(EventTypeManager.newInstance(), getActivity(),
                        true, R.id.event_manager_container);
            }

        });

        return root;
    }

    private void Initialization() {
        selectCard = binding.serviceSubCategoryCardView;
        serviceSubCategoryTextView = binding.serviceSubCategoryTextView2;
        DaggerAppComponent.create().inject(this);
        subCategoryService.getSubCategoriesMap(this);
    }

    private void getSelectedSubcategories(){
        selectedSubCategories_Category = new ArrayList<>();
        for (int i = 0; i < selectedSubCategories.length; i++) {
            if (selectedSubCategories[i]) {
                selectedSubCategories_Category.add(subCategories.get(subCategoiesAray[i]));
            }
        }
    }

    private void showSubCategoriesDialog(){
        AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
        builder.setTitle("Select Sub Categories");
        builder.setCancelable(false);
        builder.setMultiChoiceItems(subCategoiesAray, selectedSubCategories, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked){
                    subCategoriesList.add(which);
                } else {
                    subCategoriesList.remove(Integer.valueOf(which));
                }
            }
        }).setPositiveButton("Ok", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateSubCategoriesText();
            }

        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setNeutralButton("Clear all", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for(int i = 0; i < selectedSubCategories.length; i++){
                    selectedSubCategories[i] = false;

                    subCategoriesList.clear();
                    serviceSubCategoryTextView.setText("");
                }
            }
        });
        builder.show();
    }

    private void updateSubCategoriesText() {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < subCategoriesList.size(); i++){
            stringBuilder.append(subCategoiesAray[subCategoriesList.get(i)]);

            if(i != subCategoriesList.size() - 1){
                stringBuilder.append(", ");
            }
            serviceSubCategoryTextView.setText(stringBuilder.toString());
        }
    }

    @Override
    public void onSubCategoriesLoaded(List<SubCategory> subCategories) {

    }

    @Override
    public void onSubCategoriesLoaded(Map<String, SubCategory> subCategories) {
        this.subCategories = subCategories;
        selectedSubCategories = new boolean[subCategories.size()];
        subCategoiesAray = new String[subCategories.size()];
        int index = 0;
        for (SubCategory subCategory : subCategories.values()) {
            subCategoiesAray[index++] = subCategory.getName();
        }
        if(eventType != null) {
            setupSubCategories();
        }
    }

    private void setupSubCategories() {
        if(eventType.getSuggestedSubCategories() != null){
            for(SubCategory sc : eventType.getSuggestedSubCategories()){
                int i = getIndex(sc);
                selectedSubCategories[i] = true;
            }
        }
        updateSubCategoriesText();
    }

    @Override
    public void onSubCategoryLoaded(SubCategory subCategory) {

    }

    @Override
    public void onError(Exception e) {

    }
}