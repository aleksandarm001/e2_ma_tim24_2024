package com.example.eventplanner_team24.event_manager.Presentation.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.event_manager.Presentation.EventTypeOverview;
import com.example.eventplanner_team24.event_manager.Service.EventService;
import com.example.eventplanner_team24.model.EventType;

import java.util.ArrayList;

import javax.inject.Inject;

public class EventTypeAdapter extends ArrayAdapter<EventType> {
    private ArrayList<EventType> mEventTypeTypes;
    private FragmentActivity mContext;

    @Inject
    EventService eventService;

    public EventTypeAdapter(FragmentActivity context, ArrayList<EventType> eventTypeTypes){
        super(context, R.layout.event_card, eventTypeTypes);
        mContext = context;
        mEventTypeTypes = eventTypeTypes;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DaggerAppComponent.create().inject(this);
        View listItem = convertView;
        if(listItem == null) {
            listItem = LayoutInflater.from(mContext).inflate(R.layout.event_card, parent, false);
        }

        EventType currentEventTypeType = mEventTypeTypes.get(position);

        LinearLayout eventCard = listItem.findViewById(R.id.your_linear_layout);
        eventCard.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(EventTypeOverview.newInstance(currentEventTypeType), mContext,
                        true, R.id.event_manager_container);
            }
        }));

        TextView eventName = listItem.findViewById(R.id.event_name_title);
        eventName.setText(currentEventTypeType.getName());

        TextView eventDescription = listItem.findViewById(R.id.event_description);
        eventDescription.setText(currentEventTypeType.getDescription());

        ToggleButton toggleButton = listItem.findViewById(R.id.deactivateEventToggle);
        toggleButton.setChecked(!currentEventTypeType.isActive());
        toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                currentEventTypeType.setActive(false);
                eventService.updateEvent(currentEventTypeType);
            }else{
                currentEventTypeType.setActive(true);
                eventService.updateEvent(currentEventTypeType);
            }
        });

        return listItem;
    }
}
