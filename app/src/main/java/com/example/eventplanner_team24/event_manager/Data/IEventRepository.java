package com.example.eventplanner_team24.event_manager.Data;

import com.example.eventplanner_team24.model.EventType;

public interface IEventRepository {
    void getEvents(EventCallback callback);
    boolean saveEvent(EventType eventType);
    void deleteEvent(EventType eventType);
    void updateEvent(EventType eventType);
}
