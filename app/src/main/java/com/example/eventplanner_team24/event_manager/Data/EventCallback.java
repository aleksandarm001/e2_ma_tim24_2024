package com.example.eventplanner_team24.event_manager.Data;

import com.example.eventplanner_team24.model.EventType;

import java.util.List;

public interface EventCallback {
    void onEventsLoaded(List<EventType> eventTypeTypes);
    void onError(Exception e);
}
