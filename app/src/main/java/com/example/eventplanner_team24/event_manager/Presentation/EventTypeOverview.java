package com.example.eventplanner_team24.event_manager.Presentation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentEventTypeOverviewBinding;
import com.example.eventplanner_team24.model.EventType;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventTypeOverview#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventTypeOverview extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM = "param";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private EventType eventType;
    private FragmentEventTypeOverviewBinding binding;
    public EventTypeOverview() {
        // Required empty public constructor
    }

    public static EventTypeOverview newInstance(EventType eventType) {
        EventTypeOverview fragment = new EventTypeOverview();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, eventType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventType = getArguments().getParcelable(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventTypeOverviewBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        TextView nametextView = binding.eventNameTitle;
        TextView description = binding.eventDescription;
        if(eventType != null){
            nametextView.setText(eventType.getName());
            description.setText(eventType.getDescription());
        }

        Button button = binding.editEventTypeButton;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(AddEventTypeFragment.newInstance(eventType),
                        getActivity(), false, R.id.event_manager_container);
            }
        });
        return root;
    }
}