package com.example.eventplanner_team24.event_manager.Data.Infrastructure;

import android.util.Log;

import com.example.eventplanner_team24.event_manager.Data.EventCallback;
import com.example.eventplanner_team24.event_manager.Data.IEventRepository;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class EventRepository implements IEventRepository {

    private final FirebaseFirestore db;
    @Inject
    public EventRepository() {
        this.db = FirebaseFirestore.getInstance();
    }
    @Override
    public void getEvents(EventCallback callback) {
        List<EventType> eventTypeTypes = new ArrayList<>();

        db.collection("events").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    EventType eventType = EventType.fromDocumentSnapshot(document);
                    ArrayList<SubCategory> suggestedSubCategories = new ArrayList<>();
                    List<String> subCategoryIds = (List<String>) document.get("suggestedSubCategories"); // Assuming you have this method
                    for (String subCategoryId : subCategoryIds) {
                        db.collection("sub_categories").document(subCategoryId).get().addOnCompleteListener(subCategoryTask -> {
                            if (subCategoryTask.isSuccessful()) {
                                DocumentSnapshot subCategoryDoc = subCategoryTask.getResult();
                                if (subCategoryDoc.exists()) {
                                    SubCategory subCategory = SubCategory.fromDocumentSnapshot(subCategoryDoc);
                                    suggestedSubCategories.add(subCategory);
                                } else {
                                    Log.e("EventRepository", "Subcategory document not found for ID: " + subCategoryId);
                                }
                            } else {
                                Log.e("EventRepository", "Error getting subcategory document", subCategoryTask.getException());
                            }

                            // Notify callback when all subcategories are loaded
                            if (suggestedSubCategories.size() == subCategoryIds.size()) {
                                eventType.setSuggestedSubCategories(suggestedSubCategories);
                                eventTypeTypes.add(eventType);
                                if (eventTypeTypes.size() == task.getResult().size()) {
                                    callback.onEventsLoaded(eventTypeTypes);
                                }
                            }
                        });
                    }
                }
            } else {
                Log.e("CategoryRepository", "Error getting categories", task.getException());
                callback.onError(task.getException());
            }
        });
    }


    @Override
    public boolean saveEvent(EventType eventType) {
        try {
            Map<String, Object> eventMap = new HashMap<>();
            if(eventType.getId() == null){
                eventMap.put("name", eventType.getName());
                eventMap.put("description", eventType.getDescription());
                eventMap.put("isActive", eventType.isActive());

                List<String> suggestedSubCategoriesData = new ArrayList<>();
                for (SubCategory subCategory : eventType.getSuggestedSubCategories()) {
                    suggestedSubCategoriesData.add(subCategory.getId());
                }
                eventMap.put("suggestedSubCategories", suggestedSubCategoriesData);

                db.collection("events")
                        .add(eventMap)
                        .addOnSuccessListener(documentReference -> {
                            Log.d("EventRepository", "Event added successfully with ID: " + documentReference.getId());

                            documentReference.update("id", documentReference.getId())
                                    .addOnSuccessListener(aVoid -> Log.d("EventRepository", "Event ID updated successfully"))
                                    .addOnFailureListener(e -> Log.e("EventRepository", "Error updating category ID", e));
                        })
                        .addOnFailureListener(e -> Log.e("EventRepository", "Error adding category", e));
            }else {
                DocumentReference eventRef = db.collection("events").document(eventType.getId());
                //eventMap.put("name", eventType.getName());
                eventMap.put("description", eventType.getDescription());

                List<String> suggestedSubCategoriesData = new ArrayList<>();
                for (SubCategory subCategory : eventType.getSuggestedSubCategories()) {
                    suggestedSubCategoriesData.add(subCategory.getId());
                }
                eventMap.put("suggestedSubCategories", suggestedSubCategoriesData);

                eventRef.update(eventMap)
                        .addOnSuccessListener(aVoid -> Log.d("EventRepository", "Event updated successfully"))
                        .addOnFailureListener(e -> Log.e("EventRepository", "Error updating event", e));
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void deleteEvent(EventType eventType) {

    }

    @Override
    public void updateEvent(EventType eventType) {
        try {
            Map<String, Object> updatedEventData = new HashMap<>();
            updatedEventData.put("name", eventType.getName());
            updatedEventData.put("description", eventType.getDescription());
            updatedEventData.put("isActive", eventType.isActive());

            if (!eventType.getSuggestedSubCategories().isEmpty()) {
                List<String> suggestedSubCategoriesData = new ArrayList<>();
                for (SubCategory subCategory : eventType.getSuggestedSubCategories()) {
                    suggestedSubCategoriesData.add(subCategory.getId());
                }
                updatedEventData.put("suggestedSubCategories", suggestedSubCategoriesData);
            }
            else{
                updatedEventData.put("suggestedSubCategories", null);
            }


            DocumentReference eventRef = db.collection("events").document(eventType.getId());

            eventRef.update(updatedEventData)
                    .addOnSuccessListener(aVoid -> Log.d("EventRepository", "Event updated successfully"))
                    .addOnFailureListener(e -> Log.e("EventRepository", "Error updating event", e));
        } catch (Exception e) {
            Log.e("EventRepository", "Error updating event", e);
        }
    }


}
