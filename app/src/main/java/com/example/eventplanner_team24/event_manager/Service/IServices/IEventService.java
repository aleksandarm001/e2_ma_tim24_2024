package com.example.eventplanner_team24.event_manager.Service.IServices;

import com.example.eventplanner_team24.event_manager.Data.EventCallback;
import com.example.eventplanner_team24.model.EventType;

public interface IEventService {
    void getEvents(EventCallback callback);
    boolean saveEvent(EventType eventType);
    void deleteEvent(EventType eventType);
    void updateEvent(EventType eventType);
}
