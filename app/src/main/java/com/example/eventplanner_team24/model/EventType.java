package com.example.eventplanner_team24.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class EventType implements Parcelable {

    private String id;
    private String name;
    private String description;
    private ArrayList<SubCategory> suggestedSubCategories;
    private ArrayList<String> suggestedSubCategoriesIds;
    private boolean isActive;

    public ArrayList<String> getSuggestedSubCategoriesIds() {
        return suggestedSubCategoriesIds;
    }

    public void setSuggestedSubCategoriesIds(ArrayList<String> suggestedSubCategoriesIds) {
        this.suggestedSubCategoriesIds = suggestedSubCategoriesIds;
    }

    public EventType() {
        this.isActive = true;
    }

    public EventType(String id, String name, String description, ArrayList<SubCategory> suggestedSubCategories) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.suggestedSubCategories = suggestedSubCategories;
        this.isActive = true;
    }

    public EventType(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isActive = true;
    }

    protected EventType(Parcel in){
        id = in.readString();
        name = in.readString();
        description = in.readString();
        suggestedSubCategories = in.createTypedArrayList(SubCategory.CREATOR);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<SubCategory> getSuggestedSubCategories() {
        return suggestedSubCategories;
    }

    public void setSuggestedSubCategories(ArrayList<SubCategory> suggestedSubCategories) {
        this.suggestedSubCategories = suggestedSubCategories;
    }
    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeTypedList(suggestedSubCategories);
        dest.writeBoolean(isActive);
    }

    public static final Creator<EventType> CREATOR = new Creator<EventType>() {
        @Override
        public EventType createFromParcel(Parcel in) {
            return new EventType(in);
        }

        @Override
        public EventType[] newArray(int size) {
            return new EventType[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return name; // Assuming 'name' is the field containing the name of the event
    }
    public static EventType fromDocumentSnapshot(DocumentSnapshot document) {
        EventType eventType = new EventType();
        eventType.setId(document.getString("id"));
        eventType.setName(document.getString("name"));
        eventType.setDescription(document.getString("description"));
        eventType.setSuggestedSubCategories((ArrayList<SubCategory>) document.get("suggestedSubCategories"));
        eventType.setActive(document.getBoolean("isActive"));
        return eventType;
    }
}
