package com.example.eventplanner_team24.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class Service implements Parcelable{
    private String id;
    private String category;
    private String subcategory;
    private String name;
    private String description;
    private String specifics;
    private double pricePerHour;
    private double price;
    private double minDuration;
    private double maxDuration;
    private String location;
    private double discount;
    private List<String> attendants;
    private List<String> type;
    private int reservationDeadline;
    private int cancellationDeadline;
    private boolean available;
    private boolean visible;
    private boolean manualConfirmation;
    private boolean deleted;
    private List<String> imageUris;

//    private ArrayList<Change> changes;
    private String lastChange;






//    public ArrayList<Change> getChanges() {
//        return changes;
//    }

//    public void setChanges(ArrayList<Change> changes) {
//        this.changes = changes;
//    }

    public String getLastChange() {
        return lastChange;
    }

    public void setLastChange(String lastChange) {
        this.lastChange = lastChange;
    }

    public String getId() {
        return id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(double maxDuration) {
        this.maxDuration = maxDuration;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecifics() {
        return specifics;
    }

    public void setSpecifics(String specifics) {
        this.specifics = specifics;
    }

    public double getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(double duration) {
        this.minDuration = duration;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public List<String> getAttendants() {
        return attendants;
    }

    public void setAttendants(List<String> attendants) {
        this.attendants = attendants;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public double getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(Integer reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public double getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(Integer cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getManualConfirmation() {
        return manualConfirmation;
    }

    public void setManualConfirmation(Boolean manualConfirmation) {
        this.manualConfirmation = manualConfirmation;
    }

    public Service(String id, String category, String subcategory, String name, String description, List<Integer> images, String specifics, double pricePerHour, double price, double duration, String location, double discount, List<String> attendants, List<String> type, Integer reservationDeadline, Integer cancellationDeadline, Boolean available, Boolean visible, Boolean manualConfirmation) {
        this.id = id;
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.specifics = specifics;
        this.pricePerHour = pricePerHour;
        this.price = price;
        this.minDuration = duration;
        this.location = location;
        this.discount = discount;
        this.attendants = attendants;
        this.type = type;
        this.reservationDeadline = reservationDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.available = available;
        this.visible = visible;
        this.manualConfirmation = manualConfirmation;
        this.deleted=false;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", subcategory='" + subcategory + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", specifics='" + specifics + '\'' +
                ", pricePerHour=" + pricePerHour +
                ", price=" + price +
                ", duration=" + minDuration +
                ", location='" + location + '\'' +
                ", discount=" + discount +
                ", attendants=" + attendants +
                ", type=" + type +
                ", reservationDeadline=" + reservationDeadline +
                ", cancelationDeadline=" + cancellationDeadline +
                ", available=" + available +
                ", visible=" + visible +
                ", manualy=" + manualConfirmation +
                '}';
    }

    public Service(){
        this.deleted=false;
        this.discount=0;
        this.specifics="";
//        this.changes=new ArrayList<>();
        this.imageUris=new ArrayList<>();

    }

    public static final Parcelable.Creator<Service> CREATOR = new Parcelable.Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    public List<String> getImageUris() {
        return imageUris;
    }

    public void setImageUris(List<String> imageUris) {
        this.imageUris = imageUris;
    }

    protected Service(Parcel in) {
        // Čitanje ostalih atributa proizvoda iz Parcel objekta
        id = in.readString();
        name = in.readString();
        description = in.readString();
        category = in.readString();
        subcategory=in.readString();
        price=in.readDouble();
        discount=in.readDouble();
        available=in.readBoolean();
        visible=in.readBoolean();
        specifics=in.readString();
        pricePerHour=in.readDouble();
        type=in.readArrayList(String.class.getClassLoader());
        minDuration=in.readDouble();
        maxDuration=in.readDouble();
        location=in.readString();
        attendants=in.readArrayList(String.class.getClassLoader());
        reservationDeadline=in.readInt();
        cancellationDeadline=in.readInt();
        manualConfirmation=in.readBoolean();
        lastChange=in.readString();
//        changes = new ArrayList<>();
//        in.readList(changes, getClass().getClassLoader());
        imageUris=in.readArrayList(String.class.getClassLoader());

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(description);
        dest.writeString(category);
        dest.writeString(subcategory);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeList(type);
        dest.writeBoolean(available);
        dest.writeBoolean(visible);
        dest.writeString(specifics);
        dest.writeString(name);
        dest.writeDouble(pricePerHour);
        dest.writeDouble(minDuration);
        dest.writeDouble(maxDuration);
        dest.writeString(location);
        dest.writeList(attendants);
        dest.writeInt(reservationDeadline);
        dest.writeInt(cancellationDeadline);
        dest.writeBoolean(manualConfirmation);
//        dest.writeList(changes);
        dest.writeString(lastChange);
        dest.writeList(imageUris);
    }
}
