package com.example.eventplanner_team24.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategorySuggestion implements Parcelable {

    private Long id;
    private String subCategoryName;
    private SubCategory.Status status;
    private String descripiton;
    private String userName;



    public SubCategorySuggestion() {
    }

    public SubCategorySuggestion(Long id, String suggestion, SubCategory.Status status, String descripiton, String userName) {
        this.id = id;
        this.subCategoryName = suggestion;
        this.status = status;
        this.descripiton = descripiton;
        this.userName = userName;
    }

    protected SubCategorySuggestion(Parcel in) {
        id = in.readLong();
        subCategoryName = in.readString();
        descripiton = in.readString();
        status = SubCategory.Status.valueOf(in.readString());
        userName = in.readString();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getsubCategoryName() {
        return subCategoryName;
    }

    public void setsubCategoryName(String suggestion) {
        this.subCategoryName = suggestion;
    }

    public SubCategory.Status getStatus() {
        return status;
    }

    public void setStatus(SubCategory.Status status) {
        this.status = status;
    }

    public String getDescripiton() {
        return descripiton;
    }

    public void setDescripiton(String descripiton) {
        this.descripiton = descripiton;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "SubCategorySuggestion{" +
                "id=" + id +
                ", suggestion='" + subCategoryName + '\'' +
                ", status=" + status +
                ", descripiton='" + descripiton + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeLong(id);
        dest.writeString(subCategoryName);
        dest.writeString(status.name());
        dest.writeString(descripiton);
        dest.writeString(userName);
    }

    public static final Creator<SubCategorySuggestion> CREATOR = new Creator<SubCategorySuggestion>() {
        @Override
        public SubCategorySuggestion createFromParcel(Parcel in) {
            return new SubCategorySuggestion(in);
        }

        @Override
        public SubCategorySuggestion[] newArray(int size) {
            return new SubCategorySuggestion[size];
        }
    };
}
