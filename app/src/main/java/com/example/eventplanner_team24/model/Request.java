package com.example.eventplanner_team24.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.HashMap;
import java.util.Map;

public class Request implements Parcelable {
    public enum Status {
        PENDING,
        ACCEPTED,
        REJECTED
    }
    private String id;
    private String userId;
    private String companyName;
    private String ownerName;
    private String ownerSurname;
    private String email;
    private String category;
    private String eventType;
    private long requestTime;
    private Status status;

    protected Request(Parcel in) {
        id = in.readString();
        userId = in.readString();
    }

    public Request(String id, String userId, String companyName,
                   String ownerName, String ownerSurname, String email,
                   String category, String eventType, long requestTime){
        this.id = id;
        this.userId = userId;
        this.companyName = companyName;
        this.ownerName = ownerName;
        this.ownerSurname = ownerSurname;
        this.email = email;
        this.category = category;
        this.eventType = eventType;
        this.requestTime = requestTime;
        this.status = Status.PENDING;
    }

    public Request(String id, String userId, String companyName, String ownerName, String ownerSurname, String email, String category, String eventType, long requestTime, Status status) {
        this.id = id;
        this.userId = userId;
        this.companyName = companyName;
        this.ownerName = ownerName;
        this.ownerSurname = ownerSurname;
        this.email = email;
        this.category = category;
        this.eventType = eventType;
        this.requestTime = requestTime;
        this.status = status;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("userId", userId);
        return map;
    }

    public static final Creator<Request> CREATOR = new Creator<Request>() {
        @Override
        public Request createFromParcel(Parcel in) {
            return new Request(in);
        }

        @Override
        public Request[] newArray(int size) {
            return new Request[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(userId);
    }

    public Request(String id, String userId) {
        this.id = id;
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerSurname() {
        return ownerSurname;
    }

    public void setOwnerSurname(String ownerSurname) {
        this.ownerSurname = ownerSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public static Request fromDocumentSnapshot(DocumentSnapshot document) {
        String id = document.getId();
        String userId = document.getString("userId");
        String companyName = document.getString("companyName");
        String ownerName = document.getString("ownerName");
        String ownerSurname = document.getString("ownerSurname");
        String email = document.getString("email");
        String category = document.getString("category");
        String eventType = document.getString("eventType");
        String statusString = document.getString("status");
        Status status = Status.PENDING;

        if (statusString != null) {
            try {
                status = Status.valueOf(statusString.toUpperCase());
            } catch (IllegalArgumentException e) {
            }
        }
        return new Request(id, userId, companyName, ownerName, ownerSurname, email, category, eventType, 0, status);

    }
}
