package com.example.eventplanner_team24.model;

import android.os.Parcel;
import android.os.Parcelable;


public class Notification implements Parcelable {
    public enum Role {
        OD,
        ADMIN,
        PUPV
    }
    private String title;
    private String text;
    private String receiverId;
    private Boolean read;
    private Role role;

    public Notification() {
    }

    public Notification(String title, String text, String receiverId, Boolean read, Role role) {
        this.title = title;
        this.text = text;
        this.receiverId = receiverId;
        this.read = read;
        this.role = role;
    }

    protected Notification(Parcel in) {
        title = in.readString();
        text = in.readString();
        receiverId = in.readString();
        byte tmpRead = in.readByte();
        read = tmpRead == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(text);
        dest.writeString(receiverId);
        dest.writeByte((byte) (read ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}