package com.example.eventplanner_team24;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmail extends AsyncTask<Void, Void, Boolean> {
    private Context mContext;
    private String mRecipientEmail;
    private String mReason;

    public SendEmail(Context context, String recipientEmail, String reason) {
        mContext = context;
        mRecipientEmail = recipientEmail;
        mReason = reason;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        final String username = "projekatbsep@gmail.com";
        final String password = "ggma crww tfbl tppb";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mRecipientEmail));
            message.setSubject("Your request has been rejected");
            message.setText("Dear User,\n\nYour request has been rejected. Reason: " + mReason + "\n\nBest regards,\nEventEaze Team");

            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if (success) {
            Toast.makeText(mContext, "Email sent successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Failed to send email", Toast.LENGTH_SHORT).show();
        }
    }
}