package com.example.eventplanner_team24.services;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentAddServiceBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.repositories.EventTypeRepo;
import com.example.eventplanner_team24.repositories.ServiceRepository;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddServiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddServiceFragment extends Fragment {
    private FragmentAddServiceBinding binding;

    private Boolean isFirstSelection =true;
    private EditText name;
    private EditText description;
    private EditText pricePerHour;
    private EditText discount;
    private TextView orrAdd;
    private EditText specifics;
    private EditText minDuration;
    private EditText maxDuration;
    private Service newService;
    private Spinner category;
    private Spinner subcategory;
    private ArrayList<EventType> selectedEvents;
//    private SubcategorySuggestion subcategorySuggestion;
    private Category selectedCategory=null;
    private SubCategory selectedSubcategory=null;
    private String subcatgoryName="";
    private String subcategoryDescription;
    private Service service;
    private Spinner spinnerCategory;
    private Spinner spinnersubcategory;

    private boolean isEventTypeChanged;

    private List<Uri> selectedUriImages;
    private List<String> imageUris;
    private Spinner locationService;
    private Spinner performersService;
    private EditText reservationDeadline;
    private EditText cancelationDeadline;
    private RadioButton availableService;
    private RadioButton notAvailableService;
    private RadioButton visibleService;
    private RadioButton notVisibleService;
    private RadioButton manualAcceptance;
    private RadioButton automaticAcceptance;
    //private ArrayList<String> selectedPerformers;

//    private ArrayList<Employee> selectedEmployees;

    private ArrayList<Integer> selectedImages = new ArrayList<>();

    public AddServiceFragment() {
        // Required empty public constructor
    }


    public static AddServiceFragment newInstance(Service s) {
        AddServiceFragment fragment = new AddServiceFragment();
        Bundle args = new Bundle();
        args.putParcelable("SERVICE", s);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedEvents=new ArrayList<>();
//        subcategorySuggestion=new SubcategorySuggestion();
        if (getArguments() != null) {
            service = getArguments().getParcelable("SERVICE");
            newService=getArguments().getParcelable("SERVICE");
        }

        imageUris=new ArrayList<>();
        isEventTypeChanged=false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAddServiceBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        Button uploadImageButton = root.findViewById(R.id.uploadCompanyPhotoButton);
        LinearLayout photoLinearLayout = root.findViewById(R.id.photoLinearLayout);
        List<Uri> selectedUriImages = new ArrayList<>();


        orrAdd=binding.tworAdd;
        name=binding.nameService;
        description=binding.descriptionService;
        pricePerHour=binding.pricePerHourService;
        discount=binding.discountService;
        specifics=binding.specificsService;
        minDuration=binding.minHService;
        maxDuration=binding.maxHService;
        category=binding.spinnerServiceCategory;
        subcategory=binding.spinnerServiceSubcategory;
        spinnerCategory=binding.spinnerServiceCategory;
        spinnersubcategory=binding.spinnerServiceSubcategory;

        reservationDeadline=binding.deadlineService;
        cancelationDeadline=binding.cancelationService;
        availableService=binding.availableService;
        notAvailableService=binding.notAvailableService;
        visibleService=binding.visibleService;
        notVisibleService=binding.notVisibleService;
        manualAcceptance=binding.manualService;
        automaticAcceptance=binding.automaticService;
        locationService=binding.spinnerLocationService;
        performersService = binding.spinnerServicePerformers;


        if(service==null) {
            SetCreatingState();
            availableService.setChecked(true);
            visibleService.setChecked(true);
            manualAcceptance.setChecked(true);
        }else
            SetUpdatingState();

        EventTypeRepo eventTypeRepo = new EventTypeRepo(); // Instantiate your EventTypeRepo class

        eventTypeRepo.getAllActivateEventTypes(new EventTypeRepo.EventTypeFetchCallback() {
            @Override
            public void onEventTypeFetch(ArrayList<EventType> eventTypes) {
                if (eventTypes != null) {
                    Spinner spinner = (Spinner) root.findViewById(R.id.spinnerServiceEventType);
                    ArrayAdapter<EventType> arrayAdapter = new ArrayAdapter<EventType>(getActivity(), R.layout.multispiner,
                            eventTypes) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            // Set your text and checkbox state here
                            EventType eventType = getItem(position);
                            if (eventType != null) {

                                if(service!=null){
                                    for(String s:service.getType())
                                        if(s.equals(eventType.getId()) && !selectedEvents.contains(eventType))
                                            selectedEvents.add(eventType);
                                }

                                textView.setText(eventType.getName()); // Assuming EventType has a getName() method
                                checkBox.setChecked(selectedEvents.contains(eventType)); // Assuming selectedEvents is a list of selected EventType objects
                                checkBox.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        isEventTypeChanged=true;
                                        CheckBox checkBox = (CheckBox) v;
                                        EventType selectedEventType = getItem(position);
                                        if (checkBox.isChecked() && !selectedEvents.contains(selectedEventType)) {
                                            selectedEvents.add(selectedEventType);
                                        } else {
                                            selectedEvents.remove(selectedEventType);
                                        }
                                    }
                                });
                            }

                            return view;
                        }
                    };
                    spinner.setAdapter(arrayAdapter);
                }
            }
        });


        ArrayAdapter<String> arrayAdapterLocation = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.location_array));
        arrayAdapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationService.setAdapter(arrayAdapterLocation);

        if(service != null)
            locationService.setSelection(arrayAdapterLocation.getPosition(service.getLocation()));



        ArrayAdapter<String> arrayAdapterPerformers = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.performers_array));
        arrayAdapterPerformers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        performersService.setAdapter(arrayAdapterPerformers);

//        if(service != null)
//            performersService.setSelection(arrayAdapterLocation.getPosition(service.get));


        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

        ActivityResultLauncher<Intent> mGetContent = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                            Intent data = result.getData();
                            selectedUriImages.clear();  //da mi ne duplira slike

                            if (data.getClipData() != null) {
                                ClipData clipData=data.getClipData();
                                for(int i=0;i<clipData.getItemCount();i++){
                                    ClipData.Item item=clipData.getItemAt(i);
                                    Uri selectedImageUri = item.getUri();
                                    selectedUriImages.add(selectedImageUri);
                                }

                            } else if (data.getData() != null) {
                                Uri selectedImageUri=data.getData();
                                selectedUriImages.add(selectedImageUri);
                            }
                            // za prikaz svih slika u layoutu
                            for (Uri imageUri : selectedUriImages) {
                                ImageView imageView = new ImageView(getContext());
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                        300,300
                                );
                                layoutParams.setMargins(0, 0, 10, 0);
                                imageView.setLayoutParams(layoutParams);
                                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                imageView.setImageURI(imageUri);
                                photoLinearLayout.addView(imageView);
                            }
                            for(Uri imageUri:selectedUriImages){
                                StorageReference imageRef = storageRef.child("images/" + UUID.randomUUID().toString());

                                imageRef.putFile(imageUri)
                                        .addOnSuccessListener(taskSnapshot -> {
                                            // Image uploaded successfully, get the download URL
                                            imageRef.getDownloadUrl().addOnSuccessListener(uri -> {
                                                // Save the download URL to the Realtime Database
                                                String imageUrl = uri.toString();
                                                imageUris.add(imageUrl);
                                                newService.setImageUris(imageUris);
                                                if(service!=null){
                                                    List<String> uris=service.getImageUris();
                                                    uris.add(imageUrl);
                                                    service.setImageUris(uris);
                                                }
                                            });
                                        })
                                        .addOnFailureListener(exception -> {
                                            // Handle unsuccessful uploads
                                            Log.e("TAG", "Image upload failed: " + exception.getMessage());
                                        });
                            }
                        }
                    }
                });

        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                mGetContent.launch(intent);
            }
        });

        return  root;
    }



    @Override
    public void onViewCreated(@Nullable View view, @Nullable Bundle savedInstanceState) {


        Button btnSubmit = (Button) binding.btnSubmit;
        btnSubmit.setOnClickListener(v -> {
            if(service == null) {
                if (addNewService()) {
//                    if (subcategorySuggestion.getName() != null)
////                        SubcategorySuggestionsRepo.create(subcategorySuggestion);
//                    else
                    ServiceRepository.createService(newService);
                    FragmentTransition.to(ServicesPageFragment.newInstance(), getActivity(),
                            true, R.id.list_layout_services);
                }
            }else{
                if(updateService()){
                    LocalDateTime now=LocalDateTime.now();
                    service.setLastChange(now.toString());
                    ServiceRepository.updateService(service);

                    FragmentTransition.to(ServicesPageFragment.newInstance(), getActivity(),
                            true, R.id.list_layout_services);
                }

            }
        });

    }

    private void SetCreatingState(){

        newService=new Service();
        CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                if (categories != null) {
                    // Create an adapter for the spinner with the fetched categories
                    ArrayAdapter<Category> arrayAdapter = new ArrayAdapter<>(requireContext(),
                            android.R.layout.simple_spinner_item, categories);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // Set the adapter to the spinner
                    spinnerCategory.setAdapter(arrayAdapter);
                } else {
                    // Handle the case where fetching categories failed
                    // Show a toast or handle the error appropriately
                }
            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected category
                selectedCategory = (Category) parent.getItemAtPosition(position);

                // Fetch subcategories based on the selected category
                SubCategoryRepository subcategoryRepo = new SubCategoryRepository();
                subcategoryRepo.getSubcategoriesByCategoryId(selectedCategory.getId(), new SubCategoryRepository.SubcategoryFetchCallback() {
                    @Override
                    public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {
                        if (subcategories != null) {
                            // Create an adapter for the subcategories spinner
                            ArrayAdapter<SubCategory> subcategoryAdapter = new ArrayAdapter<>(requireContext(),
                                    android.R.layout.simple_spinner_item, subcategories);
                            subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            // Set the adapter to the subcategories spinner
                            spinnersubcategory.setAdapter(subcategoryAdapter);
                        } else {
                            // Handle the case where fetching subcategories failed
                            // Show a toast or handle the error appropriately
                        }
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle case when nothing is selected
            }
        });


        spinnersubcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedSubcategory = (SubCategory) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void SetUpdatingState(){
        selectedCategory=new Category();
        selectedSubcategory=new SubCategory();
        name.setText(service.getName());
        description.setText(service.getDescription());
        pricePerHour.setText(Double.toString(service.getPricePerHour()));
        discount.setText(Double.toString(service.getDiscount()));
        minDuration.setText(Double.toString(service.getMinDuration()));
        maxDuration.setText(Double.toString(service.getMaxDuration()));
        specifics.setText(service.getSpecifics());
        LinearLayout llCategory=binding.llcatService;
        llCategory.setVisibility(View.GONE);
        LinearLayout llSubcategory=binding.llsubcatService;
        llSubcategory.setVisibility(View.GONE);
        CategoryRepository.getAllCategories( new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                for(Category c:categories)
                    if(c.getId().equals(service.getCategory())){
                        SubCategoryRepository.getAllSubcategories(new SubCategoryRepository.SubcategoryFetchCallback() {
                            @Override
                            public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {
                                for(SubCategory s:subcategories)
                                    if(s.getId().equals(service.getSubcategory()))
                                        orrAdd.setText("Category: "+c.getName()+"         Subcategory: "+s.getName());
                            }
                        });
                    }
            }
        });

        if(service.getVisible())
            visibleService.setChecked(true);
        else
            notVisibleService.setChecked(true);
        if(service.getAvailable())
            availableService.setChecked(true);
        else
            notAvailableService.setChecked(true);
        if(service.getManualConfirmation())
            manualAcceptance.setChecked(true);
        else
            automaticAcceptance.setChecked(true);
        cancelationDeadline.setText(Double.toString(service.getCancellationDeadline()));
        reservationDeadline.setText(Double.toString(service.getReservationDeadline()));

    }

    private boolean addNewService(){
        if(areFieldsValid() && isLocationValid()){

            newService.setName(name.getText().toString());
            newService.setDescription(description.getText().toString());
            newService.setPricePerHour(Double.parseDouble(pricePerHour.getText().toString()));
            newService.setMinDuration(Double.parseDouble(minDuration.getText().toString()));
            newService.setMaxDuration(Double.parseDouble(maxDuration.getText().toString()));
            if(!specifics.getText().toString().isEmpty())
                newService.setSpecifics(specifics.getText().toString());
            if(!discount.getText().toString().isEmpty())
                newService.setDiscount(Double.parseDouble(discount.getText().toString()));
            newService.setCategory(selectedCategory.getId());
            ArrayList<String> events=new ArrayList<>();
            for(EventType e:selectedEvents)
                events.add(e.getId());
            newService.setType(events);
            if(subcatgoryName.isEmpty())
                newService.setSubcategory(selectedSubcategory.getId());
            newService.setPrice(newService.getPricePerHour()*newService.getMaxDuration());

            newService.setCancellationDeadline(Integer.parseInt(cancelationDeadline.getText().toString()));
            newService.setReservationDeadline(Integer.parseInt(reservationDeadline.getText().toString()));
            newService.setLocation(locationService.getSelectedItem().toString());
            ArrayList<String> employees=new ArrayList<>();
//            for(Employee e:selectedEmployees)
//                employees.add(e.getId());
            newService.setAttendants(employees);
            if(availableService.isChecked())
                newService.setAvailable(true);
            else
                newService.setAvailable(false);
            if(visibleService.isChecked())
                newService.setVisible(true);
            else
                newService.setVisible(false);
            if(manualAcceptance.isChecked()) {
                newService.setManualConfirmation(true);
            }
            else if(automaticAcceptance.isChecked()){
                newService.setManualConfirmation(false);
            }
            //servis.setImageUris(imageUris);

            return true;
        }
        return false;
    }

    private boolean updateService(){
        if(!isEventTypeChanged)
            selectedEvents.add(new EventType());
//        if(!isEmployeeChanged)
//            selectedEmployees.add(new Employee());
        if(areFieldsValid() && isLocationValid()) {
            service.setName(name.getText().toString());
            service.setDescription(description.getText().toString());
            service.setSpecifics(specifics.getText().toString());
            service.setPricePerHour(Double.parseDouble(pricePerHour.getText().toString()));
            service.setMinDuration(Double.parseDouble(minDuration.getText().toString()));
            service.setMaxDuration(Double.parseDouble(maxDuration.getText().toString()));
            if(isEventTypeChanged) {
                ArrayList<String> events = new ArrayList<>();
                for (EventType e : selectedEvents)
                    events.add(e.getId());
                service.setType(events);
            }

            service.setCancellationDeadline((int)Double.parseDouble(cancelationDeadline.getText().toString()));
            service.setReservationDeadline((int)Double.parseDouble(reservationDeadline.getText().toString()));
            service.setLocation(locationService.getSelectedItem().toString());
//            if(isEmployeeChanged) {
//                ArrayList<String> employees = new ArrayList<>();
//                for (Employee e : selectedEmployees)
//                    employees.add(e.getId());
//                service.setAttendants(employees);
//            }
            if(availableService.isChecked())
                service.setAvailable(true);
            else
                service.setAvailable(false);
            if(visibleService.isChecked())
                service.setVisible(true);
            else
                service.setVisible(false);
            if(manualAcceptance.isChecked()) {
                service.setManualConfirmation(true);
            }
            else if(automaticAcceptance.isChecked()){
                service.setManualConfirmation(false);
            }

            return true;
        }
        return false;
    }

    private boolean areFieldsValid(){
        if(name.getText().toString().isEmpty()){
            name.setError("Name is required.");
            return false;
        }
        if(description.getText().toString().isEmpty()){
            description.setError("Description is required.");
            return false;
        }
        if(pricePerHour.getText().toString().isEmpty()){
            pricePerHour.setError("Price per hour is required.");
            return false;
        }
        if(!pricePerHour.getText().toString().isEmpty() && Double.parseDouble(pricePerHour.getText().toString())<0){
            pricePerHour.setError("Input positive number.");
            return false;
        }
        if(!discount.getText().toString().isEmpty() && Double.parseDouble(discount.getText().toString())<0){
            discount.setError("Input positive number.");
            return false;
        }
        if(minDuration.getText().toString().isEmpty()){
            minDuration.setError("Minimum duration is required");
            return false;
        }
        if(maxDuration.getText().toString().isEmpty()){
            maxDuration.setError("Maximum duration is required");
            return false;
        }
        if(!minDuration.getText().toString().isEmpty() && Double.parseDouble(minDuration.getText().toString())<0){
            minDuration.setError("Input positive number.");
            return false;
        }
        if(!maxDuration.getText().toString().isEmpty() && Double.parseDouble(maxDuration.getText().toString())<0){
            pricePerHour.setError("Input positive number.");
            return false;
        }
        if( selectedCategory==null ){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); // Pass your activity or context here

            builder.setTitle("Error!");
            builder.setMessage("Please select category.");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        }
        if( selectedSubcategory==null ){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); // Pass your activity or context here

            builder.setTitle("Error!");
            builder.setMessage("Please select subcategory.");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        }

        if(Double.parseDouble(minDuration.getText().toString())>=Double.parseDouble(maxDuration.getText().toString()))
        {
            maxDuration.setError("Max duration must be greater than min duration .");
            return false;
        }
//        if( selectedEvents.size()==0 ){
//            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); // Pass your activity or context here
//
//            builder.setTitle("Error!");
//            builder.setMessage("Please select event type.");
//
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//
//            AlertDialog dialog = builder.create();
//            dialog.show();
//            return false;
//        }
        return true;
    }

    private boolean isLocationValid(){
        if(locationService.getSelectedItemPosition()<=0){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); // Pass your activity or context here

            builder.setTitle("Error!");
            builder.setMessage("Please select location.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        }
        return true;
    }

//    private boolean AreEmployeesValid(){
//        if(selectedEmployees.size()<=0){
//            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); // Pass your activity or context here
//
//            builder.setTitle("Error!");
//            builder.setMessage("Please select performers.");
//
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//
//            AlertDialog dialog = builder.create();
//            dialog.show();
//            return false;
//        }
//        return true;
//    }


}