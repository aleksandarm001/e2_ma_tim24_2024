package com.example.eventplanner_team24.services;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.HomeActivity;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Service;

import java.util.ArrayList;

public class ServicePackageListAdapter  extends ArrayAdapter<Service> {
    private ArrayList<Service> aServices;

    public ServicePackageListAdapter(Context context, ArrayList<Service> services){
        super(context, R.layout.service_list_item, services);
        aServices = services;

    }
    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return aServices.size();
    }


    @Nullable
    @Override
    public Service getItem(int position) {
        return aServices.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Service service = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_for_package,
                    parent, false);
        }
        LinearLayout productCard = convertView.findViewById(R.id.product_card_for_package_item);

        TextView productTitle = convertView.findViewById(R.id.productP_title);
        TextView productCategory = convertView.findViewById(R.id.productP_category);
        TextView productPrice = convertView.findViewById(R.id.productP_price);
        Button btnDelete=convertView.findViewById(R.id.btnDelete);


        if(service != null){
            productTitle.setText(service.getName());
            CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
                @Override
                public void onCategoryFetch(ArrayList<Category> categories) {
                    for(Category c:categories)
                        if(c.getId().equals(service.getCategory()))
                            productCategory.setText(c.getName());
                }
            });
            productPrice.setText(Double.toString(service.getPrice())+" din");
            HomeActivity activity = (HomeActivity) getContext();

            productCard.setOnClickListener(v -> {
                if (getContext() instanceof HomeActivity) {
                    FragmentTransition.to(ServiceDetails.newInstance(service),activity ,
                            true, R.id.list_layout_services);
                }


            });

            SharedPreferences prefs = getContext().getSharedPreferences("MyPrefs", MODE_PRIVATE);
            String stringValue = prefs.getString("key_string", "OWNER");

            if(stringValue.equals("EMPLOYEE"))
                btnDelete.setVisibility(View.GONE);

            btnDelete.setOnClickListener(v->{
                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                dialog.setMessage("Are you sure you want to remove this service?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }

                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = dialog.create();
                alert.show();
            });



        }

        return convertView;
    }
}