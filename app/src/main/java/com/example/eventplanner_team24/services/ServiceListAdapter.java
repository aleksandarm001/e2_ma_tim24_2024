package com.example.eventplanner_team24.services;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.HomeActivity;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.products.ProductDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServiceListAdapter extends ArrayAdapter<Service> {
    private ArrayList<Service> aService;
    private FragmentActivity mContext;

    public ServiceListAdapter(FragmentActivity context, ArrayList<Service> services){
        super(context, R.layout.service_list_item);
        mContext = context;
        aService = services;
    }

    @Override
    public int getCount() {
        return aService.size();
    }

    @Nullable
    @Override
    public Service getItem(int position) {
        return aService.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Service service = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.service_list_item,
                    parent, false);
        }
        LinearLayout productCard = convertView.findViewById(R.id.service_list_item);
        ImageView imageView = convertView.findViewById(R.id.service_image);
        TextView productTitle = convertView.findViewById(R.id.service_name);
        TextView productCategory = convertView.findViewById(R.id.service_category);
        TextView productPrice = convertView.findViewById(R.id.service_price);
        TextView productAvailable = convertView.findViewById(R.id.service_available);

        if(service != null){
            if (!service.getImageUris().isEmpty()) {
                Picasso.get().load(service.getImageUris().get(0)).into(imageView);
            }              productTitle.setText(service.getName());
            CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
                @Override
                public void onCategoryFetch(ArrayList<Category> categories) {
                    for(Category c:categories)
                        if(c.getId().equals(service.getCategory()))
                            productCategory.setText(c.getName());
                }
            });            productPrice.setText(Double.toString(service.getPricePerHour())+" din/h");
            if(service.getAvailable())
                productAvailable.setText("Dostupan");
            else
                productAvailable.setText("Nedostupan");

            productCard.setOnClickListener(v -> {
                if (getContext() instanceof HomeActivity) {
                    HomeActivity activity = (HomeActivity) getContext();
//                    Log.i("EventApp","aktivno "+activity.IsVisible());
//                    if(!activity.IsVisible()) {
                        FragmentTransition.to(ServiceDetails.newInstance(service), activity,
                                true, R.id.list_layout_services);
//                    }else{
//                    FragmentTransition.to(ServiceDetails.newInstance(service), activity,
//                            true, R.id.scroll_employees_list);
//                }

            }


            });




        }

        return convertView;
    }
}
