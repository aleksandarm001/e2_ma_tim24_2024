package com.example.eventplanner_team24.services;

import static android.content.Context.MODE_PRIVATE;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentServiceDetailsBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.repositories.ServiceRepository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServiceDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiceDetails extends Fragment {

    private FragmentServiceDetailsBinding binding;
    private List<Uri> selectedUriImages;

    private TextView name;
    private TextView description;
    private TextView category;
    private TextView event;
    private TextView pricePH;
    private TextView discount;
    private TextView discountPrice;
    private TextView performers;
    private TextView available;
    private TextView visible;
    private TextView cancelationPeriod;
    private TextView reservationPeriod;
    private TextView acceptance;
    private TextView location;
    private TextView specifics;
    private TextView minDuration;
    private TextView maxDuration;
    private Service servis;



    public ServiceDetails() {
        // Required empty public constructor
    }


    public static ServiceDetails newInstance(Service s) {
        ServiceDetails fragment = new ServiceDetails();
        Bundle args = new Bundle();
        args.putParcelable("SERVICE", s);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            servis = getArguments().getParcelable("SERVICE");
        }
        selectedUriImages = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentServiceDetailsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        LinearLayout photoLinearLayout = root.findViewById(R.id.photoLinearLayout);
        SharedPreferences prefs = requireContext().getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String stringValue = prefs.getString("key_string", "OWNER");


        name=binding.name;
        description=binding.description;
        specifics=binding.specifics;
        event=binding.event;
        category=binding.category;
        location=binding.location;
        performers=binding.performers;
        visible=binding.visible;
        available=binding.available;
        cancelationPeriod=binding.cancelationDeadline;
        reservationPeriod=binding.reservationDeadline;
        acceptance=binding.acceptance;
        pricePH=binding.pricePH;
        minDuration=binding.minimumDuration;
        maxDuration=binding.maximumDuration;
        discount=binding.discount;
        discountPrice=binding.discountPrice;


        setData();


        Button btnEdit = (Button) root.findViewById(R.id.btnEdit);
        Button btnDelete = (Button) root.findViewById(R.id.btnDelete);

        if(stringValue.equals("EMPLOYEE")){
            btnEdit.setVisibility(View.INVISIBLE);
            btnDelete.setVisibility(View.INVISIBLE);

        }


        btnEdit.setOnClickListener(v -> {
            FragmentTransition.to(AddServiceFragment.newInstance(servis), getActivity(),
                    true, R.id.list_layout_services);
        });

        btnDelete.setOnClickListener(v -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage("Are you sure you want to delete this service?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ServiceRepository.deleteService(servis.getId());
                            FragmentTransition.to(ServicesPageFragment.newInstance(),getActivity() ,
                                    true, R.id.list_layout_services);
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = dialog.create();
            alert.show();
        });

        for (String imageUrl : servis.getImageUris()) {
            ImageView imageView = new ImageView(getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    300, 300
            );
            layoutParams.setMargins(0, 0, 10, 0);
            imageView.setLayoutParams(layoutParams);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Picasso.get().load(imageUrl).into(imageView);
            photoLinearLayout.addView(imageView);
        }
        return  root;
    }


    private void setData(){
        name.setText(servis.getName());
        description.setText(servis.getDescription());
        location.setText(servis.getLocation());
        specifics.setText(servis.getLocation());
        CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                for(Category c:categories)
                    if(c.getId().equals(servis.getCategory()))
                        SubCategoryRepository.getAllSubcategories(new SubCategoryRepository.SubcategoryFetchCallback() {
                            @Override
                            public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {
                                for(SubCategory s:subcategories)
                                    if(s.getId().equals(servis.getSubcategory())) {
                                        category.setText(c.getName()+" - "+s.getName());
                                    }

                            }
                        });
            }
        });

//        EventTypeRepo eventTypeRepo=new EventTypeRepo();
//        eventTypeRepo.getAllActicateEventTypes(new EventTypeRepo.EventTypeFetchCallback() {
//            @Override
//            public void onEventTypeFetch(ArrayList<EventType> eventTypes) {
//                String events = "";
//
//                if (eventTypes != null) {
//                    for(EventType e:eventTypes)
//                        for(String id:servis.getType())
//                            if(e.getId().equals(id))
//                                events +=e.getName()+"   ";
//                    event.setText(events);
//
//                }
//            }
//        });

//        EmployeeRepo employeeRepo=new EmployeeRepo();
//        employeeRepo.getAll(new EmployeeRepo.EmployeeFetchCallback() {
//            @Override
//            public void onEmployeeFetch(ArrayList<Employee> employees) {
//                String attendants = "";
//
//                if (employees != null) {
//                    for(Employee e:employees)
//                        for(String id:servis.getAttendants())
//                            if(e.getId().equals(id))
//                                attendants +=e.getFirstName()+" "+e.getLastName()+"   ";
//                    performers.setText(attendants);
//
//                }
//            }
//        });
        if(servis.getVisible())
            visible.setText("Da");
        else
            visible.setText("Ne");
        if(servis.getAvailable())
            available.setText("Da");
        else
            available.setText("Ne");
        if(servis.getManualConfirmation())
            acceptance.setText("Ručno");
        else
            acceptance.setText("Automatski");
        cancelationPeriod.setText(servis.getCancellationDeadline()+" d");
        reservationPeriod.setText(servis.getReservationDeadline()+" d");
        minDuration.setText(servis.getMinDuration()+" h");
        maxDuration.setText(servis.getMaxDuration()+" h");
        pricePH.setText(servis.getPricePerHour()+" din");
        discount.setText(servis.getDiscount()+" %");
        discountPrice.setText(Math.round(servis.getPricePerHour()*(1- servis.getDiscount()/100))+" din");
    }
}