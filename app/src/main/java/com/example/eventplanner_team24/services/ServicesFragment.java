package com.example.eventplanner_team24.services;

import static android.content.Context.MODE_PRIVATE;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.ListFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentServicesBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.repositories.EventTypeRepo;
import com.example.eventplanner_team24.repositories.ServiceRepository;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServicesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServicesFragment extends ListFragment {

    private FragmentServicesBinding binding;
    private ServiceListAdapter adapter;
    private ServiceRepository serviceRepository;
    private ArrayList<Category> selectedCategories=new ArrayList<>();
    private ArrayList<SubCategory> selectedSubcategories=new ArrayList<>();
    private ArrayList<EventType> selectedEventTypes=new ArrayList<>();
    private EditText minPrice;
    private EditText maxPrice;
    private RadioButton availableService;
    private RadioButton notAvailableService;
    private RadioButton allServices;
    private EditText performers;
    private String minPriceText="";
    private String maxPriceText="";
    private String performersText="";
    private int availableServiceFilterState=2; //0 available 1 not available 2 all
    public ServicesFragment() {
        // Required empty public constructor
    }

    public static ServicesFragment newInstance() {
        ServicesFragment fragment = new ServicesFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceRepository=new ServiceRepository();
        serviceRepository.getAllServices(new ServiceRepository.ServiceFetchCallback() {
            @Override
            public void onServiceFetch(ArrayList<Service> services) {
                if (services != null) {
                    adapter=new ServiceListAdapter(getActivity(),services);
                    setListAdapter(adapter);
                }
            }
        }) ;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentServicesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        FragmentActivity currentActivity = getActivity();

        FloatingActionButton addButton = binding.floatingActionButtonServices;
        addButton.setVisibility(View.VISIBLE);


        //************************************FILTERS*****************************************************

        Button btnFilters = (Button) root.findViewById(R.id.btnFilters);
        btnFilters.setOnClickListener(v -> {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity());
            View dialogView = getLayoutInflater().inflate(R.layout.service_filter, null);
            bottomSheetDialog.setContentView(dialogView);


            minPrice=dialogView.findViewById(R.id.priceServiceMin);
            maxPrice=dialogView.findViewById(R.id.priceServiceMax);
            allServices=dialogView.findViewById(R.id.allService);
            availableService=dialogView.findViewById(R.id.availableService);
            notAvailableService=dialogView.findViewById(R.id.notAvailableService);
            performers=dialogView.findViewById(R.id.editTextFilterPerformers);

            minPrice.setText(minPriceText);
            maxPrice.setText(maxPriceText);
            performers.setText(performersText);
            if(availableServiceFilterState==0)
                availableService.setChecked(true);
            else if(availableServiceFilterState==1)
                notAvailableService.setChecked(true);
            else
                allServices.setChecked(true);

            Spinner spinnerCat = dialogView.findViewById(R.id.spinCat);
            CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
                @Override
                public void onCategoryFetch(ArrayList<Category> categories) {
                    ArrayAdapter<Category> arrayAdapter = new ArrayAdapter<Category>(getActivity(), R.layout.multispiner, categories) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            Category category = getItem(position);
                            textView.setText(category.getName());
                            for(Category c:selectedCategories)
                                if(c.getId().equals(category.getId()))
                                    checkBox.setChecked(true);
                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    Category selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedCategories.add(selectedItem);
                                    } else {
                                        ArrayList<Category> varCategories=new ArrayList<>();
                                        for(Category c:selectedCategories)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varCategories.add(c);

                                        selectedCategories.clear();
                                        selectedCategories.addAll(varCategories);                                             }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerCat.setAdapter(arrayAdapter);
                }
            });

            Spinner spinnerSubCat = dialogView.findViewById(R.id.spinSubcat);
            SubCategoryRepository.getAllSubcategories(new SubCategoryRepository.SubcategoryFetchCallback() {
                @Override
                public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {

                    ArrayAdapter<SubCategory> arrayAdapterSub = new ArrayAdapter<SubCategory>(getActivity(), R.layout.multispiner, subcategories) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            SubCategory subCategory = getItem(position);
                            textView.setText(subCategory.getName());
                            for(SubCategory c:selectedSubcategories)
                                if(c.getId().equals(subCategory.getId()))
                                    checkBox.setChecked(true);
                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    SubCategory selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedSubcategories.add(selectedItem);
                                    } else {
                                        ArrayList<SubCategory> varSubcategories=new ArrayList<>();
                                        for(SubCategory c:selectedSubcategories)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varSubcategories.add(c);

                                        selectedSubcategories.clear();
                                        selectedSubcategories.addAll(varSubcategories);                                             }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerSubCat.setAdapter(arrayAdapterSub);
                }
            });

            Spinner spinnerEvent = dialogView.findViewById(R.id.spinEvent);
            EventTypeRepo.getAllEventTypes(new EventTypeRepo.EventTypeFetchCallback() {
                @Override
                public void onEventTypeFetch(ArrayList<EventType> eventTypes) {
                    ArrayAdapter<EventType> arrayAdapterEvent = new ArrayAdapter<EventType>(getActivity(), R.layout.multispiner, eventTypes) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            EventType eventType = getItem(position);
                            textView.setText(eventType.getName());
                            for(EventType c:selectedEventTypes)
                                if(c.getId().equals(eventType.getId()))
                                    checkBox.setChecked(true);
                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    EventType selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedEventTypes.add(selectedItem);
                                    } else {
                                        ArrayList<EventType> varEventTypes=new ArrayList<>();
                                        for(EventType c:selectedEventTypes)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varEventTypes.add(c);

                                        selectedEventTypes.clear();
                                        selectedEventTypes.addAll(varEventTypes);                                             }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerEvent.setAdapter(arrayAdapterEvent);
                }
            });

            Button apply=dialogView.findViewById(R.id.applyFilterService);
            apply.setOnClickListener(a->{

                minPriceText=minPrice.getText().toString();
                maxPriceText=maxPrice.getText().toString();
                performersText=performers.getText().toString();
                Predicate<Service> combinedPredicate = p -> true;

                if(availableService.isChecked()) {
                    availableServiceFilterState = 0;
                    combinedPredicate = combinedPredicate.and(Service::getAvailable);
                }
                else if (notAvailableService.isChecked()) {
                    availableServiceFilterState = 1;
                    combinedPredicate = combinedPredicate.and(p-> !p.getAvailable());
                }
                else
                    availableServiceFilterState=2;

                if (!minPriceText.isEmpty()) {
                    combinedPredicate = combinedPredicate.and(p -> p.getPricePerHour() > Double.parseDouble(minPriceText));
                }
                if (!maxPriceText.isEmpty()) {
                    combinedPredicate = combinedPredicate.and(p -> p.getPricePerHour() < Double.parseDouble(maxPriceText));
                }

                if (!performersText.isEmpty()) {
                    String desc = performersText.toString();
                    combinedPredicate = combinedPredicate.and(p ->{
                        for(String performer:p.getAttendants()){
                            if(performer.toLowerCase().contains(desc.toLowerCase()))
                                return true;
                        }
                        return false;
                    });

                }


                Predicate<Service> finalCombinedPredicate = combinedPredicate;
                serviceRepository.getAllServices(new ServiceRepository.ServiceFetchCallback() {
                    @Override
                    public void onServiceFetch(ArrayList<Service> services) {
                        if(services!=null){
                            List<Service> filteredList=services.stream()
                                    .filter(finalCombinedPredicate)
                                    .collect(Collectors.toList());

                            if (getActivity() != null) {
                                List<Service> finalFilteredList = filterByCategorySubcategoryEvents(filteredList);
                                getActivity().runOnUiThread(() -> {
                                    adapter = new ServiceListAdapter(getActivity(), new ArrayList<>(finalFilteredList));
                                    setListAdapter(adapter);
                                });
                            }
                        }
                    }

                });

                bottomSheetDialog.dismiss();

            });
            Button discard=dialogView.findViewById(R.id.discardFilterService);
            discard.setOnClickListener(d->{
                serviceRepository.getAllServices(new ServiceRepository.ServiceFetchCallback() {
                    @Override
                    public void onServiceFetch(ArrayList<Service> services) {
                        if (services != null) {
                            adapter=new ServiceListAdapter(getActivity(),services);
                            setListAdapter(adapter);
                        }
                    }
                }) ;
                selectedEventTypes.clear();
                selectedSubcategories.clear();
                selectedCategories.clear();
                minPriceText="";
                maxPriceText="";
                performersText="";
                availableServiceFilterState=2;
                bottomSheetDialog.dismiss();
            });



            bottomSheetDialog.show();
        });


        addButton.setOnClickListener(v -> {
            FragmentTransition.to(AddServiceFragment.newInstance(null), currentActivity,
                    true, R.id.list_layout_services);
        });

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);



        //******************************** HIDE BTN *******************************

        SharedPreferences prefs = requireContext().getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String stringValue = prefs.getString("key_string", "OWNER");

        if(stringValue.equals("EMPLOYEE"))
        {
            addButton.setVisibility(View.GONE);
        }


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private List<Service> filterByCategorySubcategoryEvents(List<Service> filteredList){
        List<Service> filtered=new ArrayList<>();

        if(selectedCategories.size()>0){
            for(Category category:selectedCategories) {
                for (Service s : filteredList) {
                    if (s.getCategory().equals(category.getId()) && !filtered.contains(s))
                        filtered.add(s);
                }
            }

            filteredList.clear();
            filteredList.addAll(filtered);
            filtered.clear();
        }


        if(selectedSubcategories.size()>0) {
            for (SubCategory subcategory : selectedSubcategories)
                for (Service s : filteredList)
                    if (s.getSubcategory().equals(subcategory.getId()) && !filtered.contains(s))
                        filtered.add(s);
            filteredList.clear();
            filteredList.addAll(filtered);
            filtered.clear();
        }

        if(selectedEventTypes.size()>0){
            for(EventType event:selectedEventTypes)
                for(Service s:filteredList)
                    for(String type:s.getType())
                        if(type.equals(event.getId()) && !filtered.contains(s))
                            filtered.add(s);
            filteredList.clear();
            filteredList.addAll(filtered);
        }
        return filteredList;
    }
}