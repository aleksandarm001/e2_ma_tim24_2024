package com.example.eventplanner_team24.services;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentServicesPageBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServicesPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServicesPageFragment extends Fragment {

    public static ArrayList<Service> services = new ArrayList<Service>();
    private FragmentServicesPageBinding binding;

    public ServicesPageFragment() {
        // Required empty public constructor
    }

    public static ServicesPageFragment newInstance() {
        ServicesPageFragment fragment = new ServicesPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();

        binding = FragmentServicesPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        FragmentTransition.to(ServicesFragment.newInstance(), currentActivity, true, R.id.list_layout_services);
        return root;
    }

    public void onDestroyView() {
        super.onDestroyView();
        services=new ArrayList<Service>();
    }
}