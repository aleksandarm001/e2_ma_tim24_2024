package com.example.eventplanner_team24.packages;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.ListFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentPackagesBinding;
import com.example.eventplanner_team24.databinding.FragmentProductsBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Package;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.products.AddProductFragment;
import com.example.eventplanner_team24.repositories.EventTypeRepo;
import com.example.eventplanner_team24.repositories.PackageRepository;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PackagesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PackagesFragment extends ListFragment {

    private FragmentPackagesBinding binding;
    private PackageListAdapter adapter;
    private PackageRepository packageRepository;
    private SearchView searchName;
    private ArrayList<Category> selectedCategories=new ArrayList<>();
    private ArrayList<SubCategory> selectedSubcategories=new ArrayList<>();
    private ArrayList<EventType> selectedEventTypes=new ArrayList<>();
    private EditText minPrice;
    private EditText maxPrice;
    private String minPriceText="";
    private String maxPriceText="";

    public PackagesFragment() {
        // Required empty public constructor
    }


    public static PackagesFragment newInstance() {
        PackagesFragment fragment = new PackagesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        packageRepository=new PackageRepository();
        packageRepository.getAllPackages(new PackageRepository.PackageFetchCallback() {
            @Override
            public void onPackageFetch(ArrayList<Package> packages) {
                if (packages != null) {
                    adapter=new PackageListAdapter(getActivity(),packages);
                    setListAdapter(adapter);
                }
            }
        }); ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPackagesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

//        searchName=binding.searchText;
        packageRepository=new PackageRepository();


        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);


        FragmentActivity currentActivity = getActivity();
        FloatingActionButton addButton = binding.floatingActionButtonPackages;
        addButton.setVisibility(View.VISIBLE);


        //**********************FILTERS**************************************
        Button btnFilters = (Button) root.findViewById(R.id.btnFilters);
        btnFilters.setOnClickListener(v -> {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity());
            View dialogView = getLayoutInflater().inflate(R.layout.package_filter, null);
            bottomSheetDialog.setContentView(dialogView);
            minPrice=dialogView.findViewById(R.id.minPrice);
            maxPrice=dialogView.findViewById(R.id.pricePackageMax);
            minPrice.setText(minPriceText);
            maxPrice.setText(maxPriceText);


            Spinner spinnerCat = dialogView.findViewById(R.id.spinCat);
            CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
                @Override
                public void onCategoryFetch(ArrayList<Category> categories) {
                    ArrayAdapter<Category> arrayAdapter = new ArrayAdapter<Category>(getActivity(), R.layout.multispiner, categories) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            Category category = getItem(position);
                            textView.setText(category.getName());
                            for(Category c:selectedCategories)
                                if(c.getId().equals(category.getId()))
                                    checkBox.setChecked(true);
                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    Category selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedCategories.add(selectedItem);
                                    } else {
                                        ArrayList<Category> varCategories=new ArrayList<>();
                                        for(Category c:selectedCategories)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varCategories.add(c);

                                        selectedCategories.clear();
                                        selectedCategories.addAll(varCategories);                                             }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerCat.setAdapter(arrayAdapter);
                }
            });

            Spinner spinnerSubCat = dialogView.findViewById(R.id.spinSubcat);
            SubCategoryRepository.getAllSubcategories(new SubCategoryRepository.SubcategoryFetchCallback() {
                @Override
                public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {

                    ArrayAdapter<SubCategory> arrayAdapterSub = new ArrayAdapter<SubCategory>(getActivity(), R.layout.multispiner, subcategories) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            SubCategory subCategory = getItem(position);
                            textView.setText(subCategory.getName());
                            for(SubCategory c:selectedSubcategories)
                                if(c.getId().equals(subCategory.getId()))
                                    checkBox.setChecked(true);
                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    SubCategory selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedSubcategories.add(selectedItem);
                                    } else {
                                        ArrayList<SubCategory> varSubcategories=new ArrayList<>();
                                        for(SubCategory c:selectedSubcategories)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varSubcategories.add(c);

                                        selectedSubcategories.clear();
                                        selectedSubcategories.addAll(varSubcategories);                                             }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerSubCat.setAdapter(arrayAdapterSub);
                }
            });

            Spinner spinnerEvent = dialogView.findViewById(R.id.spinEvent);
            EventTypeRepo.getAllEventTypes(new EventTypeRepo.EventTypeFetchCallback() {
                @Override
                public void onEventTypeFetch(ArrayList<EventType> eventTypes) {
                    ArrayAdapter<EventType> arrayAdapterEvent = new ArrayAdapter<EventType>(getActivity(), R.layout.multispiner, eventTypes) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            EventType eventType = getItem(position);
                            textView.setText(eventType.getName());
                            for(EventType c:selectedEventTypes)
                                if(c.getId().equals(eventType.getId()))
                                    checkBox.setChecked(true);
                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    EventType selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedEventTypes.add(selectedItem);
                                    } else {
                                        ArrayList<EventType> varEventTypes=new ArrayList<>();
                                        for(EventType c:selectedEventTypes)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varEventTypes.add(c);

                                        selectedEventTypes.clear();
                                        selectedEventTypes.addAll(varEventTypes);                                             }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerEvent.setAdapter(arrayAdapterEvent);
                }
            });

            Button apply=dialogView.findViewById(R.id.applyBtn);
            apply.setOnClickListener(a->{

                minPriceText=minPrice.getText().toString();
                maxPriceText=maxPrice.getText().toString();
                Predicate<Package> combinedPredicate = p -> true;


                if (!minPriceText.isEmpty()) {
                    combinedPredicate = combinedPredicate.and(p -> p.getPrice() > Double.parseDouble(minPriceText));
                }
                if (!maxPriceText.isEmpty()) {
                    combinedPredicate = combinedPredicate.and(p -> p.getPrice() < Double.parseDouble(maxPriceText));
                }



                Predicate<Package> finalCombinedPredicate = combinedPredicate;
                packageRepository.getAllPackages(new PackageRepository.PackageFetchCallback() {
                    @Override
                    public void onPackageFetch(ArrayList<Package> packages) {
                        if(packages!=null){
                            List<Package> filteredList=packages.stream()
                                    .filter(finalCombinedPredicate)
                                    .collect(Collectors.toList());

                            if (getActivity() != null) {
                                List<Package> finalFilteredList = filterByCategorySubcategoryEvents(filteredList);
                                getActivity().runOnUiThread(() -> {
                                    adapter = new PackageListAdapter(getActivity(), new ArrayList<>(finalFilteredList));
                                    setListAdapter(adapter);
                                });
                            }
                        }
                    }

                });

                bottomSheetDialog.dismiss();

            });
            Button discard=dialogView.findViewById(R.id.discradBtn);
            discard.setOnClickListener(d->{
                packageRepository.getAllPackages(new PackageRepository.PackageFetchCallback() {
                    @Override
                    public void onPackageFetch(ArrayList<Package> packages) {
                        if (packages != null) {
                            adapter=new PackageListAdapter(getActivity(),packages);
                            setListAdapter(adapter);
                        }
                    }
                }); ;
                selectedEventTypes.clear();
                selectedSubcategories.clear();
                selectedCategories.clear();
                minPriceText="";
                maxPriceText="";

                bottomSheetDialog.dismiss();
            });




            bottomSheetDialog.show();
        });


        addButton.setOnClickListener(v -> {
            FragmentTransition.to(AddPackageFragment.newInstance(null), getActivity(),
                    true, R.id.list_layout_packages);
        });

        return root;
    }

    private List<Package> filterByCategorySubcategoryEvents(List<Package> filteredList){
        List<Package> filtered=new ArrayList<>();

        if(selectedCategories.size()>0){
            for(Category category:selectedCategories) {
                for (Package s : filteredList) {
                    if (s.getCategory().equals(category.getId()) && !filtered.contains(s))
                        filtered.add(s);
                }
            }

            filteredList.clear();
            filteredList.addAll(filtered);
            filtered.clear();
        }


        if(selectedSubcategories.size()>0) {
            for (SubCategory subcategory : selectedSubcategories)
                for (Package s : filteredList)
                    for(String id:s.getSubcategories())
                        if(id.equals(subcategory.getId()) && !filtered.contains(s))
                            filtered.add(s);
            filteredList.clear();
            filteredList.addAll(filtered);
            filtered.clear();
        }

        if(selectedEventTypes.size()>0){
            for(EventType event:selectedEventTypes)
                for(Package s:filteredList)
                    for(String type:s.getType())
                        if(type.equals(event.getId()) && !filtered.contains(s))
                            filtered.add(s);
            filteredList.clear();
            filteredList.addAll(filtered);
        }
        return filteredList;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}