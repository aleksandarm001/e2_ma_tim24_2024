package com.example.eventplanner_team24.packages;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.HomeActivity;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Package;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PackageListAdapter extends ArrayAdapter<Package> {
    private ArrayList<Package> aPackage;
    private FragmentActivity mContext;

    public PackageListAdapter(FragmentActivity context, ArrayList<Package> packages){
        super(context, R.layout.product_list_item);
        mContext = context;
        aPackage = packages;
    }

    @Override
    public int getCount() {
        return aPackage.size();
    }

    @Nullable
    @Override
    public Package getItem(int position) {
        return aPackage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Package p = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.package_list_item,
                    parent, false);
        }
        LinearLayout productCard = convertView.findViewById(R.id.package_list_item);
        ImageView imageView = convertView.findViewById(R.id.package_image);
        TextView productTitle = convertView.findViewById(R.id.package_name);
        TextView productCategory = convertView.findViewById(R.id.package_category);
        TextView productPrice = convertView.findViewById(R.id.package_price);
        TextView productAvailable = convertView.findViewById(R.id.package_available);


        if(p != null){
            if (!p.getImageUris().isEmpty()) {
                Picasso.get().load(p.getImageUris().get(0)).into(imageView);
            }              productTitle.setText(p.getName());
            CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
                @Override
                public void onCategoryFetch(ArrayList<Category> categories) {
                    for(Category c:categories)
                        if(c.getId().equals(p.getCategory()))
                            productCategory.setText(c.getName());
                }
            });
            productPrice.setText(Double.toString(p.getPrice())+" din");
            if(p.getAvailable())
                productAvailable.setText("Dostupan");
            else
                productAvailable.setText("Nedostupan");
            productCard.setOnClickListener(v -> {
                if (getContext() instanceof HomeActivity) {
                    HomeActivity activity = (HomeActivity) getContext();
                    FragmentTransition.to(PackageDetails.newInstance(p),activity ,
                            true, R.id.list_layout_packages);
                }
            });
        }

        return convertView;
    }
}
