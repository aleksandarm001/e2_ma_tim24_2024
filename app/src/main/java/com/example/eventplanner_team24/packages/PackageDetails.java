package com.example.eventplanner_team24.packages;

import static android.content.Context.MODE_PRIVATE;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentPackageDetailsBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.products.ProductPackageListAdapter;
import com.example.eventplanner_team24.repositories.EventTypeRepo;
import com.example.eventplanner_team24.repositories.PackageRepository;
import com.example.eventplanner_team24.repositories.ProductRepository;
import com.example.eventplanner_team24.model.Package;
import com.example.eventplanner_team24.repositories.ServiceRepository;
import com.example.eventplanner_team24.services.ServicePackageListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PackageDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PackageDetails extends Fragment {

    private FragmentPackageDetailsBinding binding;
    private ProductPackageListAdapter productPackageListAdapter;
    private ServicePackageListAdapter servicePackageListAdapter;

    private ProductRepository productRepository=new ProductRepository();
    private ServiceRepository serviceRepository=new ServiceRepository();
    private TextView name;
    private TextView description;
    private TextView category;
    private TextView events;
    private TextView available;
    private TextView price;
    private TextView discount;
    private TextView discountPrice;
    private TextView visible;
    private TextView cancelationDeadline;
    private TextView reservationDeadlilne;
    private ListView productsList;
    private ListView servicesList;

    private Package paket;

    private List<Uri> selectedUriImages;

    public PackageDetails() {
        // Required empty public constructor
    }

    public static PackageDetails newInstance(Package p) {
        PackageDetails fragment = new PackageDetails();
        Bundle args = new Bundle();
        args.putParcelable("PACKAGE", p);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            paket = getArguments().getParcelable("PACKAGE");
        }
        selectedUriImages = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentPackageDetailsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        name=binding.packageName;
        description=binding.packageDescription;
        available=binding.packageAvailable;
        visible=binding.packageVisible;
        price=binding.packagePrice;
        category=binding.packageCategory;
        events=binding.packageEventType;
        discount=binding.packageDiscount;
        discountPrice=binding.packageDiscountPrice;
        cancelationDeadline=binding.packageCancelationDeadline;
        reservationDeadlilne=binding.packageReservationDeadline;
        productsList=root.findViewById(R.id.listPackages);
        servicesList=root.findViewById(R.id.listServices);

        SetData();

        List<Uri> selectedUriImages = new ArrayList<>();
        SharedPreferences prefs = requireContext().getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String stringValue = prefs.getString("key_string", "OWNER");

        Button btnEdit = (Button) root.findViewById(R.id.btnEdit);
        Button btnDelete = (Button) root.findViewById(R.id.btnDelete);

        if(stringValue.equals("EMPLOYEE")){
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);

        }


        btnDelete.setOnClickListener(v -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

            dialog.setMessage("Are you sure you want to delete this package?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            PackageRepository.deletePacakge(paket.getId());
                            FragmentTransition.to(PackagesFragment.newInstance(),getActivity() ,
                                    true, R.id.list_layout_packages);
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = dialog.create();
            alert.show();
        });
        btnEdit.setOnClickListener(v->{
            FragmentTransition.to(AddPackageFragment.newInstance(paket),getActivity(),true,R.id.list_layout_packages);
        });

        return root;

    }


    private void SetData(){
        name.setText(paket.getName());
        description.setText(paket.getDescription());
        if(paket.getAvailable())
            available.setText("Da");
        else
            available.setText("Ne");
        if(paket.getVisible())
            visible.setText("Da");
        else
            visible.setText("Ne");
        price.setText(paket.getPrice()+" din");
        CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                for(Category c:categories)
                    if(c.getId().equals(paket.getCategory()))
                        category.setText(c.getName());
            }
        });
        EventTypeRepo eventTypeRepo=new EventTypeRepo();
        eventTypeRepo.getAllActivateEventTypes(new EventTypeRepo.EventTypeFetchCallback() {
            @Override
            public void onEventTypeFetch(ArrayList<EventType> eventTypes) {
                String event = "";
                if (eventTypes != null) {
                    for(EventType e:eventTypes)
                        for(String id:paket.getType())
                            if(e.getId().equals(id) && !event.contains(e.getName()+"   "))
                                event +=e.getName()+"   ";
                    events.setText(event);

                }
            }
        });
        discount.setText(paket.getDiscount()+" %");
        discountPrice.setText(Math.round(paket.getPrice()*(1-paket.getDiscount()/100))+" din");
        cancelationDeadline.setText(paket.getCancellationDeadline()+ "d");
        reservationDeadlilne.setText(paket.getReservationDeadline()+" d");



        productRepository.getAllProducts(new ProductRepository.ProductFetchCallback() {
            @Override
            public void onProductFetch(ArrayList<Product> products) {
                if (products != null) {
                    ArrayList<Product> packageProducts=new ArrayList<>();
                    for(String id:paket.getProducts()) {
                        for (Product product : products) {
                            if (product.getId().equals(id))
                                packageProducts.add(product);
                        }
                    }
                    productPackageListAdapter=new ProductPackageListAdapter(getActivity(),packageProducts);
                    productsList.setAdapter(productPackageListAdapter);
                }
            }
        });
        serviceRepository.getAllServices(new ServiceRepository.ServiceFetchCallback() {
            @Override
            public void onServiceFetch(ArrayList<Service> services) {
                if (services != null) {
                    ArrayList<Service> packageServices=new ArrayList<>();
                    for(String id:paket.getServices()) {
                        for (Service s : services) {
                            if (s.getId().equals(id))
                                packageServices.add(s);
                        }
                    }
                    servicePackageListAdapter=new ServicePackageListAdapter(getActivity(),packageServices);

                    servicesList.setAdapter(servicePackageListAdapter);
                }
            }
        });

    }
}