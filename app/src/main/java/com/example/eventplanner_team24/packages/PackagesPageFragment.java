package com.example.eventplanner_team24.packages;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentPackagesPageBinding;
import com.example.eventplanner_team24.databinding.FragmentProductsPageBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.model.Package;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.packages.PackagesPageFragment;
import com.example.eventplanner_team24.products.ProductsFragment;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PackagesPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PackagesPageFragment extends Fragment {
    private FragmentPackagesPageBinding binding;

    public PackagesPageFragment() {
        // Required empty public constructor
    }

    public static PackagesPageFragment newInstance() {
        PackagesPageFragment fragment = new PackagesPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();
        binding = FragmentPackagesPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        FragmentTransition.to(PackagesFragment.newInstance(),currentActivity,true,R.id.list_layout_packages);



        return root;
    }
}