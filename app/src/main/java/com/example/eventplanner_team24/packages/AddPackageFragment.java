package com.example.eventplanner_team24.packages;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentAddPackageBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.model.Package;
import com.example.eventplanner_team24.model.Service;
import com.example.eventplanner_team24.products.ProductListForPackageAdapter;
import com.example.eventplanner_team24.repositories.ServiceRepository;
import com.example.eventplanner_team24.services.ServiceListForPackageAdapter;
import com.example.eventplanner_team24.repositories.PackageRepository;
import com.example.eventplanner_team24.repositories.ProductRepository;

import java.util.ArrayList;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddPackageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddPackageFragment extends Fragment implements ProductListForPackageAdapter.OnProductSelectionListener {

    private ProductListForPackageAdapter productPackageListAdapter;
    private ServiceListForPackageAdapter servicePackageListAdapter;
    private FragmentAddPackageBinding binding;
    private boolean isFirstSelection = true;
    private TextView priceTextView;
    private double price=0;
    private EditText packageName;
    private EditText description;
    private EditText discount;
    private RadioButton visible;
    private RadioButton available;
    private RadioButton notVisible;
    private RadioButton notAvailable;
    private String name;
    private String category;
    private ProductRepository productRepository;
    private ServiceRepository serviceRepository = new ServiceRepository();
    private ArrayList<Product> addedProducts=new ArrayList<>();
    private ArrayList<Service> addedServices=new ArrayList<>();

    private Package newPackage;
    private ArrayList<Product> checkedProducts=new ArrayList<Product>();
    private ArrayList<Service> checkedService=new ArrayList<>();
    private TextView cancelationPeriod;
    private TextView reservationPeriod;
    private double productsPrice=0;
    private double servicePrice=0;
    private Package paket;
    private Spinner spinnerCategory;
    private TextView categoryTw;


    public AddPackageFragment() {
        // Required empty public constructor
    }

    public static AddPackageFragment newInstance(Package p) {
        AddPackageFragment fragment = new AddPackageFragment();
        Bundle args = new Bundle();
        args.putParcelable("PACKAGE", p);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            paket = getArguments().getParcelable("PACKAGE");
            newPackage = getArguments().getParcelable("PACKAGE");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.fragment_package_form, container, false);
        binding = FragmentAddPackageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        ArrayList<Service> services=new ArrayList<>();
        ArrayList<Product> products=new ArrayList<Product>();
        packageName=binding.packageName;
        description=binding.packageDescription;
        discount=binding.packageDiscount;
        available=binding.availablePackage;
        notAvailable=binding.notAvailablePackage;
        visible=binding.visiblePackage;
        notVisible=binding.notVisiblePackage;
        cancelationPeriod=binding.cancelationPeriodPackage;
        reservationPeriod=binding.reservationPeriodPackage;
        spinnerCategory = binding.spinnerPackageCategory;
        categoryTw=binding.categoryTw;
        priceTextView=binding.packagePrice;

        if(paket==null)
            SetCreatingState();
        else
            SetUpdatingState();




        ListView productsList=root.findViewById(R.id.listPackages);
        ListView servicesList=root.findViewById(R.id.listServices);

        Button btnProducts = (Button) root.findViewById(R.id.addProductToPackage);
        Button btnServices = (Button) root.findViewById(R.id.addServiceToPackage);


        priceTextView=(TextView)root.findViewById(R.id.packagePrice);


        btnProducts.setOnClickListener(v -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

            dialog.setMessage("Check products you want to add to package");
            View dialogView = inflater.inflate(R.layout.dialog_products, null);
            dialog.setView(dialogView);

            ListView listViewProducts = dialogView.findViewById(R.id.listViewProducts);
            productRepository=new ProductRepository();
            productRepository.getAllProducts(new ProductRepository.ProductFetchCallback() {
                @Override
                public void onProductFetch(ArrayList<Product> products) {
                    if (products != null) {
                        ArrayList<Product> productsForCategory=new ArrayList<>();
                        for(Product p:products) {
                            if(category.equals(p.getCategory()))
                                productsForCategory.add(p);
                        }

                        ProductListForPackageAdapter productAdapter = new ProductListForPackageAdapter(getActivity(), productsForCategory,addedProducts,paket,AddPackageFragment.this);
                        productAdapter.setValueFromEditText(name);


                        listViewProducts.setAdapter(productAdapter);
                    }
                }
            });


            AlertDialog alert = dialog.create();
            alert.show();
        });

        btnServices.setOnClickListener(v -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage("Check service you want to add to package");
            View dialogView = inflater.inflate(R.layout.dialog_products, null);
            dialog.setView(dialogView);

            ListView listViewProducts = dialogView.findViewById(R.id.listViewProducts);

            serviceRepository.getAllServices(new ServiceRepository.ServiceFetchCallback() {
                @Override
                public void onServiceFetch(ArrayList<Service> services) {
                    if (services != null) {
                        ArrayList<Service> servicesForCatgory=new ArrayList<>();
                        for(Service s:services) {
                            if(category.equals(s.getCategory()))
                                servicesForCatgory.add(s);
                        }
                        Log.i("IIIIIIIIII",servicesForCatgory.size()+"");
                        ServiceListForPackageAdapter serviceAdapter = new ServiceListForPackageAdapter(getActivity(), servicesForCatgory,addedServices,paket,AddPackageFragment.this);
                        serviceAdapter.setValueFromEditText(name);
                        listViewProducts.setAdapter(serviceAdapter);

                    }
                }
            });


            AlertDialog alert = dialog.create();
            alert.show();
        });

        Button submit=binding.btnSubmit;
        submit.setOnClickListener(v->{
            if(paket==null) {
                if (addPackage()) {
                    FragmentTransition.to(PackagesFragment.newInstance(), getActivity(),
                            true, R.id.list_layout_packages);
                }
            }else{
                if(updatePackage()){
                    PackageRepository.updatePackage(paket);
                    FragmentTransition.to(PackagesFragment.newInstance(), getActivity(),
                            true, R.id.list_layout_packages);
                }
            }
        });

        return root;

    }
    @Override
    public void onProductsSelected(ArrayList<Product> selectedProducts) {
        productsPrice=0;
        for(Product p:selectedProducts)
            productsPrice+=p.getPrice();
        price=productsPrice+servicePrice;
        priceTextView.setText(Double.toString(price)+" din");
        addedProducts.clear();
        addedProducts.addAll(selectedProducts);

        for(Product p: addedProducts)
            Log.i("KOLICINAE",addedProducts.size()+" "+p.toString());

    }
    @Override
    public void onServicesSelected(ArrayList<Service> selectedServices) {
        servicePrice=0;
        for(Service s:selectedServices)
            servicePrice+=s.getPrice();
        price=servicePrice+productsPrice;
        priceTextView.setText(Double.toString(price)+" din");
        addedServices.clear();
        addedServices.addAll(selectedServices);
        if(addedServices.size()>0) {
            double minCancelationPeriod = addedServices.get(0).getCancellationDeadline();
            double minReservationPeriod = addedServices.get(0).getReservationDeadline();


            for (Service s : addedServices) {
                if (s.getCancellationDeadline() < minCancelationPeriod)
                    minCancelationPeriod = s.getCancellationDeadline();
                if (s.getReservationDeadline() < minReservationPeriod)
                    minReservationPeriod = s.getReservationDeadline();
            }
            cancelationPeriod.setText(minCancelationPeriod + " d");
            reservationPeriod.setText(minReservationPeriod + " d");
        }

    }

    private boolean updatePackage(){
        if(AreFieldsValid()) {
            paket.setName(packageName.getText().toString());
            paket.setDescription(description.getText().toString());
            ArrayList<String> products=new ArrayList<>();
            ArrayList<String> services=new ArrayList<>();
            for(Product p:addedProducts)
                products.add(p.getId());
            for(Service s:addedServices)
                services.add(s.getId());
            paket.setProducts(products);
            paket.setServices(services);
            paket.setPrice(price);
            paket.setDiscount(Double.parseDouble(discount.getText().toString()));
            if(available.isChecked())
                paket.setAvailable(true);
            if(notAvailable.isChecked())
                paket.setAvailable(false);
            if(visible.isChecked())
                paket.setVisible(true);
            if(notVisible.isChecked())
                paket.setVisible(false);
            ArrayList<String> subcategories=new ArrayList<>();
            ArrayList<String> eventTypes=new ArrayList<>();
            ArrayList<String> images=new ArrayList<>();
            for(Product p:addedProducts) {
                subcategories.add(p.getSubcategory());
                for(String event:p.getType()) {
                    boolean contain=false;
                    for (String type : eventTypes)
                        if (type.equals(event)) {
                            contain = true;
                            break;
                        }

                    if(contain==false)
                        eventTypes.add(event);
                }

                images.addAll(p.getImageUris());
            }
            for(Service s:addedServices) {
                subcategories.add(s.getSubcategory());
                for(String event:s.getType()) {
                    boolean contain=false;
                    for (String type : eventTypes)
                        if (type.equals(event)) {
                            contain = true;
                            break;
                        }

                    if(contain==false)
                        eventTypes.add(event);
                }
                images.addAll(s.getImageUris());
            }
            paket.setSubcategories(subcategories);
            paket.setType(eventTypes);
            paket.setImageUris(images);
            if(addedServices.size()>0){
                double minCancelationPeriod=addedServices.get(0).getCancellationDeadline();
                double minReservationPeriod=addedServices.get(0).getReservationDeadline();

                for(Service s:addedServices) {
                    if (s.getCancellationDeadline() < minCancelationPeriod)
                        minCancelationPeriod = s.getCancellationDeadline();
                    if(s.getReservationDeadline()<minReservationPeriod)
                        minReservationPeriod=s.getReservationDeadline();
                    if(s.getManualConfirmation())
                        paket.setManualConfirmation(true);
                }
                paket.setCancellationDeadline(minCancelationPeriod);
                paket.setReservationDeadline(minReservationPeriod);
            }
            return true;
        }
        return false;
    }
    private boolean addPackage(){
        if(AreFieldsValid()) {
            newPackage = new Package();
            newPackage.setName(packageName.getText().toString());
            newPackage.setDescription(description.getText().toString());
            newPackage.setCategory(category);
            ArrayList<String> products=new ArrayList<>();
            ArrayList<String> services=new ArrayList<>();
            for(Product p:addedProducts)
                products.add(p.getId());
            for(Service s:addedServices)
                services.add(s.getId());
            newPackage.setProducts(products);
            newPackage.setServices(services);
            newPackage.setPrice(price);
            if(!discount.getText().toString().isEmpty())
                newPackage.setDiscount(Double.parseDouble(discount.getText().toString()));
            if(available.isChecked())
                newPackage.setAvailable(true);
            if(notAvailable.isChecked())
                newPackage.setAvailable(false);
            if(visible.isChecked())
                newPackage.setVisible(true);
            if(notVisible.isChecked())
                notVisible.setChecked(false);
            ArrayList<String> subcategories=new ArrayList<>();
            ArrayList<String> eventTypes=new ArrayList<>();
            ArrayList<String> images=new ArrayList<>();
            for(Product p:addedProducts) {
                subcategories.add(p.getSubcategory());
                eventTypes.addAll(p.getType());
                images.addAll(p.getImageUris());
            }
            for(Service s:addedServices) {
                subcategories.add(s.getSubcategory());
                eventTypes.addAll(s.getType());
                images.addAll(s.getImageUris());
            }
            newPackage.setSubcategories(subcategories);
            newPackage.setType(eventTypes);
            newPackage.setImageUris(images);
            if(addedServices.size()>0){
                double minCancelationPeriod=addedServices.get(0).getCancellationDeadline();
                double minReservationPeriod=addedServices.get(0).getReservationDeadline();

                for(Service s:addedServices) {
                    if (s.getCancellationDeadline() < minCancelationPeriod)
                        minCancelationPeriod = s.getCancellationDeadline();
                    if(s.getReservationDeadline()<minReservationPeriod)
                        minReservationPeriod=s.getReservationDeadline();
                    if(s.getManualConfirmation())
                        newPackage.setManualConfirmation(true);
                }
                newPackage.setCancellationDeadline(minCancelationPeriod);
                newPackage.setReservationDeadline(minReservationPeriod);
            }


            PackageRepository.createPackage(newPackage);
            return true;
        }
        return false;
    }
    private boolean AreFieldsValid(){
        if(description.getText().toString().isEmpty()){
            description.setError("Description is required.");
            return false;
        }
        if(addedServices.size()==0 && addedProducts.size()==0){
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext()); // Pass your activity or context here

            builder.setTitle("Error!");
            builder.setMessage("Please select at least one product or service.");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            android.app.AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        }
        return true;
    }


    private void SetCreatingState(){
        visible.setChecked(true);
        available.setChecked(true);
        categoryTw.setText("Category");
        CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                if (categories != null) {
                    // Create an adapter for the spinner with the fetched categories
                    ArrayAdapter<Category> arrayAdapter = new ArrayAdapter<>(requireContext(),
                            android.R.layout.simple_spinner_item, categories);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // Set the adapter to the spinner
                    spinnerCategory.setAdapter(arrayAdapter);
                }
            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected category
                Category c = (Category) parent.getItemAtPosition(position);
                category=c.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private static String generateUniqueString() {
        long timestamp = System.currentTimeMillis();

        String timestampString = Long.toString(timestamp);

        Random random = new Random();
        int randomInt = random.nextInt(10000);

        return timestampString + "_" + randomInt;
    }
    private void SetUpdatingState(){
        productRepository=new ProductRepository();
        productRepository.getAllProducts(new ProductRepository.ProductFetchCallback() {
            @Override
            public void onProductFetch(ArrayList<Product> products) {
                if (products != null) {
                    for(String s:paket.getProducts())
                        for(Product p:products)
                            if(p.getId().equals(s)) {
                                productsPrice += p.getPrice();
                                addedProducts.add(p);
                            }
                }
            }
        });
        serviceRepository=new ServiceRepository();
        serviceRepository.getAllServices(new ServiceRepository.ServiceFetchCallback() {
            @Override
            public void onServiceFetch(ArrayList<Service> services) {
                if (services != null) {
                    for(String id:paket.getServices())
                        for(Service s:services)
                            if(s.getId().equals(id)) {
                                addedServices.add(s);
                                servicePrice += s.getPrice();
                            }
                }
            }
        }) ;
        category=paket.getCategory();
        priceTextView.setText(paket.getPrice()+" din");
        packageName.setText(paket.getName());
        cancelationPeriod.setText(paket.getCancellationDeadline()+" d");
        reservationPeriod.setText(paket.getReservationDeadline()+" d");
        description.setText(paket.getDescription());
        discount.setText(Double.toString(paket.getDiscount()));
        spinnerCategory.setVisibility(View.GONE);
        if(paket.getAvailable())
            available.setChecked(true);
        else
            notAvailable.setChecked(true);
        if(paket.getVisible())
            visible.setChecked(true);
        else
            notVisible.setChecked(true);
        categoryTw.setWidth(200);
        CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                for(Category c:categories)
                    if(c.getId().equals(paket.getCategory()))
                        categoryTw.setText("Category   "+c.getName());
            }
        });


    }
}