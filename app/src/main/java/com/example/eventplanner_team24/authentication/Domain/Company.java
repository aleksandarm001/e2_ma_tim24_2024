package com.example.eventplanner_team24.authentication.Domain;

import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;

import java.util.ArrayList;

public class Company {
    private String companyName;
    private String companyEmail;
    private String companyAddress;
    private String companyPhoneNumber;
    private String aboutCompany;
    private ArrayList<String> workingHours;
    private ArrayList<Category> categories;
    private ArrayList<EventType> eventTypeTypes;

    public Company(String companyName, String companyEmail, String companyAddress,
                   String companyPhoneNumber, String aboutCompany,
                   ArrayList<String> workingHours, ArrayList<Category> categories,
                   ArrayList<EventType> eventTypeTypes) {
        this.companyName = companyName;
        this.companyEmail = companyEmail;
        this.companyAddress = companyAddress;
        this.companyPhoneNumber = companyPhoneNumber;
        this.aboutCompany = aboutCompany;
        this.workingHours = workingHours;
        this.categories = categories;
        this.eventTypeTypes = eventTypeTypes;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyPhoneNumber() {
        return companyPhoneNumber;
    }

    public void setCompanyPhoneNumber(String companyPhoneNumber) {
        this.companyPhoneNumber = companyPhoneNumber;
    }

    public String getAboutCompany() {
        return aboutCompany;
    }

    public void setAboutCompany(String aboutCompany) {
        this.aboutCompany = aboutCompany;
    }

    public ArrayList<String> getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(ArrayList<String> workingHours) {
        this.workingHours = workingHours;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }
    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }
}