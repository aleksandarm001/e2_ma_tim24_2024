package com.example.eventplanner_team24.authentication.Service.IService;

import com.example.eventplanner_team24.authentication.Domain.User;

public interface IUserService {
    void saveUser(String uid, User user);
}
