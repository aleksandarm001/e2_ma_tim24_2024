package com.example.eventplanner_team24.authentication.Data;

import com.example.eventplanner_team24.authentication.Domain.Company;

public interface ICompanyRepository {
    void saveCompany(Company company);
}
