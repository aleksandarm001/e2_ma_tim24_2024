package com.example.eventplanner_team24.authentication.Presentation.interfaces;

public interface IUserTypeListener {
    void onUserTypeSelected(String userType);

}

