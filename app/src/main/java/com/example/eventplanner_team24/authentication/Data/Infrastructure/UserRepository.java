package com.example.eventplanner_team24.authentication.Data.Infrastructure;

import static android.content.ContentValues.TAG;
import android.util.Log;
import androidx.annotation.NonNull;
import com.example.eventplanner_team24.authentication.Data.IUserRepository;
import com.example.eventplanner_team24.authentication.Domain.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class UserRepository implements IUserRepository {

    private final FirebaseFirestore firestore;
    private final CollectionReference collection;

    @Inject
    public UserRepository() {
        this.firestore = FirebaseFirestore.getInstance();
        this.collection = firestore.collection("users");
    }

    @Override
    public void saveUser(String uid, User user) {
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("firstName", user.getFirstName());
        userMap.put("lastName", user.getLastName());
        userMap.put("email", user.getEmail());
        userMap.put("password", user.getPassword());
        userMap.put("address", user.getAddress());
        userMap.put("phoneNumber", user.getPhoneNumber());
        userMap.put("imageUri", user.getImageUri().toString()); // Convert Uri to String
        userMap.put("role", user.getRole());
        userMap.put("deactivated", true);

        this.firestore.collection("users").document(uid) // Use UID as document ID
                .set(userMap)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });

    }

}

