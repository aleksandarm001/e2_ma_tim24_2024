package com.example.eventplanner_team24.authentication.Presentation;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

import android.Manifest;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.eventplanner_team24.HomeActivity;
import com.example.eventplanner_team24.MyFirebaseMessagingService;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.authentication.Presentation.interfaces.IUserCreatedListener;
import com.example.eventplanner_team24.authentication.Presentation.interfaces.IUserTypeListener;
import com.example.eventplanner_team24.authentication.Service.UserService;

import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.databinding.FragmentRegistrationBinding;
import com.example.eventplanner_team24.authentication.Domain.User;
import com.example.eventplanner_team24.model.Notification;
import com.example.eventplanner_team24.model.Request;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

public class RegistrationFragment extends Fragment {

    private FirebaseAuth auth;
    private EditText email, password, confirmPassword, firstName, lastName, address, phoneNumber;
    private Button registerButton;
    private Uri selectedImageUri;
    private static final int REQUEST_IMAGE_PICK = 1;
    private IUserTypeListener userTypeListener;
    private IUserCreatedListener userCreationListener;
    private FragmentRegistrationBinding binding;
    private MyFirebaseMessagingService messagingService;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    @Inject
    UserService userService;

    public RegistrationFragment() {
        // Required empty public constructor
    }


    public static RegistrationFragment newInstance(String param1, String param2) {
        RegistrationFragment fragment = new RegistrationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.messagingService = new MyFirebaseMessagingService();

        DaggerAppComponent.create().inject(this);

        auth = FirebaseAuth.getInstance();
        binding = FragmentRegistrationBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();

        Button uploadImageButton = binding.uploadImageButton;
        uploadImageButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 0);
        });


        email = rootView.findViewById(R.id.emailEditText);
        password = rootView.findViewById(R.id.passwordEditText);
        confirmPassword = rootView.findViewById(R.id.confirmPasswordEditText);
        firstName = rootView.findViewById(R.id.firstNameEditText);
        lastName = rootView.findViewById(R.id.lastNameEditText);
        address = rootView.findViewById(R.id.addressEditText);
        phoneNumber = rootView.findViewById(R.id.phoneNumberEditText);

        //------------------------------
        email.setText(generateEmail());
        password.setText("password123");
        confirmPassword.setText("password123");
        firstName.setText("John");
        lastName.setText("Doe");
        address.setText("123 Main St");
        phoneNumber.setText("1234567890");
        //------------------------------


        RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.userTypeRadioGroup);

        registerButton = (Button) rootView.findViewById(R.id.registerButton);

        radioGroup.setOnCheckedChangeListener(((group, checkedId) -> {
            if (checkedId == R.id.userTypeRadioButton1) {
                registerButton.setText("Register");
            } else if (checkedId == R.id.userTypeRadioButton2) {
                registerButton.setText("Next step");
            }
        }));


        registerButton.setOnClickListener(v -> {
            String emailString = email.getText().toString();
            String passwordString = password.getText().toString();
            String confirmPasswordString = confirmPassword.getText().toString();
            String firstNameString = firstName.getText().toString();
            String lastNameString = lastName.getText().toString();
            String addressString = address.getText().toString();
            String phoneNumberString = phoneNumber.getText().toString();

            if (TextUtils.isEmpty(emailString)) {
                email.setError("Email cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(passwordString)) {
                password.setError("Password cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(confirmPasswordString)) {
                confirmPassword.setError("Confirm Password cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(firstNameString)) {
                firstName.setError("First Name cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(lastNameString)) {
                lastName.setError("Last Name cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(addressString)) {
                address.setError("Address cannot be empty!");
                return;
            }
            if (TextUtils.isEmpty(phoneNumberString)) {
                phoneNumber.setError("Phone Number cannot be empty!");
                return;
            }

            if (!checkPasswordsMatch(passwordString, confirmPasswordString)) {
                confirmPassword.setError("Passwords do not match!");
                return;
            }

            int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
            if (selectedRadioButtonId == R.id.userTypeRadioButton1) {
                auth.createUserWithEmailAndPassword(emailString, passwordString).addOnCompleteListener(
                        task -> {
                            if (task.isSuccessful()) {
                                FirebaseUser firebaseUser = task.getResult().getUser();
                                String uid = firebaseUser.getUid();
                                uploadImageToFirebaseStorage(selectedImageUri);
                                User newUser = new User(firstNameString, lastNameString, emailString,
                                        passwordString, addressString, phoneNumberString, this.selectedImageUri, 0);

                                saveUser(uid, newUser);
                                auth.getCurrentUser().sendEmailVerification();
                                Toast.makeText(getContext(), "Registration successful", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Registration failed: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            Intent intent = new Intent(getContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                );
            } else if (selectedRadioButtonId == R.id.userTypeRadioButton2) {
                auth.createUserWithEmailAndPassword(emailString, passwordString).addOnCompleteListener(
                        task -> {
                            if (task.isSuccessful()) {
                                userTypeListener.onUserTypeSelected("PUP-V");
                                FirebaseUser firebaseUser = task.getResult().getUser();
                                String uid = firebaseUser.getUid();
                                uploadImageToFirebaseStorage(selectedImageUri);
                                User newUser = new User(firstNameString, lastNameString, emailString,
                                        passwordString, addressString, phoneNumberString, this.selectedImageUri, 1);
                                executor.execute(() -> {
                                    Notification notification = new Notification("PUPV Registration", "New PUPV registration request", "Yf7DPJybfDaoLmrrzqz7std9f3v2", false, Notification.Role.ADMIN);
                                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                                    db.collection("notifications").add(notification);
                                });
                                saveUser(uid, newUser);
                                saveRequest(new Request(uid, uid));
                                Toast.makeText(getContext(), "Registration successful", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Registration failed: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            Intent intent = new Intent(getContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                );
            } else {
                Toast.makeText(getContext(), "Please select a user type", Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }

    private void saveRequest(Request request){
        Map<String, Object> requestMap = request.toMap();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("requests")
                .add(requestMap)
                .addOnSuccessListener(documentReference -> {
                    documentReference.update("id", documentReference.getId())
                            .addOnSuccessListener(aVoid -> Log.d("Request", "Request updated successfully"))
                            .addOnFailureListener(e -> Log.e("Request", "Error updating request ID", e));
                })
                .addOnFailureListener(e -> Log.e("Request", "Error adding request", e));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null && data.getData() != null) {
            selectedImageUri = data.getData();
            // Load the selected image into profileImageView for preview
            binding.profileImageView.setImageURI(selectedImageUri);
        }
    }

    private void uploadImageToFirebaseStorage(Uri imageUri) {
        StorageReference storageRef = FirebaseStorage.getInstance().getReference().child("profile_images/" + UUID.randomUUID().toString());
        UploadTask uploadTask = storageRef.putFile(imageUri);

        uploadTask.addOnSuccessListener(taskSnapshot -> {
            // Image uploaded successfully
            Toast.makeText(getContext(), "Image uploaded successfully", Toast.LENGTH_SHORT).show();
        }).addOnFailureListener(exception -> {
            Toast.makeText(getContext(), "Image upload failed: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    private void saveUser(String uid, User user) {
        this.userService.saveUser(uid, user);
    }


    private boolean checkPasswordsMatch(String password, String confirmPassword){
        return password.equals(confirmPassword);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            userTypeListener = (IUserTypeListener) context;
            userCreationListener = (IUserCreatedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement UserTypeListener");
        }
    }

    private String generateEmail(){
        Random random = new Random();
        int randomNumber = random.nextInt(900000) + 100000; // Generates a random number between 100000 and 999999
        // Concatenate the random number with the email string
        String email = "example" + randomNumber + "@example.com";
        return email;
    }

}