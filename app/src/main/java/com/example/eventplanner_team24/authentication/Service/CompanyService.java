package com.example.eventplanner_team24.authentication.Service;

import com.example.eventplanner_team24.authentication.Data.ICompanyRepository;
import com.example.eventplanner_team24.authentication.Data.Infrastructure.CompanyRepository;
import com.example.eventplanner_team24.authentication.Domain.Company;
import com.example.eventplanner_team24.authentication.Service.IService.ICompanyService;

import javax.inject.Inject;

public class CompanyService implements ICompanyService {
    private final CompanyRepository companyRepository;

    @Inject
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public void saveCompany(Company company) {
        companyRepository.saveCompany(company);
    }
}
