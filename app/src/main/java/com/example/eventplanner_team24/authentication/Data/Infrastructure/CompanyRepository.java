package com.example.eventplanner_team24.authentication.Data.Infrastructure;

import static android.content.ContentValues.TAG;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner_team24.authentication.Data.ICompanyRepository;
import com.example.eventplanner_team24.authentication.Domain.Company;
import com.example.eventplanner_team24.model.Category;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

public class CompanyRepository implements ICompanyRepository {
    private final FirebaseFirestore firestore;

    @Inject
    public CompanyRepository() {
        this.firestore = FirebaseFirestore.getInstance();
    }

    @Override
    public void saveCompany(Company company) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String document = Objects.requireNonNull(auth.getCurrentUser()).getUid();
        Map<String, Object> companyMap = new HashMap<>();
        companyMap.put("name", company.getCompanyName());
        companyMap.put("email", company.getCompanyEmail());
        companyMap.put("address", company.getCompanyAddress());
        companyMap.put("phoneNumber", company.getCompanyPhoneNumber());
        companyMap.put("about", company.getAboutCompany());
        companyMap.put("workingHours", company.getWorkingHours());


        // Convert the list of categories to a list of Maps
        List<Map<String, Object>> categoryList = new ArrayList<>();
        for (Category category : company.getCategories()) {
            Map<String, Object> categoryMap = new HashMap<>();
            categoryMap.put("id", category.getId());
            categoryMap.put("name", category.getName());
            categoryMap.put("description", category.getDescription());
            categoryList.add(categoryMap);
        }
        companyMap.put("categories", categoryList);

        List<Map<String, Object>> eventList = new ArrayList<>();
        for (Category category : company.getCategories()) {
            Map<String, Object> eventMap = new HashMap<>();
            eventMap.put("id", category.getId());
            eventMap.put("name", category.getName());
            eventMap.put("description", category.getDescription());
            eventList.add(eventMap);
        }
        companyMap.put("events", eventList);

        this.firestore.collection("companies").document(document)
                .set(companyMap)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });

    }
}
