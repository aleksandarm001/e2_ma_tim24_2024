package com.example.eventplanner_team24.authentication.Service;

import com.example.eventplanner_team24.authentication.Data.Infrastructure.UserRepository;
import com.example.eventplanner_team24.authentication.Domain.User;
import com.example.eventplanner_team24.authentication.Service.IService.IUserService;

import javax.inject.Inject;

public class UserService implements IUserService {

    private final UserRepository userRepository;

    @Inject
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void saveUser(String uid, User user) {
        userRepository.saveUser(uid, user);
    }
}