package com.example.eventplanner_team24.authentication.Domain;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class User implements Parcelable {
    public enum Role {
        OD,
        ADMIN,
        PUPV,
        PUPZ
    }
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String address;
    private String phoneNumber;
    private Uri imageUri;
    private Company company;
    private boolean isApproved;
    private Role role;
    private Boolean isDeactivated;
    private String companyId;

    public User(String firstName, String lastName, String email, String password,
                String address, String phoneNumber, Uri imageUri, int role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.imageUri = imageUri;
        switch(role){
            case 0:
                this.role = Role.OD;
                break;
            case 1:
                this.role = Role.ADMIN;
                break;
            case 2:
                this.role = Role.PUPV;
                break;
            case 3:
                this.role = Role.PUPZ;
                break;

        }
    }
    public User(String firstName, String lastName, String email, String password,
                String address, String phoneNumber, Uri imageUri, Company company) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.imageUri = imageUri;
        this.company = company;
    }

    public User(String id, String email, String firstName, String lastName, String companyId, String role,
                String address, String phoneNumber, String imageUri, boolean isDeactivated, String password) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.companyId = companyId;
        if (role.equals("OD")) {
            this.role = Role.OD;
        } else if (role.equals("ADMIN")) {
            this.role = Role.ADMIN;
        } else if (role.equals("PUPV")) {
            this.role = Role.PUPV;
        }
        else {
            this.role = Role.PUPZ;
        }
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.imageUri = Uri.parse(imageUri);
        this.isDeactivated = isDeactivated;
        this.password = password;
    }

    public User(){}
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean isDeactivated() {
        return isDeactivated;
    }

    public void setDeactivated(Boolean isDeactivated) {
        this.isDeactivated = isDeactivated;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    protected User(Parcel in) {
        id = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phoneNumber = in.readString();
        imageUri=Uri.parse(in.readString());

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(phoneNumber);
        dest.writeString(imageUri.toString());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };


}
