package com.example.eventplanner_team24.authentication.Presentation;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.eventplanner_team24.databinding.FragmentWorkingHoursPopupBinding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WorkingHoursPopupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WorkingHoursPopupFragment extends DialogFragment {
    private static final String ARG_PARAM1 = "param";
    private FragmentWorkingHoursPopupBinding binding;
    private List<String> selectedWorkingHours = new ArrayList<>();

    public WorkingHoursPopupFragment() {
        // Required empty public constructor
    }

    public static WorkingHoursPopupFragment newInstance() {
        WorkingHoursPopupFragment fragment = new WorkingHoursPopupFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        binding = FragmentWorkingHoursPopupBinding.inflate(inflater,container,false);
        View root = binding.getRoot();


        EditText mondayStartTimeEditText = binding.edittextMondayStartTime;
        EditText mondayEndTimeEditText = binding.edittextMondayEndTime;
        EditText tuesdayStartTimeEditText = binding.edittextTuesdayStartTime;
        EditText tuesdayEndTimeEditText = binding.edittextTuesdayEndTime;
        EditText wednesdayStartTimeEditText = binding.edittextWednesdayStartTime;
        EditText wednesdayEndTimeEditText = binding.edittextWednesdayEndTime;
        EditText thursdayStartTimeEditText = binding.edittextThursdayStartTime;
        EditText thursdayEndTimeEditText = binding.edittextThursdayEndTime;
        EditText fridayStartTimeEditText = binding.edittextFridayStartTime;
        EditText fridayEndTimeEditText = binding.edittextFridayEndTime;
        EditText saturdayStartTimeEditText = binding.edittextSaturdayStartTime;
        EditText saturdayEndTimeEditText = binding.edittextSaturdayEndTime;
        EditText sundayStartTimeEditText = binding.edittextSundayStartTime;
        EditText sundayEndTimeEditText = binding.edittextSundayEndTime;

        CheckBox checkBoxMonday = binding.checkboxMonday;
        CheckBox checkBoxTuesday = binding.checkboxTuesday;
        CheckBox checkBoxWednesday = binding.checkboxWednesday;
        CheckBox checkBoxThursday = binding.checkboxThursday;
        CheckBox checkBoxFriday = binding.checkboxFriday;
        CheckBox checkBoxSaturday = binding.checkboxSaturday;
        CheckBox checkBoxSunday = binding.checkboxSunday;

        // Set click listeners to show time picker dialog
        mondayStartTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(mondayStartTimeEditText);
            }
        });
        mondayEndTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(mondayEndTimeEditText);
            }
        });
        tuesdayStartTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(tuesdayStartTimeEditText);
            }
        });
        tuesdayEndTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(tuesdayEndTimeEditText);
            }
        });
        wednesdayStartTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(wednesdayStartTimeEditText);
            }
        });
        wednesdayEndTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(wednesdayEndTimeEditText);
            }
        });
        thursdayStartTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(thursdayStartTimeEditText);
            }
        });
        thursdayEndTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(thursdayEndTimeEditText);
            }
        });
        fridayStartTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(fridayStartTimeEditText);
            }
        });
        fridayEndTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(fridayEndTimeEditText);
            }
        });
        saturdayStartTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(saturdayStartTimeEditText);
            }
        });
        saturdayEndTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(saturdayEndTimeEditText);
            }
        });
        sundayStartTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(sundayStartTimeEditText);
            }
        });
        sundayEndTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(sundayEndTimeEditText);
            }
        });

        binding.buttonConfirm.setOnClickListener(v -> {
            if(checkBoxMonday.isChecked()){
                String startTime = mondayStartTimeEditText.getText().toString();
                String endTime = mondayEndTimeEditText.getText().toString();
                if(startTime.trim().isEmpty() || endTime.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please select start and end time for Monday", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedWorkingHours.add("Monday: " + startTime + " - " + endTime);
            }
            if(checkBoxTuesday.isChecked()){
                String startTime = tuesdayStartTimeEditText.getText().toString();
                String endTime = tuesdayEndTimeEditText.getText().toString();
                if(startTime.trim().isEmpty() || endTime.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please select start and end time for Monday", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedWorkingHours.add("Tuesday: " + startTime + " - " + endTime);
            }
            if(checkBoxWednesday.isChecked()){
                String startTime = wednesdayStartTimeEditText.getText().toString();
                String endTime = wednesdayEndTimeEditText.getText().toString();
                if(startTime.trim().isEmpty() || endTime.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please select start and end time for Monday", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedWorkingHours.add("Wednesday: " + startTime + " - " + endTime);
            }
            if(checkBoxThursday.isChecked()){
                String startTime = thursdayStartTimeEditText.getText().toString();
                String endTime = thursdayEndTimeEditText.getText().toString();
                if(startTime.trim().isEmpty() || endTime.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please select start and end time for Monday", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedWorkingHours.add("Thursday: " + startTime + " - " + endTime);
            }
            if(checkBoxFriday.isChecked()){
                String startTime = fridayStartTimeEditText.getText().toString();
                String endTime = fridayEndTimeEditText.getText().toString();
                if(startTime.trim().isEmpty() || endTime.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please select start and end time for Monday", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedWorkingHours.add("Friday: " + startTime + " - " + endTime);
            }
            if(checkBoxSaturday.isChecked()){
                String startTime = saturdayStartTimeEditText.getText().toString();
                String endTime = saturdayEndTimeEditText.getText().toString();
                if(startTime.trim().isEmpty() || endTime.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please select start and end time for Monday", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedWorkingHours.add("Saturday: " + startTime + " - " + endTime);
            }
            if(checkBoxSunday.isChecked()){
                String startTime = sundayStartTimeEditText.getText().toString();
                String endTime = sundayEndTimeEditText.getText().toString();
                if(startTime.trim().isEmpty() || endTime.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please select start and end time for Monday", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedWorkingHours.add("Sunday: " + startTime + " - " + endTime);
            }
            dismiss();
        });


        return root;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setLayout(400, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    private void showTimePickerDialog(final EditText editText) {
        // Get current time
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and show it
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        // Update the text of the EditText with the selected time
                        editText.setText(String.format("%02d:%02d", hourOfDay, minute));
                    }
                }, hour, minute, true);
        timePickerDialog.show();
    }

    public List<String> getSelectedWorkingHours() {
        return selectedWorkingHours;
    }

}