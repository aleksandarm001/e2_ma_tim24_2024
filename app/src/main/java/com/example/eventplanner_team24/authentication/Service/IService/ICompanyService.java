package com.example.eventplanner_team24.authentication.Service.IService;

import com.example.eventplanner_team24.authentication.Domain.Company;

public interface ICompanyService {
    void saveCompany(Company company);
}
