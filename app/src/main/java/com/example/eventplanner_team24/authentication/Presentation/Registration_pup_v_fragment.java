package com.example.eventplanner_team24.authentication.Presentation;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.authentication.Domain.Company;
import com.example.eventplanner_team24.authentication.Service.CompanyService;
import com.example.eventplanner_team24.category_manager.Data.CategoryCallback;
import com.example.eventplanner_team24.category_manager.Service.CategoryService;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.databinding.FragmentRegistrationPupVFragmentBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class Registration_pup_v_fragment extends Fragment implements CategoryCallback {
    private FirebaseAuth auth;
    private EditText companyName, companyEmail, companyAddress, companyPhoneNumber, aboutCompany;
    private Button registerButton;
    private FragmentRegistrationPupVFragmentBinding binding;
    private ArrayList<String> selectedWorkingHours;

    MaterialCardView selectCard;
    TextView serviceCategoryTextView;
    List<Category> categories = new ArrayList<>();
    ArrayList<Category> selectedCategories_Category;
    boolean [] selectedCategories;
    ArrayList<Integer> categoriesList = new ArrayList<>();
    String [] categoiesAray = new ArrayList<String>().toArray(new String[0]);

    //-----------------------------------
    MaterialCardView selectEventTypesCard;
    List<EventType> eventTypeTypeTypes = new ArrayList<>();
    ArrayList<EventType> selectedEventTypes_EventTypeTypeTypes;
    boolean [] selectedEventTypes;
    ArrayList<Integer> eventTypeList = new ArrayList<>();
    String [] eventTypesArray = new ArrayList<String>().toArray(new String[0]);

    @Inject
    CompanyService companyService;
    @Inject
    CategoryService categoryService;

    public Registration_pup_v_fragment() {
        // Required empty public constructor
    }

    public static Registration_pup_v_fragment newInstance() {
        Registration_pup_v_fragment fragment = new Registration_pup_v_fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        auth = FirebaseAuth.getInstance();
        binding = binding.inflate(inflater, container, false);
        DaggerAppComponent.create().inject(this);
        View rootView = binding.getRoot();

        InitializeViewsAndArrays();
        fetchCategories();

        selectCard.setOnClickListener(v -> {
            showCotegoriesDialog();
        });

        selectEventTypesCard.setOnClickListener(v -> {
            showEventTypesDialog();
        });

        binding.selectWorkingHoursCardView.setOnClickListener(v -> {
            showWorkingHoursPickerDialog();
        });

        //-------------------------------
        fillFields();
        //********************************

        registerButton = (Button)rootView.findViewById(R.id.registerCompanyButton);

        registerButton.setOnClickListener(v -> {
            // Validate companyName
            if (validateFields()) return;

            Company newCompany = CreateCompany();
            this.companyService.saveCompany(newCompany);
            Toast.makeText(getContext(), "Registration successful", Toast.LENGTH_SHORT).show();
        });

        return rootView;
    }

    private void InitializeViewsAndArrays() {
        companyName = binding.companyNameEditText;
        companyEmail = binding.companyEmailEditText;
        companyAddress = binding.companyAddressEditText;
        companyPhoneNumber = binding.companyPhoneNumberEditText;
        aboutCompany = binding.companyDescriptionEditText;

        selectCard = binding.serviceCategoryCardView;
        serviceCategoryTextView = binding.serviceCategoryTextView2;

        selectEventTypesCard = binding.selectEventTypesCardView;
        eventTypesArray = generateEventTypes();
        selectedEventTypes = new boolean[eventTypesArray.length];
    }
    private void fillFields() {
        companyName.setText("My Company Name");
        companyEmail.setText("company@example.com");
        companyAddress.setText("123 Main St, City, Country");
        companyPhoneNumber.setText("123-456-7890");
        aboutCompany.setText("About my company...");
    }
    private boolean validateFields() {
        String companyNameText = companyName.getText().toString().trim();
        if (companyNameText.isEmpty()) {
            companyName.setError("Company name is required");
            companyName.requestFocus();
            return true;
        }

        // Validate companyEmail
        String companyEmailText = companyEmail.getText().toString().trim();
        if (companyEmailText.isEmpty()) {
            companyEmail.setError("Company email is required");
            companyEmail.requestFocus();
            return true;
        }

        // Validate companyAddress
        String companyAddressText = companyAddress.getText().toString().trim();
        if (companyAddressText.isEmpty()) {
            companyAddress.setError("Company address is required");
            companyAddress.requestFocus();
            return true;
        }

        // Validate companyPhoneNumber
        String companyPhoneNumberText = companyPhoneNumber.getText().toString().trim();
        if (companyPhoneNumberText.isEmpty()) {
            companyPhoneNumber.setError("Company phone number is required");
            companyPhoneNumber.requestFocus();
            return true;
        }

        // Validate aboutCompany
        String aboutCompanyText = aboutCompany.getText().toString().trim();
        if (aboutCompanyText.isEmpty()) {
            aboutCompany.setError("About the company is required");
            aboutCompany.requestFocus();
            return true;
        }

        // Validate selected working hours
        if (selectedWorkingHours.isEmpty()) {
            Toast.makeText(getContext(), "Please select working hours", Toast.LENGTH_SHORT).show();
            return true;
        }

        // Validate selected categories
        if (categoriesList.isEmpty()) {
            Toast.makeText(getContext(), "Please select at least one category", Toast.LENGTH_SHORT).show();
            return true;
        }

        // Validate selected event types
        if (eventTypeList.isEmpty()) {
            Toast.makeText(getContext(), "Please select at least one event type", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
    @NonNull
    private Company CreateCompany() {
        getSelectedCategories();
        getSelectedEventTypes();
        Company newCompany = new Company(companyName.getText().toString(),
                companyEmail.getText().toString(),
                companyAddress.getText().toString(),
                companyPhoneNumber.getText().toString(),
                aboutCompany.getText().toString(),
                selectedWorkingHours,
                selectedCategories_Category,
                selectedEventTypes_EventTypeTypeTypes);
        return newCompany;
    }
    private void showCotegoriesDialog(){
        AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
        builder.setTitle("Select Service Categories");
        builder.setCancelable(false);
        builder.setMultiChoiceItems(categoiesAray, selectedCategories, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked){
                    categoriesList.add(which);
                } else {
                    categoriesList.remove(Integer.valueOf(which));
                }
            }
        }).setPositiveButton("Ok", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StringBuilder stringBuilder = new StringBuilder();
                for(int i = 0; i < categoriesList.size(); i++){
                    stringBuilder.append(categoiesAray[categoriesList.get(i)]);

                    if(i != categoriesList.size() - 1){
                        stringBuilder.append(", ");
                    }
                    serviceCategoryTextView.setText(stringBuilder.toString());
                }
            }

        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setNeutralButton("Clear all", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for(int i = 0; i < selectedCategories.length; i++){
                    selectedCategories[i] = false;

                    categoriesList.clear();
                    serviceCategoryTextView.setText("");
                }
            }
        });
        builder.show();
    }
    private void showEventTypesDialog(){
        AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
        builder.setTitle("Select Event Types");
        builder.setCancelable(false);
        builder.setMultiChoiceItems(eventTypesArray, selectedEventTypes, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked){
                    eventTypeList.add(which);
                } else {
                    eventTypeList.remove(which);
                }
            }
        }).setPositiveButton("Ok", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StringBuilder stringBuilder = new StringBuilder();
                for(int i = 0; i < eventTypeList.size(); i++){
                    stringBuilder.append(eventTypesArray[eventTypeList.get(i)]);

                    if(i != eventTypeList.size() - 1){
                        stringBuilder.append(", ");
                    }
                    binding.eventTypesTextView2.setText(stringBuilder.toString());
                }
            }

        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setNeutralButton("Clear all", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for(int i = 0; i < selectedEventTypes.length; i++){
                    selectedEventTypes[i] = false;

                    eventTypeList.clear();
                    binding.eventTypesTextView2.setText("");
                }
            }
        });
        builder.show();
    }
    private void showWorkingHoursPickerDialog() {
        WorkingHoursPopupFragment fragment = new WorkingHoursPopupFragment();   
        fragment.show(getChildFragmentManager(), "WorkingHoursPopupFragment");
        this.selectedWorkingHours = (ArrayList<String>) fragment.getSelectedWorkingHours();
        StringBuilder textBuilder = new StringBuilder();
        for (int i = 0; i < selectedWorkingHours.size(); i++) {
            textBuilder.append(selectedWorkingHours.get(i));
            if (i < selectedWorkingHours.size() - 1) {
                textBuilder.append(", ");
            }
        }
        String text = textBuilder.toString();
        binding.workingHoursTextView2.setText(text);
    }


    // FOR TESTING PURPOSES
    private String[] generateCategories() {
        List<String> categoryNames = new ArrayList<>();
        categories.add(new Category("1L", "Ugostiteljski objekti, hrana, ketering, torte i kolači",
                "Pružanje usluga pripreme i posluživanja hrane i pića za događaje, " +
                        "uključujući koktele, svečane večere, švedske stolove, tematske obroke i drugo. " +
                        "Lokacije koje nude mogućnost organizacije događaja unutar njihovih prostora, " +
                        "često s kompletnim ugostiteljskim uslugama. Ovo može uključivati restorane sa " +
                        "privatnim sobama ili sale za bankete specijalizovane za veće događaje. Usluge " +
                        "poput degustacija vina, koktel barmena, mobilnih barova, tematskih hrana i pića " +
                        "koje dodaju poseban dodir događajima. Specijalizovane pekare i poslastičarnice " +
                        "koje nude dizajnirane torte, slatke stolove i druge slatkiše prilagođene temi događaja."));

        categories.add(new Category("2L", "Smeštaj", "Hoteli, vile, apartmani i druge opcije smeštaja za goste događaja."));

        categories.add(new Category("3L", "Muzika i zabava", "DJ-evi, bendovi, solo izvođači, animatori za decu, plesači."));

        categories.add(new Category("4L", "Foto i video", "Profesionalno fotografisanje i snimanje događaja."));

        categories.add(new Category("5L", "Dekoracija i rasveta", "Ova kategorija može uključivati ne samo tematsku dekoraciju prostora, " +
                "cvetne aranžmane, balone, stolnjake, itd. već i specijalističko osvetljenje koje " +
                "može transformisati prostor događaja, uključujući scensko osvetljenje, LED rasvetu, " +
                "laserske efekte i druge."));

        categories.add(new Category("6L", "Garderoba i stilizovanje", "Iznajmljivanje ili kupovina formalne odeće, usluge stilista."));

        categories.add(new Category("7L", "Nega i lepota", "Frizerske usluge, makeup artisti, manikir/pedikir."));

        categories.add(new Category("8L", "Planiranje i koordinacija", "Agencije ili pojedinci specijalizovani za planiranje i koordinaciju događaja."));

        categories.add(new Category("9L", "Pozivnice i papirna galanterija", "Dizajn i štampa pozivnica, menija, rasporeda sedenja."));

        categories.add(new Category("10L", "Logistika i obezbeđenje", "Organizacija prevoza za goste i učesnike događaja. " +
                "Parking rešenja za događaje. Obezbeđenje događaja i osoblje za prvu pomoć. " +
                "Praćenje vremenskih rokova i isporuka."));


        for (Category category : categories) {
            categoryNames.add(category.getName());
        }
        return categoryNames.toArray(new String[0]);
    }
    private String[] generateEventTypes(){
        List<String> eventTypesNames = new ArrayList<>();
        eventTypeTypeTypes.add(new EventType("1L", "Privatni dogadjaji", "Opis"));
        eventTypeTypeTypes.add(new EventType("2L", "Korporativni dogadjaji", "Opis"));
        eventTypeTypeTypes.add(new EventType("3L", "Događaji u obrazovanju", "Opis"));
        eventTypeTypeTypes.add(new EventType("4L", "Kulturni i zabavni događaji", "Opis"));
        eventTypeTypeTypes.add(new EventType("5L", "Sportski događaji", "Opis"));
        eventTypeTypeTypes.add(new EventType("6L", "Humanitarni i dobrotvorni događaji", "Opis"));
        eventTypeTypeTypes.add(new EventType("7L", "Vlada i politički događaji", "Opis"));

        for (EventType e : eventTypeTypeTypes) {
            eventTypesNames.add(e.getName());
        }
        return eventTypesNames.toArray(new String[0]);
    }

    private void getSelectedCategories() {
        selectedCategories_Category = new ArrayList<>();
        for (int i = 0; i < selectedCategories.length; i++) {
            if (selectedCategories[i]) {
                selectedCategories_Category.add(categories.get(i));
            }
        }
    }
    private void getSelectedEventTypes(){
        selectedEventTypes_EventTypeTypeTypes = new ArrayList<>();
        for (int i = 0; i < selectedEventTypes.length; i++) {
            if (selectedEventTypes[i]) {
                selectedEventTypes_EventTypeTypeTypes.add(eventTypeTypeTypes.get(i));
            }
        }
    }

    public void fetchCategories() {
        categoryService.getAllCategories(this);
    }

    @Override
    public void onCategoriesLoaded(List<Category> categories) {
        this.categories.addAll(categories);
        categoiesAray = generateCategories();
        selectedCategories = new boolean[categoiesAray.length];
    }

    @Override
    public void onCategoryLoaded(Category category) {

    }

    @Override
    public void onError(Exception e) {
        Toast.makeText(getContext(), "Error loading categories", Toast.LENGTH_SHORT).show();
    }
}