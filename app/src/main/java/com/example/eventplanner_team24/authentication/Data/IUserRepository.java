package com.example.eventplanner_team24.authentication.Data;

import com.example.eventplanner_team24.authentication.Domain.User;

public interface IUserRepository {
    void saveUser(String uid, User user);
}
