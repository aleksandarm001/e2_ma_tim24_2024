package com.example.eventplanner_team24.authentication.Presentation.interfaces;

import com.example.eventplanner_team24.authentication.Domain.User;

public interface IUserCreatedListener {

    void onUserCreated(User user);
}
