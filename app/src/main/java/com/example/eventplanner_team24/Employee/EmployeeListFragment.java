package com.example.eventplanner_team24.Employee;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.HomeActivity;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.authentication.Domain.User;
import com.example.eventplanner_team24.databinding.FragmentEmployeeListBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeeListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeListFragment extends ListFragment {
    private EmployeeListAdapter adapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<User> mEmployees;
    private ArrayList<User> allEmployees;
    private FragmentEmployeeListBinding binding;
    private EmployeeRepo employeeRepo;
    private String companyId;
    private SearchView searchName;
    private FirebaseUser currentUser;
    private String storedOwnerId;

    public static EmployeeListFragment newInstance(ArrayList<User> employees){
        EmployeeListFragment fragment = new EmployeeListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, employees);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("EventApp", "onCreate Employees List Fragment");
        //setListByOwner();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mEmployees = new ArrayList<>();
        allEmployees=new ArrayList<>();
        employeeRepo = new EmployeeRepo();
        storedOwnerId="";
        Log.i("EventApp","current user: "+currentUser.getUid());

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i("EventApp", "onCreateView Employees List Fragment");
        binding = FragmentEmployeeListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        FloatingActionButton registerButton = root.findViewById(com.example.eventplanner_team24.R.id.register_button);

        if (currentUser != null) {
            String ownerId = currentUser.getUid();
            if(storedOwnerId.equals("")){
                storedOwnerId=ownerId;
            }
            employeeRepo.getById(currentUser.getUid(), new EmployeeRepo.EmployeeFetchCallback(){
                    @Override
                    public void onEmployeeObjectFetched(User user, String errorMessage){
                        if(user!=null){
                            companyId=user.getCompanyId();
                            employeeRepo.getByFirmId(companyId, new EmployeeRepo.EmployeeFetchCallback() {
                                @Override
                                public void onEmployeeFetch(ArrayList<User> employees) {
                                    if (employees != null) {
                                        allEmployees.clear();
                                        allEmployees.addAll(employees);
                                        Log.i("EventApp", "firm" + employees.size());
                                        adapter = new EmployeeListAdapter(getContext(), allEmployees, (AppCompatActivity) getActivity());
                                        setListAdapter(adapter);
                                    }
                                }
                            });
                        }
                    }
            });

        }



        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null && getActivity() instanceof HomeActivity) {
                    FragmentTransition.to(RegistrationEmployeeFragment.newInstance("",""),getActivity(),true, R.id.scroll_employees_list);
                }
            }
        });
        searchName=binding.searchText;
        searchName.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                EmployeeRepo.getByFirmId(companyId,new EmployeeRepo.EmployeeFetchCallback(){
                    @Override
                    public void onEmployeeFetch(ArrayList<User> employees) {
                        if (employees != null) {
                            allEmployees=new ArrayList<>();
                            for(User e:employees){
                                if (e.getFirstName().toLowerCase().contains(newText.toLowerCase()) && !allEmployees.contains(e))
                                    allEmployees.add(e);
                                if (e.getLastName().toLowerCase().contains(newText.toLowerCase()) && !allEmployees.contains(e))
                                    allEmployees.add(e);
                                if (e.getEmail().split("@")[0].toLowerCase().contains(newText.toLowerCase()) && !allEmployees.contains(e))
                                    allEmployees.add(e);

                            }
                            adapter=new EmployeeListAdapter(getContext(),allEmployees, (AppCompatActivity) getActivity());
                            setListAdapter(adapter);
                        }
                    }
                });

                return true;
            }
        });

        searchName.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                EmployeeRepo.getByFirmId(companyId,new EmployeeRepo.EmployeeFetchCallback(){
                    @Override
                    public void onEmployeeFetch(ArrayList<User> employees) {
                        if (employees != null) {
                            adapter=new EmployeeListAdapter(getContext(),allEmployees, (AppCompatActivity) getActivity());
                            setListAdapter(adapter);
                        }
                    }
                });
                searchName.setQuery("", false);
                return false;
            }
        });
        return root;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}