package com.example.eventplanner_team24.Employee;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner_team24.authentication.Domain.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Random;

public class EmployeeRepo {
    private static final FirebaseFirestore db = FirebaseFirestore.getInstance();

    public interface EmployeeFetchCallback {
        default void onEmployeeFetch(ArrayList<User> employees) {

        }
        default void onEmployeeObjectFetched(User user, String errorMessage) {

        }
        default void onUpdateSuccess() {}
        default void onUpdateFailure(String errorMessage) {}
    }
    public static void getByFirmId(String firmId, EmployeeFetchCallback callback) {
        Query query = db.collection("users").whereEqualTo("companyId", firmId).whereEqualTo("role", "PUPZ");
        ArrayList<User> employees = new ArrayList<>();
        query.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                for (QueryDocumentSnapshot document : task.getResult()) {
                    User user = new User(
                            document.getId(),
                            document.get("email").toString(),
                            document.get("firstName").toString(),
                            document.get("lastName").toString(),
                            document.get("companyId").toString(),
                            document.get("role").toString(),
                            document.get("address").toString(),
                            document.get("phoneNumber").toString(),
                            document.get("imageUri").toString(),
                            document.get("deactivated").toString().equals("true"),
                            document.get("password").toString());
                    employees.add(user);
                }
                callback.onEmployeeFetch(employees);

            } else {
                Log.d("OwnerRepo", "Error getting owners by firmId", task.getException());
                callback.onEmployeeFetch(null);
            }
        });
    }
    public static void getById(String id, EmployeeRepo.EmployeeFetchCallback callback) {
        db.collection("users").document(id)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (queryDocumentSnapshots.exists()) {
                        User user = new User(
                                queryDocumentSnapshots.getId(),
                        queryDocumentSnapshots.get("email").toString(),
                        queryDocumentSnapshots.get("firstName").toString(),
                        queryDocumentSnapshots.get("lastName").toString(),
                        queryDocumentSnapshots.get("companyId").toString(),
                        queryDocumentSnapshots.get("role").toString(),
                        queryDocumentSnapshots.get("address").toString(),
                        queryDocumentSnapshots.get("phoneNumber").toString(),
                        queryDocumentSnapshots.get("imageUri").toString(),
                                queryDocumentSnapshots.get("deactivated").toString().equals("true"),
                                queryDocumentSnapshots.get("password").toString());

//                        User user = queryDocumentSnapshots.toObject(User.class);
                        Log.i("REZ", "USer" + user);
                        callback.onEmployeeObjectFetched(user, null);
                    } else {
                        callback.onEmployeeObjectFetched(null, "User not found");
                    }
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents", e);
                    callback.onEmployeeObjectFetched(null, e.getMessage());
                });
    }
    public static void updateEmployee(User employee, EmployeeRepo.EmployeeFetchCallback callback) {
        db.collection("users")
                .document(employee.getId())
                .set(employee)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("REZ_DB", "DocumentSnapshot updated with ID: " + employee.getId());
                        callback.onUpdateSuccess();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("REZ_DB", "Error updating document", e);
                        callback.onUpdateFailure("Failed");
                    }
                });
    }

}
