package com.example.eventplanner_team24.Employee;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.authentication.Domain.User;
import com.example.eventplanner_team24.databinding.FragmentEmployeeDetailsBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeeDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeDetailsFragment extends Fragment {

    private User employee;
    FragmentEmployeeDetailsBinding binding;
    public static EmployeeDetailsFragment newInstance(User employee) {
        EmployeeDetailsFragment fragment = new EmployeeDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable("employee", employee);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        employee = getArguments().getParcelable("employee");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentEmployeeDetailsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        binding.name.setText(employee.getFirstName() + " " + employee.getLastName());
        binding.email.setText(employee.getEmail());
        binding.phoneNumber.setText(employee.getPhoneNumber());
        binding.address.setText(employee.getAddress());
        binding.profileImage.setImageURI(employee.getImageUri());

        return inflater.inflate(R.layout.fragment_employee_details, container, false);
    }
}