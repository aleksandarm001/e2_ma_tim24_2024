package com.example.eventplanner_team24.Employee;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.HomeActivity;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.authentication.Domain.User;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class EmployeeListAdapter extends ArrayAdapter<User> {
    private ArrayList<User> aEmployees;
    private AppCompatActivity mActivity;
    private String state="deactivate";
    private FirebaseAuth mAuth;


    public EmployeeListAdapter(Context context, ArrayList<User> employees, AppCompatActivity activity){
        super(context, R.layout.employee_item, employees);
        aEmployees = employees;
        mActivity=activity;
    }

    @Override
    public int getCount() {
        return aEmployees.size();
    }


    @Nullable
    @Override
    public User getItem(int position) {
        return aEmployees.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        User employee = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.employee_item,
                    parent, false);
        }
        LinearLayout employeeCard = convertView.findViewById(R.id.employee_item);
        ImageView imageView = convertView.findViewById(R.id.employee_image);
        TextView name = convertView.findViewById(R.id.employee_name);
        TextView email = convertView.findViewById(R.id.employee_email);
        Button deactivate =convertView.findViewById(R.id.deactivate);
        mAuth = FirebaseAuth.getInstance();
        if(employee != null){
            if (employee.getImageUri()!=null && employee.getImageUri().toString()!="") {
                Picasso.get().load(employee.getImageUri()).into(imageView);
            }

            name.setText(employee.getFirstName()+" "+employee.getLastName());
            email.setText(employee.getEmail());
            employeeCard.setOnClickListener(v -> {
                if(getContext() instanceof HomeActivity){
                    HomeActivity activity = (HomeActivity) getContext();
                    FragmentTransition.to(EmployeeDetailsFragment.newInstance(employee),activity,true,R.id.scroll_employees_list);

                }
            });
            if(employee.isDeactivated()){
                deactivate.setText("Activate");
                deactivate.setBackgroundColor(getContext().getResources().getColor(R.color.green));
                state="activate";
            }else{
                deactivate.setText("Deactivate");
                deactivate.setBackgroundColor(getContext().getResources().getColor(R.color.red));
                state="deactivate";
            }
            deactivate.setOnClickListener(v -> {
                if (getContext() instanceof HomeActivity) {
                    HomeActivity activity = (HomeActivity) getContext();
                    AlertDialog.Builder dialog = new AlertDialog.Builder(activity);

                    if (employee.isDeactivated()) {
                        dialog.setMessage("Are you sure you want to activate this account?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Log.i("pass","pass email "+employee.getEmail()+" "+employee.getPassword());
                                        employee.setDeactivated(false);
                                        deactivate.setBackgroundColor(getContext().getResources().getColor(R.color.red));
                                        deactivate.setText("Deactivate");
                                        EmployeeRepo.updateEmployee(employee, new EmployeeRepo.EmployeeFetchCallback() {
                                            @Override
                                            public void onUpdateSuccess() {
                                                Log.i("eventapp","uspesno");
                                                EmployeeRepo.EmployeeFetchCallback.super.onUpdateSuccess();
                                            }

                                            @Override
                                            public void onUpdateFailure(String errorMessage) {
                                                EmployeeRepo.EmployeeFetchCallback.super.onUpdateFailure(errorMessage);
                                            }
                                        });
                                    }
                                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Log.i("pass","pass email "+employee.getEmail()+" "+employee.getPassword());
                            }
                                });
                        AlertDialog alert = dialog.create();
                        alert.show();
                    } else {
                        dialog.setMessage("Are you sure you want to deactivate this account?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Log.i("pass","pass email "+employee.getEmail()+" "+employee.getPassword());


                                        deactivate.setText("Activate");
                                        state = "activate";
                                        deactivate.setBackgroundColor(getContext().getResources().getColor(R.color.green));
                                        employee.setDeactivated(true);
                                        EmployeeRepo.updateEmployee(employee, new EmployeeRepo.EmployeeFetchCallback() {
                                            @Override
                                            public void onUpdateSuccess() {
                                                EmployeeRepo.EmployeeFetchCallback.super.onUpdateSuccess();
                                            }

                                            @Override
                                            public void onUpdateFailure(String errorMessage) {
                                                EmployeeRepo.EmployeeFetchCallback.super.onUpdateFailure(errorMessage);
                                            }
                                        });
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Log.i("pass","pass email "+employee.getEmail()+" "+employee.getPassword());
                                    }
                                });
                        AlertDialog alert = dialog.create();
                        alert.show();

                    }
                }
            });
        }
        return convertView;
    }

}
