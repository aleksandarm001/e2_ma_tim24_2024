package com.example.eventplanner_team24.Employee;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentEmployeePageBinding;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeePageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeePageFragment extends Fragment {

//    static ArrayList<Employee> employees = new ArrayList<Employee>();

    private FragmentEmployeePageBinding binding;

    public static EmployeePageFragment newInstance(String param1, String param2) {
        return new EmployeePageFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeePageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        FragmentTransition.to(com.example.eventplanner_team24.Employee.EmployeeListFragment.newInstance(new ArrayList<>()), getActivity(),
                false, R.id.scroll_employees_list);

        return root;
    }
}