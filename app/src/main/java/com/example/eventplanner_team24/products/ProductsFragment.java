package com.example.eventplanner_team24.products;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.ListFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentProductsBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.repositories.EventTypeRepo;
import com.example.eventplanner_team24.repositories.ProductRepository;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProductsFragment extends ListFragment {

    private ProductListAdapter adapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Product> mProducts;
    private ArrayList<Product> allProducts;

    private FragmentProductsBinding binding;
    private Boolean isFirstSelection=true;
    private SearchView searchName;
    private EditText minPrice;
    private EditText maxPrice;
    private EditText description;
    private RadioButton availableProductsFilter;
    private RadioButton notAvailableProductsFilter;
    private RadioButton allProductsFilter;
    private String minPriceText="";
    private String maxPriceText="";
    private String descriptionText="";
    private List<EventType> selectedEvents = new ArrayList<>();
    private List<Category> selectedCategories = new ArrayList<>();
    private List<SubCategory> selectedSubcategories = new ArrayList<>();
    private int availableProductFilterState=2; //0 available 1 not available 2 all
    private ProductRepository productRepository;


    public ProductsFragment() {
        // Required empty public constructor
    }

    public static ProductsFragment newInstance(ArrayList<Product> products) {
        ProductsFragment fragment = new ProductsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, products);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productRepository=new ProductRepository();
        productRepository.getAllProducts(new ProductRepository.ProductFetchCallback() {
            @Override
            public void onProductFetch(ArrayList<Product> products) {
                if (products != null) {
                    adapter=new ProductListAdapter(getActivity(),products);
                    setListAdapter(adapter);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentProductsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);


        FragmentActivity currentActivity = getActivity();
        FloatingActionButton addButton = binding.floatingActionButtonProducts;
        addButton.setVisibility(View.VISIBLE);


        //******************************FILTERS**********************************************************

        Button btnFilters = (Button) root.findViewById(R.id.btnFilters);
        btnFilters.setOnClickListener(v -> {
            BottomSheetDialog bottomSheetDialog =  new BottomSheetDialog(requireActivity());
            View dialogView = getLayoutInflater().inflate(R.layout.product_filter, null);
            bottomSheetDialog.setContentView(dialogView);

            minPrice=dialogView.findViewById(R.id.priceProductMin);
            maxPrice=dialogView.findViewById(R.id.priceProductMax);
            description=dialogView.findViewById(R.id.editTextFilterDescription);
            availableProductsFilter=dialogView.findViewById(R.id.availableProductFilter);
            notAvailableProductsFilter=dialogView.findViewById(R.id.notAvailableProductFilter);
            allProductsFilter=dialogView.findViewById(R.id.allProductFilter);

            //initial state set
            minPrice.setText(minPriceText);
            maxPrice.setText(maxPriceText);
            description.setText(descriptionText);
            if(availableProductFilterState==0)
                availableProductsFilter.setChecked(true);
            else if(availableProductFilterState==1)
                notAvailableProductsFilter.setChecked(true);
            else
                allProductsFilter.setChecked(true);
            Spinner spinnerCat = dialogView.findViewById(R.id.spinCat);
            CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
                @Override
                public void onCategoryFetch(ArrayList<Category> categories) {
                    ArrayAdapter<Category> arrayAdapter = new ArrayAdapter<Category>(getActivity(), R.layout.multispiner, categories) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            Category category = getItem(position);
                            textView.setText(category.getName());

                            for(Category c:selectedCategories)
                                if(c.getId().equals(category.getId()))
                                    checkBox.setChecked(true);

                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    Category selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedCategories.add(selectedItem);
                                    } else {
                                        ArrayList<Category> varCategories=new ArrayList<>();
                                        for(Category c:selectedCategories)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varCategories.add(c);

                                        selectedCategories.clear();
                                        selectedCategories.addAll(varCategories);

                                    }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerCat.setAdapter(arrayAdapter);
                }
            });


            Spinner spinnerSubCat = dialogView.findViewById(R.id.spinSubcat);
            SubCategoryRepository.getAllSubcategories(new SubCategoryRepository.SubcategoryFetchCallback() {
                @Override
                public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {
                    ArrayAdapter<SubCategory> arrayAdapterSub = new ArrayAdapter<SubCategory>(getActivity(), R.layout.multispiner, subcategories) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            SubCategory subCategory = getItem(position);
                            textView.setText(subCategory.getName());
                            for(SubCategory c:selectedSubcategories)
                                if(c.getId().equals(subCategory.getId()))
                                    checkBox.setChecked(true);
                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    SubCategory selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedSubcategories.add(selectedItem);
                                    } else {
                                        ArrayList<SubCategory> varSubcategories=new ArrayList<>();
                                        for(SubCategory c:selectedSubcategories)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varSubcategories.add(c);

                                        selectedSubcategories.clear();
                                        selectedSubcategories.addAll(varSubcategories);                                    }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerSubCat.setAdapter(arrayAdapterSub);
                }
            });

            Spinner spinnerEvent = dialogView.findViewById(R.id.spinEvent);
            EventTypeRepo.getAllActivateEventTypes(new EventTypeRepo.EventTypeFetchCallback() {
                @Override
                public void onEventTypeFetch(ArrayList<EventType> eventTypes) {

                    ArrayAdapter<EventType> arrayAdapterEvent = new ArrayAdapter<EventType>(getActivity(), R.layout.multispiner, eventTypes) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            EventType event = getItem(position);
                            textView.setText(event.getName());
                            for(EventType c:selectedEvents)
                                if(c.getId().equals(event.getId()))
                                    checkBox.setChecked(true);
                            checkBox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox checkBox = (CheckBox) v;
                                    EventType selectedItem = getItem(position);
                                    if (checkBox.isChecked()) {
                                        selectedEvents.add(selectedItem);
                                    } else {
                                        ArrayList<EventType> varEventTypes=new ArrayList<>();
                                        for(EventType c:selectedEvents)
                                            if(!c.getId().equals(selectedItem.getId()))
                                                varEventTypes.add(c);

                                        selectedEvents.clear();
                                        selectedEvents.addAll(varEventTypes);                                       }
                                }
                            });

                            return view;
                        }
                    };
                    spinnerEvent.setAdapter(arrayAdapterEvent);
                }
            });


            //***********************FILTERS*********************************
            Button apply=dialogView.findViewById(R.id.applyProductFilters);
            apply.setOnClickListener(a->{

                minPriceText=minPrice.getText().toString();
                maxPriceText=maxPrice.getText().toString();
                descriptionText=description.getText().toString();
                Predicate<Product> combinedPredicate = p -> true;

                if(availableProductsFilter.isChecked()) {
                    availableProductFilterState = 0;
                    combinedPredicate = combinedPredicate.and(Product::getAvailable);
                }
                else if (notAvailableProductsFilter.isChecked()) {
                    availableProductFilterState = 1;
                    combinedPredicate = combinedPredicate.and(p-> !p.getAvailable());
                }
                else
                    availableProductFilterState=2;

                if (!minPriceText.isEmpty()) {
                    combinedPredicate = combinedPredicate.and(p -> p.getPrice() > Double.parseDouble(minPriceText));
                }
                if (!maxPriceText.isEmpty()) {
                    combinedPredicate = combinedPredicate.and(p -> p.getPrice() < Double.parseDouble(maxPriceText));
                }

                if (!descriptionText.isEmpty()) {
                    String desc = descriptionText.toLowerCase();
                    combinedPredicate = combinedPredicate.and(p -> p.getDescription().toLowerCase().contains(desc));
                }


                Predicate<Product> finalCombinedPredicate = combinedPredicate;
                productRepository.getAllProducts(new ProductRepository.ProductFetchCallback() {
                    @Override
                    public void onProductFetch(ArrayList<Product> products) {
                        if (products != null) {
                            List<Product> filteredList = products.stream()
                                    .filter(finalCombinedPredicate)
                                    .collect(Collectors.toList());

                            if (getActivity() != null ) {
                                getActivity().runOnUiThread(() -> {
                                    adapter = new ProductListAdapter(getActivity(), new ArrayList<>(filterByCategorySubcategoryEvents(filteredList)));
                                    setListAdapter(adapter);
                                });
                            }
                        }
                    }
                });

                bottomSheetDialog.dismiss();

            });

            //****************DISCARD FILTERS********************************

            Button discard=dialogView.findViewById(R.id.discardProductFilters);
            discard.setOnClickListener(d->{
                productRepository.getAllProducts(new ProductRepository.ProductFetchCallback() {
                    @Override
                    public void onProductFetch(ArrayList<Product> products) {
                        if (products != null) {
                            adapter=new ProductListAdapter(getActivity(),products);
                            setListAdapter(adapter);
                        }
                    }
                });
                selectedEvents.clear();
                selectedSubcategories.clear();
                selectedCategories.clear();
                minPriceText="";
                maxPriceText="";
                descriptionText="";
                availableProductFilterState=2;
                bottomSheetDialog.dismiss();
            });


            bottomSheetDialog.show();
        });



        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addButton.setVisibility(View.GONE);
                FragmentTransition.to(AddProductFragment.newInstance(null), currentActivity, true, R.id.list_layout_products);
//                btnFilters.setVisibility(View.GONE);
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private List<Product> filterByCategorySubcategoryEvents(List<Product> filteredList){
        List<Product> filtered=new ArrayList<>();

        if(selectedCategories.size()>0){
            for(Category category:selectedCategories) {
                for (Product s : filteredList) {
                    if (s.getCategory().equals(category.getId()) && !filtered.contains(s))
                        filtered.add(s);
                }
            }

            filteredList.clear();
            filteredList.addAll(filtered);
            filtered.clear();
        }


        if(selectedSubcategories.size()>0) {
            for (SubCategory subcategory : selectedSubcategories)
                for (Product s : filteredList)
                    if (s.getSubcategory().equals(subcategory.getId()) && !filtered.contains(s))
                        filtered.add(s);
            filteredList.clear();
            filteredList.addAll(filtered);
            filtered.clear();
        }

        if(selectedEvents.size()>0){
            for(EventType event:selectedEvents)
                for(Product s:filteredList)
                    for(String type:s.getType())
                        if(type.equals(event.getId()) && !filtered.contains(s))
                            filtered.add(s);
            filteredList.clear();
            filteredList.addAll(filtered);
        }
        return filteredList;
    }

}
