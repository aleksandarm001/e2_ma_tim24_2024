package com.example.eventplanner_team24.products;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentProductDetailsBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.repositories.EventTypeRepo;
import com.example.eventplanner_team24.repositories.ProductRepository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductDetails extends Fragment {

    private TextView productName;
    private TextView productDescription;
    private TextView productPrice;
    private TextView productDiscount;
    private TextView productDiscountPrice;
    private TextView productCategory;
    private TextView productSubcategory;
    private TextView productVisible;
    private TextView productAvailable;
    private TextView productEventType;
    private Product product;
    private List<Uri> selectedUriImages;
    private FragmentProductDetailsBinding binding;

    public ProductDetails() {
        // Required empty public constructor
    }


    public static ProductDetails newInstance(Product p) {
        ProductDetails fragment = new ProductDetails();
        Bundle args = new Bundle();
        args.putParcelable("PROIZVOD", p);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = getArguments().getParcelable("PROIZVOD");
        }
        selectedUriImages = new ArrayList<>();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProductDetailsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        LinearLayout photoLinearLayout = root.findViewById(R.id.photoLinearLayout);



        Button btnEdit = (Button) root.findViewById(R.id.btnEdit);
        Button btnDelete = (Button) root.findViewById(R.id.btnDelete);
        productName=binding.nameDetail;
        productDescription=binding.descriptionDetail;
        productCategory=binding.categoryDetail;
        productSubcategory=binding.subcategoryDetail;
        productDiscount=binding.discountDetail;
        productEventType=binding.eventDetail;
        productPrice=binding.priceDetail;
        productDiscountPrice=binding.discountPriceDetail;
        productAvailable=binding.availableDetail;
        productVisible=binding.visibleDetail;

        SharedPreferences prefs = requireContext().getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String stringValue = prefs.getString("key_string", "OWNER");

        if(stringValue.equals("EMPLOYEE")){
            btnEdit.setVisibility(View.INVISIBLE);
            btnDelete.setVisibility(View.INVISIBLE);
        }



        SetData();

        btnEdit.setOnClickListener(v -> {
            FragmentTransition.to(AddProductFragment.newInstance(product), getActivity(),
                    true, R.id.list_layout_products);
        });

        btnDelete.setOnClickListener(v -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage("Are you sure you want to delete this product?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ProductRepository.deleteProduct(product.getId());
                            FragmentTransition.to(ProductsFragment.newInstance(new ArrayList<>()),getActivity() ,
                                    true, R.id.list_layout_products);
                        }


                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = dialog.create();
            alert.show();
        });

        for (String imageUrl : product.getImageUris()) {
            ImageView imageView = new ImageView(getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    300, 300
            );
            layoutParams.setMargins(0, 0, 10, 0);
            imageView.setLayoutParams(layoutParams);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Picasso.get().load(imageUrl).into(imageView);
            photoLinearLayout.addView(imageView);
        }
        return  root;
    }


    private void SetData(){
        productName.setText(product.getName());
        productDescription.setText(product.getDescription());
        CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                for(Category c:categories)
                    if(c.getId().equals(product.getCategory()))
                        productCategory.setText(c.getName());
            }
        });
        SubCategoryRepository.getAllSubcategories(new SubCategoryRepository.SubcategoryFetchCallback() {
            @Override
            public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {
                for(SubCategory s:subcategories)
                    if(s.getId().equals(product.getSubcategory())) {
                        productSubcategory.setText(s.getName());
                    }

            }
        });
        productPrice.setText(product.getPrice()+" din");
        productDiscount.setText(product.getDiscount()+ " %");
        double discountPrice=product.getPrice()*(1-product.getDiscount()/100);
        productDiscountPrice.setText(Math.round(discountPrice) +" din");
        if(product.getAvailable())
            productAvailable.setText("Da");
        else
            productAvailable.setText("Ne");
        if(product.getVisible())
            productVisible.setText("Da");
        else
            productVisible.setText("Ne");
        EventTypeRepo eventTypeRepo=new EventTypeRepo();
        eventTypeRepo.getAllActivateEventTypes(new EventTypeRepo.EventTypeFetchCallback() {
            @Override
            public void onEventTypeFetch(ArrayList<EventType> eventTypes) {
                String events = "";
                if (eventTypes != null) {
                    for(EventType e:eventTypes)
                        for(String id:product.getType())
                            if(e.getId().equals(id))
                                events +=e.getName()+"   ";
                    productEventType.setText(events);

                }
            }
        });


    }
}