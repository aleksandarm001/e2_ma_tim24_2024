package com.example.eventplanner_team24.products;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.databinding.FragmentAddProductBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.model.SubCategory;
import com.example.eventplanner_team24.repositories.EventTypeRepo;
import com.example.eventplanner_team24.repositories.ProductRepository;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddProductFragment extends Fragment {

    private ArrayList<String> imageUris=new ArrayList<>();
    private List<Uri> selectedUriImages;
    private FragmentAddProductBinding binding;
    private Boolean isFirstSelection=true;
    private TextView subcateSuggestion;
    private String subcatSuggestionName="";
    private EditText name;
    private EditText description;
    private EditText discount;
    private EditText price;
    private Category selectedCategory=null;
    private RadioButton available;
    private RadioButton notAvailable;
    private RadioButton visible;
    private RadioButton notVisible;
    private Spinner category;
    private Spinner subcategory;
    private Spinner eventType;
    private  String productCategory="";
    private ArrayList<EventType> selectedEvents;
    private SubCategory selectedSubcategory=null;
    private String subcategoryName;
    private String subcategoryDescription;
    private Spinner spinnerCategory;
    private Spinner spinnersubcategory;
    private TextView categoryTextView;
    private SubCategoryRepository subcategoryRepo;
    private  Boolean isEventTypeChanged;

    private Product newProduct;
    private Product product;

    public AddProductFragment() {
        // Required empty public constructor
    }

    public static AddProductFragment newInstance(Product p) {
        AddProductFragment fragment = new AddProductFragment();
        Bundle args = new Bundle();
        args.putParcelable("PRODUCT", p);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = getArguments().getParcelable("PRODUCT");
            newProduct = getArguments().getParcelable("PRODUCT");
        }
        selectedEvents=new ArrayList<>();
        subcategoryRepo = new SubCategoryRepository();
        isEventTypeChanged=false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAddProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        subcateSuggestion=binding.twSubcatSuggestion;
        spinnerCategory = (Spinner) root.findViewById(R.id.spinnerProductCategory);
        spinnersubcategory=binding.spinnerProductSubcategory;
        subcateSuggestion.setVisibility(View.GONE);
        name = binding.nameProduct;
        description = binding.descriptionProduct;
        discount = binding.discountProduct;
        price = binding.priceProduct;
        visible = binding.visibleProduct;
        notVisible = binding.notVisibleProduct;
        available = binding.availableProduct;
        notAvailable = binding.notAvailableProduct;
        eventType = binding.spinnerProductEventType;

        if(product==null)
            setCreatingState();
        else
            setUpdateState();  //IZMENA


        //******************************************EVENT TYPE SPINNER******************************************************
        EventTypeRepo eventTypeRepo = new EventTypeRepo(); // Instantiate your EventTypeRepo class

        eventTypeRepo.getAllActivateEventTypes(new EventTypeRepo.EventTypeFetchCallback() {
            @Override
            public void onEventTypeFetch(ArrayList<EventType> eventTypes) {
                if (eventTypes != null) {
                    Spinner spinner = (Spinner) root.findViewById(R.id.spinnerProductEventType);
                    ArrayAdapter<EventType> arrayAdapter = new ArrayAdapter<EventType>(getActivity(), R.layout.multispiner,
                            eventTypes) {

                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            return createCustomView(position, convertView, parent);
                        }

                        private View createCustomView(int position, View convertView, ViewGroup parent) {
                            LayoutInflater inflater = LayoutInflater.from(getContext());
                            View view = convertView;
                            if (view == null) {
                                view = inflater.inflate(R.layout.multispiner, parent, false);
                            }

                            TextView textView = view.findViewById(R.id.textView);
                            CheckBox checkBox = view.findViewById(R.id.checkBox);

                            // Set your text and checkbox state here
                            EventType eventType = getItem(position);
                            if (eventType != null) {
                                if(product!=null){
                                    for(String s:product.getType())
                                        if(s.equals(eventType.getId()) && !selectedEvents.contains(eventType))
                                            selectedEvents.add(eventType);
                                }
                                textView.setText(eventType.getName()); // Assuming EventType has a getName() method
                                checkBox.setChecked(selectedEvents.contains(eventType)); // Assuming selectedEvents is a list of selected EventType objects

                                checkBox.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        isEventTypeChanged=true;
                                        CheckBox checkBox = (CheckBox) v;
                                        EventType selectedEventType = getItem(position);
                                        if (checkBox.isChecked() && !selectedEvents.contains(selectedEventType)) {
                                            // Add item to selectedEvents list if checked
                                            selectedEvents.add(selectedEventType);
                                        } else {
                                            // Remove item from selectedEvents list if unchecked
                                            selectedEvents.remove(selectedEventType);
                                        }
                                    }
                                });
                            }

                            return view;
                        }
                    };
                    spinner.setAdapter(arrayAdapter);
                } else {
                    // Handle the case where fetching event types failed
                    // Show a toast or handle the error appropriately
                }
            }
        });

        //******************************SLIKE*****************************************
        Button uploadImageButton = root.findViewById(R.id.uploadCompanyPhotoButton);
        LinearLayout photoLinearLayout = root.findViewById(R.id.photoLinearLayout);
        List<Uri> selectedUriImages = new ArrayList<>(); // tu ce biti URI od slika

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

        ActivityResultLauncher<Intent> mGetContent = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                            Intent data = result.getData();
                            selectedUriImages.clear();  //da mi ne duplira slike

                            if (data.getClipData() != null) {
                                ClipData clipData=data.getClipData();
                                for(int i=0;i<clipData.getItemCount();i++){
                                    ClipData.Item item=clipData.getItemAt(i);
                                    Uri selectedImageUri = item.getUri();
                                    selectedUriImages.add(selectedImageUri);
                                }

                            } else if (data.getData() != null) {
                                Uri selectedImageUri=data.getData();
                                selectedUriImages.add(selectedImageUri);
                            }
                            // za prikaz svih slika u layoutu
                            for (Uri imageUri : selectedUriImages) {
                                ImageView imageView = new ImageView(getContext());
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                        300,300
                                );
                                layoutParams.setMargins(0, 0, 10, 0);
                                imageView.setLayoutParams(layoutParams);
                                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                imageView.setImageURI(imageUri);
                                photoLinearLayout.addView(imageView);
                            }
                            for(Uri imageUri:selectedUriImages){
                                StorageReference imageRef = storageRef.child("images/" + UUID.randomUUID().toString());

                                imageRef.putFile(imageUri)
                                        .addOnSuccessListener(taskSnapshot -> {
                                            // Image uploaded successfully, get the download URL
                                            imageRef.getDownloadUrl().addOnSuccessListener(uri -> {
                                                // Save the download URL to the Realtime Database
                                                String imageUrl = uri.toString();
                                                imageUris.add(imageUrl);
                                                if(product!=null){
                                                    List<String> uris=product.getImageUris();
                                                    uris.add(imageUrl);
                                                    product.setImageUris((ArrayList<String>) uris);
                                                }
                                            });
                                        })
                                        .addOnFailureListener(exception -> {
                                            // Handle unsuccessful uploads
                                            Log.e("TAG", "Image upload failed: " + exception.getMessage());
                                        });
                            }
                        }
                    }
                });

        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                mGetContent.launch(intent);
            }
        });

        return root;

    }


    @Override
    public void onViewCreated(@Nullable View view, @Nullable Bundle savedInstanceState) {

        Button btnSubmit = (Button) binding.btnSubmit;
        btnSubmit.setOnClickListener(v -> {
            if(product==null) {
                if (addNewProduct()) {
                    if (subcatSuggestionName.equals("")) {
                        ProductRepository.createProduct(newProduct);
                        productCategory = "";
                    }
                    FragmentTransition.to(ProductsFragment.newInstance(new ArrayList<>()), getActivity(),
                            true, R.id.list_layout_products);
                }
            }else{
                if(editProduct()){
                    FragmentTransition.to(ProductsFragment.newInstance(new ArrayList<>()), getActivity(),
                            true, R.id.list_layout_products);
                }
            }

        });




    }
    private boolean areValidFields(){
        if(name.getText().toString().isEmpty()) {
            name.setError("Name is required.");
            return false;
        }
        if(description.getText().toString().isEmpty()){
            description.setError("Description is required.");
            return false;
        }
        if(price.getText().toString().isEmpty()){
            price.setError("Price is required.");
            return false;
        }
        if(!price.getText().toString().isEmpty() && Double.parseDouble(price.getText().toString())<0){
            price.setError("Input positive number.");
            return false;
        }
        if(!discount.getText().toString().isEmpty() && Double.parseDouble(discount.getText().toString())<0){
            discount.setError("Input positive number.");
            return false;
        }
//        if( selectedEvents.size()==0 ){
//            Log.i("SE",selectedEvents.size()+"");
//            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); // Pass your activity or context here
//
//            builder.setTitle("Error!");
//            builder.setMessage("Please select event type.");
//
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//
//            AlertDialog dialog = builder.create();
//            dialog.show();
//            return false;
//        }
        return true;
    }

    private boolean areCategoryAndSubcategoryValid(){
        if( selectedCategory==null ){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); // Pass your activity or context here

            builder.setTitle("Error!");
            builder.setMessage("Please select category.");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        }
        if( selectedSubcategory==null ){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); // Pass your activity or context here

            builder.setTitle("Error!");
            builder.setMessage("Please select subcategory.");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        }

        return true;
    }
    private boolean addNewProduct() {
        newProduct = new Product();
        if(areValidFields() && areCategoryAndSubcategoryValid()) {
            String productName = name.getText().toString();
            String productDescription = description.getText().toString();
            double productPrice = Double.parseDouble(price.getText().toString());
            double productDiscount=0;
            if(!discount.getText().toString().isEmpty())
                productDiscount = Double.parseDouble(discount.getText().toString());
            if (subcatSuggestionName.equals("")) {
                newProduct.setSubcategory(selectedSubcategory.getId());
            }
            newProduct.setName(productName);
            newProduct.setDescription(productDescription);
            newProduct.setPrice(productPrice);
            newProduct.setDiscount(productDiscount);
            if (visible.isChecked()) {
                newProduct.setVisible(true);
            } else if (notVisible.isChecked()) {
                newProduct.setVisible(false);
            }
            if (available.isChecked()) {
                newProduct.setAvailable(true);
            } else if (notAvailable.isChecked()) {
                newProduct.setAvailable(false);
            }
            ArrayList<String> events=new ArrayList<>();
            for(EventType e:selectedEvents)
                events.add(e.getId());
            newProduct.setTypes(events);
            newProduct.setCategory(selectedCategory.getId());
            newProduct.setImageUris(imageUris);
            return true;
        }else{
            return false;
        }
    }

    private boolean editProduct(){
        if(!isEventTypeChanged)
            selectedEvents.add(new EventType());

        if(areValidFields()){
            product.setName(name.getText().toString());
            product.setDescription(description.getText().toString());
            product.setPrice(Double.parseDouble(price.getText().toString()));
            if(!discount.getText().toString().isEmpty())
                product.setDiscount(Double.parseDouble(discount.getText().toString()));
            if (visible.isChecked()) {
                product.setVisible(true);
            } else if (notVisible.isChecked()) {
                product.setVisible(false);
            }
            if (available.isChecked()) {
                product.setAvailable(true);
            } else if (notAvailable.isChecked()) {
                product.setAvailable(false);
            }
            if(isEventTypeChanged) {
                ArrayList<String> events = new ArrayList<>();
                for (EventType e : selectedEvents)
                    events.add(e.getId());
                product.setTypes(events);
            }
            return true;
        }
        return false;
    }
    private static String generateUniqueString() {
        long timestamp = System.currentTimeMillis();

        String timestampString = Long.toString(timestamp);

        Random random = new Random();
        int randomInt = random.nextInt(10000);

        return timestampString + "_" + randomInt;
    }


    private void setUpdateState(){
        name.setText(product.getName());
        description.setText(product.getDescription());
        price.setText(Double.toString(product.getPrice()));
        discount.setText(Double.toString(product.getDiscount()));
        if(product.getAvailable())
            available.setChecked(true);
        else
            notAvailable.setChecked(true);
        if(product.getVisible())
            visible.setChecked(true);
        else
            notVisible.setChecked(true);

        spinnerCategory.setEnabled(false);
        spinnersubcategory.setEnabled(false);
        LinearLayout cat=binding.llcatProduct;
        cat.setVisibility(View.GONE);
        LinearLayout subcat=binding.llsubcatProduct;
        subcat.setVisibility(View.GONE);
        CategoryRepository.getAllCategories( new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                for(Category c:categories)
                    if(c.getId().equals(product.getCategory())){
                        SubCategoryRepository.getAllSubcategories(new SubCategoryRepository.SubcategoryFetchCallback() {
                            @Override
                            public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {
                                for(SubCategory s:subcategories)
                                    if(s.getId().equals(product.getSubcategory())){
                                        subcateSuggestion.setVisibility(View.VISIBLE);
                                        subcateSuggestion.setText("Category: "+c.getName()+"         Subcategory: "+s.getName());
                                    }
                            }
                        });
                    }
            }
        });
    }

    private void setCreatingState(){
        available.setChecked(true);
        visible.setChecked(true);

        CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
            @Override
            public void onCategoryFetch(ArrayList<Category> categories) {
                if (categories != null) {
                    // Create an adapter for the spinner with the fetched categories
                    ArrayAdapter<Category> arrayAdapter = new ArrayAdapter<>(requireContext(),
                            android.R.layout.simple_spinner_item, categories);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // Set the adapter to the spinner
                    spinnerCategory.setAdapter(arrayAdapter);
                } else {
                    // Handle the case where fetching categories failed
                    // Show a toast or handle the error appropriately
                }
            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected category
                selectedCategory = (Category) parent.getItemAtPosition(position);

                // Fetch subcategories based on the selected category
                subcategoryRepo.getSubcategoriesByCategoryId(selectedCategory.getId(), new SubCategoryRepository.SubcategoryFetchCallback() {
                    @Override
                    public void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {
                        if (subcategories != null) {
                            ArrayAdapter<SubCategory> subcategoryAdapter = new ArrayAdapter<>(requireContext(),
                                    android.R.layout.simple_spinner_item, subcategories);
                            subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnersubcategory.setAdapter(subcategoryAdapter);
                        }
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle case when nothing is selected
            }
        });
        spinnersubcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedSubcategory = (SubCategory) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }
}