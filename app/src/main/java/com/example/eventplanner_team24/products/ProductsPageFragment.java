package com.example.eventplanner_team24.products;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.databinding.FragmentProductsPageBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.Product;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;


import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductsPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductsPageFragment extends Fragment {
    public static ArrayList<Product> products = new ArrayList<Product>();
    private FragmentProductsPageBinding binding;
    public ProductsPageFragment() {
        // Required empty public constructor
    }

    public static ProductsPageFragment newInstance() {
        ProductsPageFragment fragment = new ProductsPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();
        binding = FragmentProductsPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        prepareProductsList(products);
        FragmentTransition.to(ProductsFragment.newInstance(products),currentActivity,true,R.id.list_layout_products);



        return root;
    }

    private void prepareProductsList(ArrayList<Product> products){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Product product = document.toObject(Product.class);
                            products.add(product);
                        }

                        for (Product product : products) {
                            Log.d("REZ_DB", "Product: " + product.toString());
                        }

                    } else {
                        Log.w("REZ_DB", "Error getting documents.", task.getException());
                    }
                });
    }

    public void onDestroyView() {
        super.onDestroyView();
        products=new ArrayList<Product>();
    }
}


