package com.example.eventplanner_team24.products;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.HomeActivity;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ProductListAdapter extends ArrayAdapter<Product> {
    private ArrayList<Product> aProduct;
    private FragmentActivity mContext;

    public ProductListAdapter(FragmentActivity context, ArrayList<Product> products){
        super(context, R.layout.product_list_item);
        mContext = context;
        aProduct = products;
    }

    @Override
    public int getCount() {
        return aProduct.size();
    }

    @Nullable
    @Override
    public Product getItem(int position) {
        return aProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Product product = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_list_item,
                    parent, false);
        }
        LinearLayout productCard = convertView.findViewById(R.id.product_list_item);
        ImageView imageView = convertView.findViewById(R.id.product_image);
        TextView productTitle = convertView.findViewById(R.id.product_name);
        TextView productCategory = convertView.findViewById(R.id.product_category);
        TextView productPrice = convertView.findViewById(R.id.product_price);
        TextView productAvailable = convertView.findViewById(R.id.product_available);


        if(product != null){
            if (!product.getImageUris().isEmpty()) {
                Picasso.get().load(product.getImageUris().get(0)).into(imageView);
            }
            productTitle.setText(product.getName());

            CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
                @Override
                public void onCategoryFetch(ArrayList<Category> categories) {
                    for(Category c:categories)
                        if(c.getId().equals(product.getCategory()))
                            productCategory.setText(c.getName());
                }
            });


            productPrice.setText(Double.toString(product.getPrice())+" din");
            if(product.getAvailable())
                productAvailable.setText("Dostupan");
            else
                productAvailable.setText("Nedostupan");
            productCard.setOnClickListener(v -> {
                if (getContext() instanceof HomeActivity) {
                    HomeActivity activity = (HomeActivity) getContext();
                    FragmentTransition.to(ProductDetails.newInstance(product),activity ,
                            true, R.id.list_layout_products);
                }
            });




        }

        return convertView;
    }
}

