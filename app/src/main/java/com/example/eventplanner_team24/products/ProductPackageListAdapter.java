package com.example.eventplanner_team24.products;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.HomeActivity;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Product;

import java.util.ArrayList;

public class ProductPackageListAdapter extends ArrayAdapter<Product> {
    private ArrayList<Product> aProducts;

    public ProductPackageListAdapter(Context context, ArrayList<Product> products){
        super(context, R.layout.product_list_item, products);
        aProducts = products;

    }
    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return aProducts.size();
    }


    @Nullable
    @Override
    public Product getItem(int position) {
        return aProducts.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Product product = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_for_package,
                    parent, false);
        }
        LinearLayout productCard = convertView.findViewById(R.id.product_card_for_package_item);
        TextView productTitle = convertView.findViewById(R.id.productP_title);
        TextView productCategory = convertView.findViewById(R.id.productP_category);
        TextView productPrice = convertView.findViewById(R.id.productP_price);
        Button btnDelete=convertView.findViewById(R.id.btnDelete);

        if(product != null){
            productTitle.setText(product.getName());
            CategoryRepository.getAllCategories(new CategoryRepository.CategoryFetchCallback() {
                @Override
                public void onCategoryFetch(ArrayList<Category> categories) {
                    for(Category c:categories)
                        if(c.getId().equals(product.getCategory()))
                            productCategory.setText(c.getName());
                }
            });            productPrice.setText(Double.toString(product.getPrice())+" din");
            HomeActivity activity = (HomeActivity) getContext();

            productCard.setOnClickListener(v -> {
                if (getContext() instanceof HomeActivity) {
                    FragmentTransition.to(ProductDetails.newInstance(product),activity ,
                            true, R.id.list_layout_packages);
                }


            });

            SharedPreferences prefs = getContext().getSharedPreferences("MyPrefs", MODE_PRIVATE);
            String stringValue = prefs.getString("key_string", "OWNER");

            if(stringValue.equals("EMPLOYEE"))
                btnDelete.setVisibility(View.GONE);

            btnDelete.setOnClickListener(v->{
                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                dialog.setMessage("Are you sure you want to remove this product?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }

                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = dialog.create();
                alert.show();
            });




        }

        return convertView;
    }

}
