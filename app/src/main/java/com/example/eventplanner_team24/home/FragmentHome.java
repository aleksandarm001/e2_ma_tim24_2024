package com.example.eventplanner_team24.home;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.authentication.Domain.User;
import com.example.eventplanner_team24.model.Notification;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentHome#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentHome extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    private int notificationID =0;
    private static String CHANNEL_1 = "Channel_1";
    private NotificationManager notificationManager;
    private ArrayList<QueryDocumentSnapshot> notifications = new ArrayList<QueryDocumentSnapshot>();
    private String userId;
    public FragmentHome() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentHome.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentHome newInstance(String param1, String param2) {
        FragmentHome fragment = new FragmentHome();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public void getAdminNotifications(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("notifications")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if(document.toObject(Notification.class).getReceiverId() != null && document.toObject(Notification.class).getReceiverId().toString().equals("Yf7DPJybfDaoLmrrzqz7std9f3v2") && !document.toObject(Notification.class).getRead()){
                                    notifications.add(document);
                                }
                            }
                            if(!notifications.isEmpty()){
                                posalji();
                            }
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                        }

                    }
        });
    }
    public void getPUPNotifications(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("notifications")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if(document.toObject(Notification.class).getRole() == Notification.Role.PUPV
                                        && (document.toObject(Notification.class).getReceiverId() == null
                                        || document.toObject(Notification.class).getReceiverId().equals(userId))
                                        && !document.toObject(Notification.class).getRead()){
                                    notifications.add(document);
                                }
                            }
                            if(!notifications.isEmpty()){
                                posalji();
                            }
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                        }
                    }
                });
    }
    private void posalji() {
        // Kreiraj notifikacije
        for (DocumentSnapshot notification : notifications) {
            createNotification(notification.toObject(Notification.class).getTitle(),
                    notification.toObject(Notification.class).getText(), notification.getId());
        }
    }
    private void createNotification(String title, String text, String id) {
        notificationID++;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(requireContext(), CHANNEL_1)
                .setSmallIcon(R.drawable.ic_action_about)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        notificationManager.notify(notificationID, builder.build());

        executor.execute(()-> {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("notifications").document(id);
            docRef.update("read", true);
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            if(ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.POST_NOTIFICATIONS)!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(getActivity(),new String[]{android.Manifest.permission.POST_NOTIFICATIONS},101);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        notificationManager = (NotificationManager) requireContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationID = 0;
        notifications.clear();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        if (user != null) {
            this.userId = user.getUid();
            DocumentReference docRef = db.collection("users").document(user.getUid());
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    String role = documentSnapshot.getString("role");
                    if (role.equals("ADMIN")) {
                        getAdminNotifications();
                    } else if (role.equals("PUP-V")) {
                        getPUPNotifications();
                    }

                }
            });
        }

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

}