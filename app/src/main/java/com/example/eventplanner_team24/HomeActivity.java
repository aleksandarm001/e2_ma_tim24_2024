package com.example.eventplanner_team24;

import static android.content.ContentValues.TAG;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.eventplanner_team24.authentication.Presentation.LoginActivity;
import com.example.eventplanner_team24.category_manager.Presentation.CategoryManagerFragment;
import com.example.eventplanner_team24.category_manager.Presentation.FragmentCreateCategory;
import com.example.eventplanner_team24.databinding.ActivityHomeBinding;
import com.example.eventplanner_team24.recievers.SyncReceiver;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;

public class HomeActivity extends AppCompatActivity {
    private ActivityHomeBinding binding;

    private Toolbar toolbar;
    private NavController navController;
    private AppBarConfiguration mAppBarConfiguration;
    private SyncReceiver syncReceiver;
    public static String SYNC_DATA = "SYNC_DATA";
    private static String CHANNEL_1 = "Channel_1";
    private String role = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getUserRole();

        FirebaseMessaging.getInstance().subscribeToTopic("admin_notifications_user_created").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                String msg = "Done";
                if (!task.isSuccessful()) {
                    msg = "Failed";
                }

            }
        });

        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        toolbar = binding.toolbar;
        setSupportActionBar(toolbar);

        navController = Navigation.findNavController(this,R.id.fragment_nav_content_main);
        mAppBarConfiguration = new AppBarConfiguration
                .Builder(R.id.category_manager,R.id.event_manager)
                .build();

        NavigationUI.setupWithNavController(binding.toolbar,navController);
        NavigationUI.setupWithNavController(binding.bottomNavigationView,navController);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);

        createNotificationChannel("Channel_1", "Channel for user created notifications",
                CHANNEL_1, NotificationManager.IMPORTANCE_DEFAULT);


        Menu menu = binding.bottomNavigationView.getMenu();
        MenuItem categoryItem = menu.findItem(R.id.category_manager);
        MenuItem productItem = menu.findItem(R.id.product_manager);
        MenuItem serviceItem = menu.findItem(R.id.service_manager);
        MenuItem packageItem = menu.findItem(R.id.package_manager);
        MenuItem eventItem = menu.findItem(R.id.event_manager);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null){
            if(user.getUid().equals("Yf7DPJybfDaoLmrrzqz7std9f3v2")){
                categoryItem.setVisible(true);
                eventItem.setVisible(true);
                productItem.setVisible(true);
                serviceItem.setVisible(true);
                packageItem.setVisible(true);
            }else{
                categoryItem.setVisible(false);
                eventItem.setVisible(false);
                productItem.setVisible(true);
                serviceItem.setVisible(true);
                packageItem.setVisible(true);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }


    private void getUserRole() {

    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        int id = item.getItemId();
        navController = Navigation.findNavController(this, R.id.fragment_nav_content_main);
        if (id == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return NavigationUI.onNavDestinationSelected(item, navController) || super.onOptionsItemSelected(item);
    }

    private void setUpReceiver(){
        syncReceiver = new SyncReceiver();
        /*
         * Registrujemo nas BroadcastReceiver i dodajemo mu 'filter'.
         * Filter koristimo prilikom prispeca poruka. Jedan receiver
         * moze da reaguje na vise tipova poruka. One nam kazu
         * sta tacno treba da se desi kada poruka odredjenog tipa (filera)
         * stigne.
         * */
        IntentFilter filter = new IntentFilter();
        filter.addAction(SYNC_DATA);
        registerReceiver(syncReceiver, filter);

    }
    private void createNotificationChannel(CharSequence name, String description, String channel_id, int importance) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        NotificationChannel channel = new NotificationChannel(channel_id, name, importance);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

}
