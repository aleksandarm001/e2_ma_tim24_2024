package com.example.eventplanner_team24.repositories;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner_team24.model.Service;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;


import java.util.ArrayList;
import java.util.Random;
public class ServiceRepository {

    private static final FirebaseFirestore db = FirebaseFirestore.getInstance();


    public static void createService(Service newService){
        newService.setId("service_"+generateUniqueString());
        db.collection("services")
                .document(newService.getId().toString())
                .set(newService)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("REZ_DB", "DocumentSnapshot added with ID: " + newService.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("REZ_DB", "Error adding document", e);
                    }
                });
    }


    public void getAllServices(ServiceRepository.ServiceFetchCallback callback) {
        ArrayList<Service> services = new ArrayList<>();

        db.collection("services")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Service service = document.toObject(Service.class);
                            if (!service.getDeleted()) {
                                services.add(service);
                            }
                        }
                        // Invoke the callback with the products list
                        callback.onServiceFetch(services);
                    } else {
                        Log.w("REZ_DB", "Error getting documents.", task.getException());
                        // Invoke the callback with null if an error occurs
                        callback.onServiceFetch(null);
                    }
                });
    }

    public static void deleteService(String id){
        DocumentReference docRef = db.collection("services").document(id);
        docRef.update("deleted", true)
                .addOnSuccessListener(aVoid -> Log.d("REZ_DB", "Service successfully changed"))
                .addOnFailureListener(e -> Log.w("REZ_DB", "Error getting documents.", e));
    }

    public static void updateService(Service s) {
        db.collection("services")
                .document(s.getId().toString())
                .set(s)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("REZ_DB", "DocumentSnapshot updated with ID: " + s.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("REZ_DB", "Error updating document", e);
                    }
                });
    }


    public interface ServiceFetchCallback {
        default void onServiceFetch(ArrayList<Service> services) {

        }
        default void onResult(boolean contains) {

        }
    }


    private static String generateUniqueString() {
        long timestamp = System.currentTimeMillis();

        String timestampString = Long.toString(timestamp);

        Random random = new Random();
        int randomInt = random.nextInt(10000);

        return timestampString + "_" + randomInt;
    }
}
