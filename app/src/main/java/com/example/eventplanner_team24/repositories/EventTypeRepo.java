package com.example.eventplanner_team24.repositories;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner_team24.model.EventType;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EventTypeRepo {

    private static final FirebaseFirestore db = FirebaseFirestore.getInstance();

    public static void createEventType(EventType newEventType){
        newEventType.setId("eventType_"+generateUniqueString());
        db.collection("events")
                .document(newEventType.getId())
                .set(newEventType)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("REZ_DB", "DocumentSnapshot added with ID: " + newEventType.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("REZ_DB", "Error adding document", e);
                    }
                });
    }

    public static void updateEventType(EventType updatedEventType) {
        db.collection("events")
                .document(updatedEventType.getId())
                .set(updatedEventType)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("REZ_DB", "DocumentSnapshot updated with ID: " + updatedEventType.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("REZ_DB", "Error updating document", e);
                    }
                });
    }


    public static void getAllEventTypes(EventTypeFetchCallback callback) {
        ArrayList<EventType> eventTypeTypes = new ArrayList<>();

        db.collection("events")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            EventType eventType = EventType.fromDocumentSnapshot(document);
                            ArrayList<SubCategory> suggestedSubCategories = new ArrayList<>();
                            List<String> subCategoryIds = (List<String>) document.get("suggestedSubCategories"); // Assuming you have this method
                            for (String subCategoryId : subCategoryIds) {
                                db.collection("sub_categories").document(subCategoryId).get().addOnCompleteListener(subCategoryTask -> {
                                    if (subCategoryTask.isSuccessful()) {
                                        DocumentSnapshot subCategoryDoc = subCategoryTask.getResult();
                                        if (subCategoryDoc.exists()) {
                                            SubCategory subCategory = SubCategory.fromDocumentSnapshot(subCategoryDoc);
                                            suggestedSubCategories.add(subCategory);
                                        } else {
                                            Log.e("EventRepository", "Subcategory document not found for ID: " + subCategoryId);
                                        }
                                    } else {
                                        Log.e("EventRepository", "Error getting subcategory document", subCategoryTask.getException());
                                    }

                                    // Notify callback when all subcategories are loaded
                                    if (suggestedSubCategories.size() == subCategoryIds.size()) {
                                        eventType.setSuggestedSubCategories(suggestedSubCategories);
                                        eventTypeTypes.add(eventType);
                                        if (eventTypeTypes.size() == task.getResult().size()) {
                                            callback.onEventTypeFetch(eventTypeTypes);
                                        }
                                    }
                                });
                            }
                        }
                    } else {
                        Log.w("REZ_DB", "Error getting documents.", task.getException());
                        // Invoke the callback with null if an error occurs
                        callback.onEventTypeFetch(null);
                    }
                });
    }

    public static void getAllActivateEventTypes(EventTypeFetchCallback callback) {
        ArrayList<EventType> eventTypeTypes = new ArrayList<>();

        db.collection("events")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            EventType eventType = EventType.fromDocumentSnapshot(document);
                            ArrayList<SubCategory> suggestedSubCategories = new ArrayList<>();
                            List<String> subCategoryIds = (List<String>) document.get("suggestedSubCategories"); // Assuming you have this method
                            for (String subCategoryId : subCategoryIds) {
                                db.collection("sub_categories").document(subCategoryId).get().addOnCompleteListener(subCategoryTask -> {
                                    if (subCategoryTask.isSuccessful()) {
                                        DocumentSnapshot subCategoryDoc = subCategoryTask.getResult();
                                        if (subCategoryDoc.exists()) {
                                            SubCategory subCategory = SubCategory.fromDocumentSnapshot(subCategoryDoc);
                                            suggestedSubCategories.add(subCategory);
                                        } else {
                                            Log.e("EventRepository", "Subcategory document not found for ID: " + subCategoryId);
                                        }
                                    } else {
                                        Log.e("EventRepository", "Error getting subcategory document", subCategoryTask.getException());
                                    }

                                    // Notify callback when all subcategories are loaded
                                    if (suggestedSubCategories.size() == subCategoryIds.size()) {
                                        eventType.setSuggestedSubCategories(suggestedSubCategories);
                                        eventTypeTypes.add(eventType);
                                        if (eventTypeTypes.size() == task.getResult().size()) {
                                            callback.onEventTypeFetch(eventTypeTypes);
                                        }
                                    }
                                });
                            }
                        }
                    } else {
                        Log.w("REZ_DB", "Error getting documents.", task.getException());
                        // Invoke the callback with null if an error occurs
                        callback.onEventTypeFetch(null);
                    }
                });
    }


    public static void activate(String id, EventTypeActivationCallback callback) {
        DocumentReference docRef = db.collection("events").document(id);
        docRef.update("deactivated", false)
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Event type successfully activated.");
                    callback.onActivationSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error activating event type.", e);
                    callback.onActivationFailure("Failed to activate event type.");
                });
    }

    public static void deactivate(String id, EventTypeDeactivationCallback callback) {
        DocumentReference docRef = db.collection("events").document(id);
        docRef.update("deactivated", true)
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Event type successfully deactivated.");
                    callback.onDeactivationSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error activating event type.", e);
                    callback.onDeactivationFailure("Failed to deactivate event type.");
                });
    }

    public interface EventTypeFetchCallback {
        void onEventTypeFetch(ArrayList<EventType> eventTypes);
    }

    public interface EventTypeActivationCallback {
        void onActivationSuccess();
        void onActivationFailure(String errorMessage);
    }
    public interface EventTypeDeactivationCallback {
        void onDeactivationSuccess();
        void onDeactivationFailure(String errorMessage);
    }

    public static void deleteSubcategoriesFromEventTypes(String subcategoryId) {
        db.collection("events")
                .whereArrayContains("suggestedSubcategoriesIds", subcategoryId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            DocumentReference docRef = document.getReference();
                            docRef.update("suggestedSubcategoriesIds", FieldValue.arrayRemove(subcategoryId))
                                    .addOnSuccessListener(aVoid -> Log.d("REZ_DB", "Event updated successfully"))
                                    .addOnFailureListener(e -> Log.w("REZ_DB", "Error updating event", e));
                        }
                    } else {
                        Log.d("REZ_DB", "Error getting events: ", task.getException());
                    }
                });
    }

    private static String generateUniqueString() {
        long timestamp = System.currentTimeMillis();

        String timestampString = Long.toString(timestamp);

        Random random = new Random();
        int randomInt = random.nextInt(10000);

        return timestampString + "_" + randomInt;
    }
}
