package com.example.eventplanner_team24.requests;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.CategoryCallback;
import com.example.eventplanner_team24.category_manager.Presentation.FragmentCategoryList;
import com.example.eventplanner_team24.category_manager.Presentation.adapters.CategoryAdapter;
import com.example.eventplanner_team24.databinding.FragmentCategoryListBinding;
import com.example.eventplanner_team24.databinding.FragmentRequestsOverviewBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Request;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RequestsOverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RequestsOverviewFragment extends Fragment {

    private static final String ARG_PARAM = "param";
    private ArrayList<Request> mRequests;
    private FragmentRequestsOverviewBinding binding;
    private RequestsAdapter adapter;
    private List<Request> requests = new ArrayList<>();
    ExecutorService executor = Executors.newSingleThreadExecutor();

    public RequestsOverviewFragment() {
        // Required empty public constructor
    }


    public static RequestsOverviewFragment newInstance() {
        RequestsOverviewFragment fragment = new RequestsOverviewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getRequests();
        mRequests = new ArrayList<>(generateRandomRequests(3));
        adapter = new RequestsAdapter(getActivity(), mRequests);
        if (getArguments() != null) {
            //mRequests = getArguments().getParcelableArrayList(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = binding.inflate(inflater,container,false);
        View root = binding.getRoot();
        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        setupSearchBar(root);
        return root;
    }

    public void getRequests() {
        executor.execute(() -> {
            List<Request> tempRequests = new ArrayList<>();
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            CountDownLatch latch = new CountDownLatch(1);

            db.collection("requests")
                    .whereEqualTo("status", "PENDING") // Filter by status = "PENDING"
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Request request = Request.fromDocumentSnapshot(document);
                                tempRequests.add(request);
                            }
                            this.mRequests = new ArrayList<>(tempRequests);
                        } else {
                            Log.e("Requests", "Error getting requests", task.getException());
                        }
                        latch.countDown();
                    });

            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    private void setupSearchBar(View root) {
        EditText searchBar = root.findViewById(R.id.searchBar);

        searchBar.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterRequests(s.toString());
            }
        });
    }

    private void filterRequests(String query) {
        String lowerCaseQuery = query.toLowerCase();
        List<Request> filteredRequests = new ArrayList<>();

        for (Request request : mRequests) {
            if ((request.getCompanyName() != null && request.getCompanyName().toLowerCase().contains(lowerCaseQuery)) ||
                    (request.getOwnerName() != null && request.getOwnerName().toLowerCase().contains(lowerCaseQuery)) ||
                    (request.getOwnerSurname() != null && request.getOwnerSurname().toLowerCase().contains(lowerCaseQuery)) ||
                    (request.getEmail() != null && request.getEmail().toLowerCase().contains(lowerCaseQuery)) ||
                    (request.getCategory() != null && request.getCategory().toLowerCase().contains(lowerCaseQuery)) ||
                    (request.getEventType() != null && request.getEventType().toLowerCase().contains(lowerCaseQuery))) {
                filteredRequests.add(request);
            }
        }

        adapter.clear();
        adapter.addAll(filteredRequests);
        adapter.notifyDataSetChanged();
    }
    private abstract class SimpleTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void afterTextChanged(Editable s) { }
    }
    private static final String[] COMPANY_NAMES = {"Company A", "Company B", "Company C"};
    private static final String[] OWNER_NAMES = {"John", "Jane", "Alex"};
    private static final String[] OWNER_SURNAMES = {"Doe", "Smith", "Johnson"};
    private static final String[] EMAIL_DOMAINS = {"example.com", "test.com", "sample.com"};
    private static final String[] CATEGORIES = {"Category1", "Category2", "Category3"};
    private static final String[] EVENT_TYPES = {"EventType1", "EventType2", "EventType3"};

    public static List<Request> generateRandomRequests(int count) {
        List<Request> requests = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < count; i++) {
            String id = UUID.randomUUID().toString();
            String userId = UUID.randomUUID().toString();
            String companyName = COMPANY_NAMES[random.nextInt(COMPANY_NAMES.length)];
            String ownerName = OWNER_NAMES[random.nextInt(OWNER_NAMES.length)];
            String ownerSurname = OWNER_SURNAMES[random.nextInt(OWNER_SURNAMES.length)];
            String email = ownerName.toLowerCase() + "." + ownerSurname.toLowerCase() + "@" + EMAIL_DOMAINS[random.nextInt(EMAIL_DOMAINS.length)];
            String category = CATEGORIES[random.nextInt(CATEGORIES.length)];
            String eventType = EVENT_TYPES[random.nextInt(EVENT_TYPES.length)];
            long requestTime = System.currentTimeMillis() - (random.nextInt(1000) * 60 * 60 * 24);
            boolean isAccepted = random.nextBoolean();

            Request request = new Request(id, userId, companyName, ownerName, ownerSurname, email, category, eventType, requestTime);
            requests.add(request);
        }

        return requests;
    }
}