package com.example.eventplanner_team24.requests;

import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.SendEmail;
import com.example.eventplanner_team24.category_manager.Service.CategoryService;
import com.example.eventplanner_team24.model.Request;
import com.google.firebase.firestore.FirebaseFirestore;

import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import java.util.ArrayList;
import java.util.Properties;

import javax.inject.Inject;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

public class RequestsAdapter extends ArrayAdapter<Request>{
    private ArrayList<Request> mRequests;
    private FragmentActivity mContext;
    @Inject
    CategoryService categoryService;

    public RequestsAdapter(FragmentActivity context, ArrayList<Request> requests){
        super(context, R.layout.item_request, requests);
        mContext = context;
        mRequests= requests;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) {
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_request, parent, false);
        }

        Request currentRequest = mRequests.get(position);

        TextView requestDetails = listItem.findViewById(R.id.requestDetail);
        String text = currentRequest.getOwnerName() + " " + currentRequest.getCompanyName();
        requestDetails.setText(text);

        Button acceptButton = listItem.findViewById(R.id.acceptButton);
        Button rejectButton = listItem.findViewById(R.id.rejectButton);

        acceptButton.setOnClickListener(v -> acceptRequest(currentRequest));
        rejectButton.setOnClickListener(v -> rejectRequest(currentRequest));

        listItem.setOnClickListener(v -> showRequestDialog(currentRequest));

        return listItem;
    }

    private void showRequestDialog(Request request) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = mContext.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.request_info_dialog, null);
        builder.setView(dialogView);

        TextView textCompanyName = dialogView.findViewById(R.id.textCompanyName);
        TextView textOwnerName = dialogView.findViewById(R.id.textOwnerName);
        TextView textEmail = dialogView.findViewById(R.id.textEmail);
        TextView textOwnerSurname = dialogView.findViewById(R.id.textOwnerSurname);
        TextView textCategory = dialogView.findViewById(R.id.textCategory);
        TextView textEventType = dialogView.findViewById(R.id.textEventType);
        TextView requestTime = dialogView.findViewById(R.id.textRequestTime);


        textCompanyName.setText("Company Name: " + request.getCompanyName());
        textOwnerName.setText("Owner Name: " + request.getOwnerName());
        textOwnerSurname.setText("Owner Surname: " + request.getOwnerSurname());
        textEmail.setText("Email: " + request.getEmail());
        textCategory.setText("Category: " + request.getCategory());
        textEventType.setText("Event Type: " + request.getEventType());
        requestTime.setText("Request Time: " + request.getRequestTime());


        builder.setPositiveButton("OK", (dialog, which) -> dialog.dismiss());
        builder.show();
    }


    private void acceptRequest(Request request) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("requests").document(request.getId()).update("status", "ACCEPTED")
                .addOnSuccessListener(aVoid -> {
                    mRequests.remove(request);
                    notifyDataSetChanged();
                });
    }

    private void rejectRequest(Request request) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = mContext.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.reject_request, null);
        builder.setView(dialogView);

        EditText reasonEditText = dialogView.findViewById(R.id.reasonEditText);

        builder.setPositiveButton("Reject", (dialog, which) -> {
            String reason = reasonEditText.getText().toString().trim();
            if (!reason.isEmpty()) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("requests").document(request.getId()).update("status", "REJECTED", "rejectReason", reason)
                        .addOnSuccessListener(aVoid -> {
                            new SendEmail(mContext, request.getEmail(), reason).execute();
                            mRequests.remove(request);
                            notifyDataSetChanged();
                        });
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    private void sendEmail(String recipientEmail, String reason) {
        final String username = "projekatbsep@gmail.com";
        final String password = "slobasalecoaanta";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail));
            message.setSubject("Your request has been rejected");
            message.setText("Dear User,\n\nYour request has been rejected. Reason: " + reason + "\n\nBest regards,\nYour App Team");

            Transport.send(message);

            Toast.makeText(mContext, "Email sent successfully", Toast.LENGTH_SHORT).show();

        } catch (MessagingException e) {
            Toast.makeText(mContext, "Failed to send email: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}
