package com.example.eventplanner_team24.category_manager.Data;

import android.view.View;

import com.example.eventplanner_team24.model.Category;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public interface CategoryCallback {

    void onCategoriesLoaded(List<Category> categories);
    void onCategoryLoaded(Category category);
    void onError(Exception e);
}
