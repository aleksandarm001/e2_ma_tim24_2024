package com.example.eventplanner_team24.category_manager.Service.ICategoryService;

import com.example.eventplanner_team24.category_manager.Data.CategoryCallback;
import com.example.eventplanner_team24.model.Category;

import java.util.List;

public interface ICategoryService {
    boolean saveCategory(Category category);
    void deleteCategory(Category category);
    void updateCategory(Category category);
    void getAllCategories(CategoryCallback categoryCallback);
}
