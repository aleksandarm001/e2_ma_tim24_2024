package com.example.eventplanner_team24.category_manager.Data.Infrastructure;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner_team24.category_manager.Data.CategoryCallback;
import com.example.eventplanner_team24.category_manager.Data.ICategoryRepository;
import com.example.eventplanner_team24.model.Category;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

public class CategoryRepository implements ICategoryRepository {

    private static final FirebaseFirestore db = FirebaseFirestore.getInstance();;


    @Inject
    public CategoryRepository() {}
    @Override
    public void getCategories(CategoryCallback callback) {
        List<Category> categories = new ArrayList<>();

        db.collection("categories").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Category category = Category.fromDocumentSnapshot(document);
                    categories.add(category);
                }
                callback.onCategoriesLoaded(categories);
            } else {
                Log.e("CategoryRepository", "Error getting categories", task.getException());
                callback.onError(task.getException());
            }
        });
    }

    @Override
    public boolean saveCategory(Category category) {
        try {
            Map<String, Object> categoryMap = new HashMap<>();
            if(category.getId() == null){
                categoryMap.put("name", category.getName());
                categoryMap.put("description", category.getDescription());

                db.collection("categories")
                        .add(categoryMap)
                        .addOnSuccessListener(documentReference -> {
                            Log.d("CategoryRepository", "Category added successfully with ID: " + documentReference.getId());

                            documentReference.update("id", documentReference.getId())
                                    .addOnSuccessListener(aVoid -> Log.d("CategoryRepository", "Category ID updated successfully"))
                                    .addOnFailureListener(e -> Log.e("CategoryRepository", "Error updating category ID", e));
                        })
                        .addOnFailureListener(e -> Log.e("CategoryRepository", "Error adding category", e));
            }else{
                Map<String, Object> updatedData = new HashMap<>();
                updatedData.put("name", category.getName());
                updatedData.put("description", category.getDescription());

                DocumentReference categoryRef = db.collection("categories").document(category.getId());

                categoryRef.update(updatedData)
                        .addOnSuccessListener(aVoid -> Log.d("CategoryRepository", "Category updated successfully"))
                        .addOnFailureListener(e -> Log.e("CategoryRepository", "Error updating category", e));
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @Override
    public void deleteCategory(Category category) {
        try {
            db.collection("categories")
                    .document(category.getId())
                    .delete()
                    .addOnSuccessListener(aVoid -> Log.d("CategoryRepository", "Category deleted successfully"))
                    .addOnFailureListener(e -> Log.e("CategoryRepository", "Error deleting category", e));
        } catch (Exception e) {
            Log.e("CategoryRepository", "Exception occurred while deleting category", e);
        }
    }

    @Override
    public void updateCategory(Category category) {
        try {
            db.collection("categories")
                    .document(category.getId().toString())
                    .set(category)
                    .addOnSuccessListener(aVoid -> Log.d("CategoryRepository", "Category updated successfully"))
                    .addOnFailureListener(e -> Log.e("CategoryRepository", "Error updating category", e));
        } catch (Exception e) {
            Log.e("CategoryRepository", "Exception occurred while updating category", e);
        }
    }




    public static void getAllCategories(CategoryRepository.CategoryFetchCallback callback) {
        ArrayList<Category> categories = new ArrayList<>();

        db.collection("categories")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Category category = document.toObject(Category.class);
                            categories.add((category));
                        }

                        callback.onCategoryFetch(categories);
                    } else {
                        Log.w("REZ_DB", "Error getting documents.", task.getException());
                        // Invoke the callback with null if an error occurs
                        callback.onCategoryFetch(null);
                    }
                });
    }



    public interface CategoryFetchCallback {
        default void onCategoryFetch(ArrayList<Category> categories) {

        }
        default void onCategoryByIdFetch(Category category) {

        }

    }
}


