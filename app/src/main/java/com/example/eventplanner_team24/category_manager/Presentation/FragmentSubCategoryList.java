package com.example.eventplanner_team24.category_manager.Presentation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner_team24.category_manager.Presentation.adapters.SubCategoryAdapter;
import com.example.eventplanner_team24.databinding.FragmentSubCategoryListBinding;
import com.example.eventplanner_team24.model.SubCategory;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentSubCategoryList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSubCategoryList extends Fragment {

    private static final String ARG_PARAM = "param";
    private ArrayList<SubCategory> mSubCategories;
    private FragmentSubCategoryListBinding binding;
    private SubCategoryAdapter adapter;


    public FragmentSubCategoryList() {
        // Required empty public constructor
    }


    public static FragmentSubCategoryList newInstance(ArrayList<SubCategory> subCategories) {
        FragmentSubCategoryList fragment = new FragmentSubCategoryList();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, subCategories);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSubCategories = getArguments().getParcelableArrayList(ARG_PARAM);
            adapter = new SubCategoryAdapter(getActivity(), mSubCategories);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = binding.inflate(inflater,container,false);
        View root = binding.getRoot();
        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }
}