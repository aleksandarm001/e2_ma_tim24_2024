package com.example.eventplanner_team24.category_manager.Presentation.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Presentation.FragmentCreateCategory;
import com.example.eventplanner_team24.category_manager.Service.CategoryService;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.model.Category;

import java.util.ArrayList;

import javax.inject.Inject;

public class CategoryAdapter extends ArrayAdapter<Category> {
    private ArrayList<Category> mCategories;
    private FragmentActivity mContext;
    @Inject
    CategoryService categoryService;

    public CategoryAdapter(FragmentActivity context, ArrayList<Category> categories){
        super(context, R.layout.category_card, categories);
        mContext = context;
        mCategories= categories;
        DaggerAppComponent.create().inject(this);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) {
            listItem = LayoutInflater.from(mContext).inflate(R.layout.category_card, parent, false);
        }

        Category currentCategory = mCategories.get(position);

        TextView categoryName = listItem.findViewById(R.id.category_title);
        categoryName.setText(currentCategory.getName());

        TextView categoryDescription = listItem.findViewById(R.id.category_description);
        categoryDescription.setText(currentCategory.getDescription());

        ImageView editIcon = listItem.findViewById(R.id.edit_icon);
        editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(FragmentCreateCategory.newInstance(currentCategory), mContext, false, R.id.category_manager_container);
            }
        });

        ImageView deleteIcon = listItem.findViewById(R.id.delete_icon);
        deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Delete Category")
                        .setMessage("Are you sure you want to delete this category?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mCategories.remove(currentCategory);
                                categoryService.deleteCategory(currentCategory);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });


        return listItem;
    }
}