package com.example.eventplanner_team24.category_manager.Data.Infrastructure;
import android.util.Log;

import com.example.eventplanner_team24.category_manager.Data.ICategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.ISubCategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.SubCategoryCallback;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class SubCategoryRepository implements ISubCategoryRepository{

    private static final FirebaseFirestore db = FirebaseFirestore.getInstance();;
    @Inject
    public SubCategoryRepository() {

    }

    @Override
    public void getSubCategories(SubCategoryCallback callback) {
        List<SubCategory> subCategories = new ArrayList<>();

        db.collection("sub_categories").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    SubCategory subCategory = SubCategory.fromDocumentSnapshot(document);
                    subCategories.add(subCategory);
                }
                callback.onSubCategoriesLoaded(subCategories);
            } else {
                Log.e("CategoryRepository", "Error getting categories", task.getException());
                callback.onError(task.getException());
            }
        });
    }

    @Override
    public void getSubCategoriesMap(SubCategoryCallback callback) {
        Map<String, SubCategory> subCategoriesMap = new HashMap<>();

        db.collection("sub_categories").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    SubCategory subCategory = SubCategory.fromDocumentSnapshot(document);
                    subCategoriesMap.put(subCategory.getName(), subCategory);
                }
                callback.onSubCategoriesLoaded(subCategoriesMap);
            } else {
                Log.e("CategoryRepository", "Error getting sub_categories", task.getException());
                callback.onError(task.getException());
            }
        });
    }



    @Override
    public boolean saveSubCategory(SubCategory subCategory) {
        try {
            Map<String, Object> subCategoryMap = new HashMap<>();
            subCategoryMap.put("name", subCategory.getName());
            subCategoryMap.put("description", subCategory.getDescription());
            subCategoryMap.put("category_id", subCategory.getCategory().getId());
            subCategoryMap.put("status", subCategory.getStatus().toString());

            db.collection("sub_categories")
                    .add(subCategoryMap)
                    .addOnSuccessListener(documentReference -> {
                        Log.d("SubCategoryRepository", "SubCategory added successfully with ID: " + documentReference.getId());

                        documentReference.update("id", documentReference.getId())
                                .addOnSuccessListener(aVoid -> Log.d("SubCategoryRepository", "Category ID updated successfully"))
                                .addOnFailureListener(e -> Log.e("SubCategoryRepository", "Error updating category ID", e));
                    })
                    .addOnFailureListener(e -> Log.e("SubCategoryRepository", "Error adding category", e));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void deleteSubCategory(SubCategory subCategory) {
        try {
            DocumentReference subCategoryRef = db.collection("sub_categories").document(subCategory.getId());

            subCategoryRef.delete()
                    .addOnSuccessListener(aVoid -> Log.d("SubCategoryRepository", "SubCategory deleted successfully"))
                    .addOnFailureListener(e -> Log.e("SubCategoryRepository", "Error deleting subcategory", e));

        } catch (Exception e) {
            Log.e("SubCategoryRepository", "Error deleting subcategory", e);
        }
    }

    @Override
    public void updateSubCategory(SubCategory subCategory) {
        try {
            Map<String, Object> subCategoryMap = new HashMap<>();
            subCategoryMap.put("name", subCategory.getName());
            subCategoryMap.put("description", subCategory.getDescription());
            subCategoryMap.put("category_id", subCategory.getCategory().getId());
            subCategoryMap.put("status", subCategory.getStatus().toString());
            DocumentReference subCategoryRef = db.collection("sub_categories").document(subCategory.getId());

            subCategoryRef.update(subCategoryMap)
                    .addOnSuccessListener(aVoid -> Log.d("SubCategoryRepository", "SubCategory updated successfully"))
                    .addOnFailureListener(e -> Log.e("SubCategoryRepository", "Error updating subcategory", e));

        } catch (Exception e) {
            Log.e("SubCategoryRepository", "Error updating subcategory", e);
        }
    }
    @Override
    public void getById(String id, SubCategoryCallback callback) {
        db.collection("sub_categories").document(id).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    SubCategory subCategory = SubCategory.fromDocumentSnapshot(document);
                    String categoryId = document.getString("category_id");
                    db.collection("categories").document(categoryId).get().addOnCompleteListener(categoryTask -> {
                        if (categoryTask.isSuccessful()) {
                            DocumentSnapshot categoryDoc = categoryTask.getResult();
                            if (categoryDoc.exists()) {
                                Category category = Category.fromDocumentSnapshot(categoryDoc);
                                subCategory.setCategory(category);
                            } else {
                                Log.e("SubCategoryRepository", "Category document not found for SubCategory with ID: " + id);
                            }
                            callback.onSubCategoryLoaded(subCategory);
                        } else {
                            Log.e("SubCategoryRepository", "Error getting category document", categoryTask.getException());
                            callback.onError(categoryTask.getException());
                        }
                    });
                } else {
                    Log.e("SubCategoryRepository", "SubCategory document not found with ID: " + id);
                    callback.onError(new Exception("SubCategory document not found with ID: " + id));
                }
            } else {
                Log.e("SubCategoryRepository", "Error getting subcategory document", task.getException());
                callback.onError(task.getException());
            }
        });
    }




    public static void getSubcategoriesByCategoryId(String categoryId, SubCategoryRepository.SubcategoryFetchCallback callback) {
        ArrayList<SubCategory> subcategories = new ArrayList<>();

        db.collection("sub_categories")
                .whereEqualTo("category_id", categoryId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            SubCategory subCategory = SubCategory.fromDocumentSnapshot(document);
                            subcategories.add(subCategory);
                        }

                        callback.onSubcategoryFetch(subcategories);
                    } else {
                        Log.w("REZ_DB", "Error getting documents.", task.getException());
                        // Invoke the callback with null if an error occurs
                        callback.onSubcategoryFetch(null);
                    }
                });
    }

    public static void getAllSubcategories(SubCategoryRepository.SubcategoryFetchCallback callback) {
        ArrayList<SubCategory> subcategories = new ArrayList<>();

        db.collection("sub_categories")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            SubCategory subCategory = SubCategory.fromDocumentSnapshot(document);
                            subcategories.add((subCategory));
                        }

                        callback.onSubcategoryFetch(subcategories);
                    } else {
                        Log.w("REZ_DB", "Error getting documents.", task.getException());
                        // Invoke the callback with null if an error occurs
                        callback.onSubcategoryFetch(null);
                    }
                });
    }


    public interface SubcategoryFetchCallback {
        default void onSubcategoryFetch(ArrayList<SubCategory> subcategories) {

        }
        default void onSubcategoryFetched(SubCategory subcategory, String errorMessage) {

        }
    }

}
