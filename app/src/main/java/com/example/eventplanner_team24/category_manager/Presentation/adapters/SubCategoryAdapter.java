package com.example.eventplanner_team24.category_manager.Presentation.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Presentation.FragmentCreateSubCategory;
import com.example.eventplanner_team24.category_manager.Service.SubCategoryService;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.model.SubCategory;

import java.util.ArrayList;

import javax.inject.Inject;

public class SubCategoryAdapter extends ArrayAdapter<SubCategory> {
    private ArrayList<SubCategory> mCategories;
    private FragmentActivity mContext;
    @Inject
    SubCategoryService subCategoryService;

    public SubCategoryAdapter(FragmentActivity context, ArrayList<SubCategory> categories){
        super(context, R.layout.sub_category_card, categories);
        mContext = context;
        mCategories= categories;
        DaggerAppComponent.create().inject(this);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) {
            listItem = LayoutInflater.from(mContext).inflate(R.layout.sub_category_card, parent, false);
        }

        SubCategory currentCategory = mCategories.get(position);

        TextView subCategoryName = listItem.findViewById(R.id.sub_category_title);
        subCategoryName.setText(currentCategory.getName());

        TextView subCategoryDescription = listItem.findViewById(R.id.sub_category_description);
        subCategoryDescription.setText(currentCategory.getDescription());

        TextView subCategoryType = listItem.findViewById(R.id.sub_category_type);
        subCategoryType.setText(currentCategory.getStatus().toString());

        ImageView editIcon = listItem.findViewById(R.id.edit_icon);
        editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(FragmentCreateSubCategory.newInstance(currentCategory),
                        mContext, false, R.id.category_manager_container);
            }
        });

        ImageView deleteIcon = listItem.findViewById(R.id.delete_icon);
        deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Delete Sub Category")
                        .setMessage("Are you sure you want to delete this sub category?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mCategories.remove(currentCategory);
                                subCategoryService.deleteSubCategory(currentCategory);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        return listItem;
    }
}
