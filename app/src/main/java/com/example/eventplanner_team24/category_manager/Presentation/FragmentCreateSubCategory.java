package com.example.eventplanner_team24.category_manager.Presentation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.CategoryCallback;
import com.example.eventplanner_team24.category_manager.Service.CategoryService;
import com.example.eventplanner_team24.category_manager.Service.SubCategoryService;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.databinding.FragmentCreateSubCategoryBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Notification;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCreateSubCategory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCreateSubCategory extends Fragment implements CategoryCallback {

    private static final String ARG_PARAM = "param";
    private SubCategory subCategory;
    private FragmentCreateSubCategoryBinding binding;
    private Map<String, Category> categoryMap = new HashMap<>();
    Spinner categorySpinner;
    private Category selectedCategory;
    @Inject
    SubCategoryService subCategoryService;
    @Inject
    CategoryService categoryService;

    public FragmentCreateSubCategory() {
        // Required empty public constructor
    }

    public static FragmentCreateSubCategory newInstance(SubCategory subCategory) {
        FragmentCreateSubCategory fragment = new FragmentCreateSubCategory();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, subCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subCategory = getArguments().getParcelable(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCreateSubCategoryBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        DaggerAppComponent.create().inject(this);
        categorySpinner = binding.categorySpinner;
        EditText nametextView = binding.edittextName;
        EditText description = binding.edittextDescription;
        RadioGroup radioGroup = binding.radioGroupType;
        if(subCategory != null){
            nametextView.setText(subCategory.getName());
            description.setText(subCategory.getDescription());
            if(subCategory.getStatus().equals(SubCategory.Status.SERVICE)){
                radioGroup.check(R.id.radioButtonService);
            } else {
                radioGroup.check(R.id.radioButtonProduct);
            }
        }
        fetchCategories();

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCategoryName = (String) parent.getItemAtPosition(position);
                selectedCategory = categoryMap.get(selectedCategoryName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        RadioGroup radioGroupType = binding.radioGroupType;
        AtomicInteger type = new AtomicInteger();

        radioGroupType.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radioButtonService){
                type.set(1);
            } else if (checkedId == R.id.radioButtonProduct){
                type.set(2);
            } else {
                return;
            }
        });

        binding.buttonSubmit.setOnClickListener(v -> {
            String name = nametextView.getText().toString();
            String desc = description.getText().toString();
            int status = type.get();
            boolean result;

            if(subCategory == null) {
                subCategory = new SubCategory(name, desc, selectedCategory, status);
                result = subCategoryService.saveSubCategory(subCategory);
            } else {
                subCategory.setName(name);
                subCategory.setDescription(desc);
                subCategory.setStatus(subCategory.getStatus(status));
                subCategory.setCategory(selectedCategory);
                subCategoryService.updateSubCategory(subCategory);
                result = true;
            }

            if(result) {
                Toast.makeText(getContext(), "SubCategory " +
                        (subCategory == null ? "added" : "updated")
                        + " successfully.", Toast.LENGTH_SHORT).show();
                if(subCategory != null){
                    Notification notification = new Notification("Sub Category change", "There was a sub category change ,check it out!", null, false, Notification.Role.PUPV);
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    db.collection("notifications").add(notification);
                }
                FragmentTransition.to(CategoryManagerFragment.newInstance(), getActivity(),
                        true, R.id.category_manager_container);
            }else {
                Toast.makeText(getContext(), "Saving failed!", Toast.LENGTH_SHORT).show();
                FragmentTransition.to(CategoryManagerFragment.newInstance(), getActivity(),
                        true, R.id.category_manager_container);
            }
        });

        return root;
    }

    private void setupCategorySpinner(List<String> categoryNames) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapter);
    }
    public void fetchCategories() {
        categoryService.getAllCategories(this);
    }
    @Override
    public void onCategoriesLoaded(List<Category> categories) {
        List<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
            categoryMap.put(category.getName(), category);
        }
        setupCategorySpinner(categoryNames);
    }

    @Override
    public void onCategoryLoaded(Category category) {

    }

    @Override
    public void onError(Exception e) {

    }
}