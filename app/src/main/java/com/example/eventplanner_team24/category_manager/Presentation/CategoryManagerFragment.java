package com.example.eventplanner_team24.category_manager.Presentation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Data.CategoryCallback;
import com.example.eventplanner_team24.category_manager.Data.SubCategoryCallback;
import com.example.eventplanner_team24.category_manager.Service.CategoryService;
import com.example.eventplanner_team24.category_manager.Service.SubCategoryService;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.databinding.FragmentCategoryManagerBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.SubCategory;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class CategoryManagerFragment extends Fragment
        implements CategoryCallback, SubCategoryCallback {

    private FragmentCategoryManagerBinding binding;
    private List<Category> categories = new ArrayList<>();
    private List<SubCategory> subCategories = new ArrayList<>();
    private static final String ARG_PARAM1 = "param1";
    private boolean categoriesLoaded = false;
    private boolean subCategoriesLoaded = false;
    private int selectedTabIndex = 0; // Added to store the selected tab index
    @Inject
    CategoryService categoryService;
    @Inject
    SubCategoryService subCategoryService;

    public CategoryManagerFragment() {
        // Required empty public constructor
    }

    public static CategoryManagerFragment newInstance() {
        CategoryManagerFragment fragment = new CategoryManagerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCategoryManagerBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        DaggerAppComponent.create().inject(this);
        fetchCategories();
        fetchSubCategories();

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTabIndex = tab.getPosition(); // Save selected tab index
                switch (selectedTabIndex) {
                    case 0:
                        if (categoriesLoaded && !categories.isEmpty()) {
                            performFragmentTransitionCategories();
                        } else {
                            Toast.makeText(getContext(), "Categories not loaded yet", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        if (subCategoriesLoaded && !subCategories.isEmpty()) {
                            performFragmentTransitionSubCategories();
                        } else {
                            Toast.makeText(getContext(), "Subcategories not loaded yet", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 2:
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // Unused, but required method
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // Unused, but required method
            }
        });

        binding.addCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (selectedTabIndex) {
                    case 0:
                        binding.searchCategoriesLayout.setVisibility(View.GONE);
                        FragmentTransition.to(FragmentCreateCategory.newInstance(null),
                                getActivity(), true, R.id.category_manager_container);
                        break;
                    case 1:
                        binding.searchCategoriesLayout.setVisibility(View.GONE);
                        FragmentTransition.to(FragmentCreateSubCategory.newInstance(null),
                                getActivity(), true, R.id.category_manager_container);
                        break;
                    case 2:
                        break;
                }
            }
        });
        return root;
    }

    @Override
    public void onStop() {
        super.onStop();
        binding.categoryManagerLayout.setVisibility(View.VISIBLE);
    }

    private void fetchCategories() {
        categoryService.getAllCategories(this);
    }

    private void fetchSubCategories() {
        subCategoryService.getSubCategories(this);
    }

    @Override
    public void onCategoriesLoaded(List<Category> categories) {
        this.categories.clear();
        this.categories.addAll(categories);
        this.categoriesLoaded = true;
        performFragmentTransitionCategories();
    }

    @Override
    public void onCategoryLoaded(Category category) {
        // Unused method
    }

    @Override
    public void onSubCategoriesLoaded(List<SubCategory> subCategories) {
        this.subCategories.clear();
        this.subCategories.addAll(subCategories);
        subCategoriesLoaded = true;
    }

    @Override
    public void onSubCategoriesLoaded(Map<String, SubCategory> subCategories) {
        // Unused method
    }

    @Override
    public void onSubCategoryLoaded(SubCategory subCategory) {
        // Unused method
    }

    @Override
    public void onError(Exception e) {
        // Unused method
    }

    private void performFragmentTransitionCategories() {
        if (categoriesLoaded) {
            FragmentTransition.to(FragmentCategoryList.newInstance((ArrayList<Category>) this.categories), getActivity(),
                    true, R.id.category_manager_container);
        } else {
            Toast.makeText(getContext(), "Failed to load categories", Toast.LENGTH_SHORT).show();
        }
    }

    private void performFragmentTransitionSubCategories() {
        if (subCategoriesLoaded) {
            FragmentTransition.to(FragmentSubCategoryList.newInstance((ArrayList<SubCategory>) this.subCategories), getActivity(),
                    false, R.id.category_manager_container);
        } else {
            Toast.makeText(getContext(), "Failed to load subcategories", Toast.LENGTH_SHORT).show();
        }
    }
}
