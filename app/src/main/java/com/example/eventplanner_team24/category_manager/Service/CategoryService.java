package com.example.eventplanner_team24.category_manager.Service;

import com.example.eventplanner_team24.category_manager.Data.CategoryCallback;
import com.example.eventplanner_team24.category_manager.Data.ICategoryRepository;
import com.example.eventplanner_team24.category_manager.Service.ICategoryService.ICategoryService;
import com.example.eventplanner_team24.model.Category;

import java.util.List;

import javax.inject.Inject;

public class CategoryService implements ICategoryService {
    private final ICategoryRepository categoryRepository;
    @Inject
    public CategoryService(ICategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public boolean saveCategory(Category category) {
        return categoryRepository.saveCategory(category);
    }

    @Override
    public void deleteCategory(Category category) {
        categoryRepository.deleteCategory(category);
    }

    @Override
    public void updateCategory(Category category) {
        categoryRepository.updateCategory(category);
    }

    @Override
    public void getAllCategories(CategoryCallback categoryCallback) {
        categoryRepository.getCategories(categoryCallback);
    }

}
