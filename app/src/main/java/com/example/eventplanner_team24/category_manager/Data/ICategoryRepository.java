package com.example.eventplanner_team24.category_manager.Data;

import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.model.Category;

import java.util.List;

public interface ICategoryRepository {
    void getCategories(CategoryCallback callback);
    boolean saveCategory(Category category);
    void deleteCategory(Category category);
    void updateCategory(Category category);
}
