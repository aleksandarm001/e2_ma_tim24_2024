package com.example.eventplanner_team24.category_manager.Data;

import com.example.eventplanner_team24.model.SubCategory;

public interface ISubCategoryRepository {
    void getSubCategories(SubCategoryCallback callback);
    void getSubCategoriesMap(SubCategoryCallback callback);
    boolean saveSubCategory(SubCategory category);
    void deleteSubCategory(SubCategory category);
    void updateSubCategory(SubCategory category);
    void getById(String id, SubCategoryCallback callback);
}
