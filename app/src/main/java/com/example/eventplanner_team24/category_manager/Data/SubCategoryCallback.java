package com.example.eventplanner_team24.category_manager.Data;

import com.example.eventplanner_team24.model.SubCategory;

import java.util.List;
import java.util.Map;

public interface SubCategoryCallback {
    void onSubCategoriesLoaded(List<SubCategory> subCategories);
    void onSubCategoriesLoaded(Map<String, SubCategory> subCategories);
    void onSubCategoryLoaded(SubCategory subCategory);
    void onError(Exception e);
}
