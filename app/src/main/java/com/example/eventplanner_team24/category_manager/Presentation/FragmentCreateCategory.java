package com.example.eventplanner_team24.category_manager.Presentation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner_team24.FragmentTransition;
import com.example.eventplanner_team24.R;
import com.example.eventplanner_team24.category_manager.Service.CategoryService;
import com.example.eventplanner_team24.components.DaggerAppComponent;
import com.example.eventplanner_team24.databinding.FragmentCreateCategoryBinding;
import com.example.eventplanner_team24.model.Category;
import com.example.eventplanner_team24.model.Notification;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.firebase.firestore.FirebaseFirestore;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCreateCategory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCreateCategory extends Fragment {

    private static final String ARG_PARAM = "param";
    private Category category;
    private FragmentCreateCategoryBinding binding;
    @Inject
    CategoryService categoryService;

    public FragmentCreateCategory() {
        // Required empty public constructor
    }

    public static FragmentCreateCategory newInstance(Category category) {
        FragmentCreateCategory fragment = new FragmentCreateCategory();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            category = getArguments().getParcelable(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCreateCategoryBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        EditText nametextView = binding.categoryName;
        EditText description = binding.categoryDescription;
        DaggerAppComponent.create().inject(this);
        if(category != null){
            nametextView.setText(category.getName());
            description.setText(category.getDescription());
        }

        binding.buttonSubmit.setOnClickListener(v -> {
            Category newCategory = new Category();
            if(category != null){
                newCategory.setName(nametextView.getText().toString());
                newCategory.setDescription(description.getText().toString());
                newCategory.setId(category.getId());
            }else{
                newCategory.setName(nametextView.getText().toString());
                newCategory.setDescription(description.getText().toString());
            }
            boolean result = categoryService.saveCategory(newCategory);
            if(result){
                Toast.makeText(getContext(), "Category added successfully.", Toast.LENGTH_SHORT).show();
                if(category != null){
                    Notification notification = new Notification("Category change", "There was a category change ,check it out!", null, false, Notification.Role.PUPV);
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    db.collection("notifications").add(notification);
                }
                FragmentTransition.to(CategoryManagerFragment.newInstance(), getActivity(),
                        true, R.id.category_manager_container);
            }else{
                Toast.makeText(getContext(), "Saving failed!", Toast.LENGTH_SHORT).show();
                FragmentTransition.to(CategoryManagerFragment.newInstance(), getActivity(),
                        true, R.id.category_manager_container);
            }
        });


        return root;
    }
}