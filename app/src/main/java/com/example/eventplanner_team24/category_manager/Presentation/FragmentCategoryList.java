package com.example.eventplanner_team24.category_manager.Presentation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner_team24.category_manager.Presentation.adapters.CategoryAdapter;
import com.example.eventplanner_team24.databinding.FragmentCategoryListBinding;
import com.example.eventplanner_team24.model.Category;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCategoryList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCategoryList extends Fragment {

    private static final String ARG_PARAM = "param";
    private ArrayList<Category> mCategories;
    private FragmentCategoryListBinding binding;
    private CategoryAdapter adapter;


    public FragmentCategoryList() {
        // Required empty public constructor
    }


    public static FragmentCategoryList newInstance(ArrayList<Category> categories) {
        FragmentCategoryList fragment = new FragmentCategoryList();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, categories);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategories = getArguments().getParcelableArrayList(ARG_PARAM);
            adapter = new CategoryAdapter(getActivity(), mCategories);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = binding.inflate(inflater,container,false);
        View root = binding.getRoot();
        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        return root;
    }
}