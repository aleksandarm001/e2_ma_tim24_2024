package com.example.eventplanner_team24.category_manager.Service;

import com.example.eventplanner_team24.category_manager.Data.ISubCategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.SubCategoryCallback;
import com.example.eventplanner_team24.category_manager.Service.ICategoryService.ISubCategoryService;
import com.example.eventplanner_team24.model.SubCategory;

import java.util.List;

import javax.inject.Inject;

public class SubCategoryService implements ISubCategoryService {
    private final ISubCategoryRepository subCategoryRepository;
    @Inject
    public SubCategoryService(ISubCategoryRepository subCategoryRepository) {
        this.subCategoryRepository = subCategoryRepository;
    }
    @Override
    public void getSubCategories(SubCategoryCallback callback){
        this.subCategoryRepository.getSubCategories(callback);
    }
    @Override
    public void getSubCategoriesMap(SubCategoryCallback callback){
        this.subCategoryRepository.getSubCategoriesMap(callback);
    }
    @Override
    public boolean saveSubCategory(SubCategory category) {
        return this.subCategoryRepository.saveSubCategory(category);
    }

    @Override
    public void deleteSubCategory(SubCategory category) {
        this.subCategoryRepository.deleteSubCategory(category);
    }

    @Override
    public void updateSubCategory(SubCategory category) {
        this.subCategoryRepository.updateSubCategory(category);
    }

    @Override
    public void getById(String id, SubCategoryCallback callback) {
        this.subCategoryRepository.getById(id, callback);
    }
}
