package com.example.eventplanner_team24.category_manager.Service.ICategoryService;

import com.example.eventplanner_team24.category_manager.Data.SubCategoryCallback;
import com.example.eventplanner_team24.model.SubCategory;

import java.util.List;

public interface ISubCategoryService {
    void getSubCategories(SubCategoryCallback callback);
    void getSubCategoriesMap(SubCategoryCallback callback);
    boolean saveSubCategory(SubCategory category);
    void deleteSubCategory(SubCategory category);
    void updateSubCategory(SubCategory category);
    void getById(String id, SubCategoryCallback callback);
}
