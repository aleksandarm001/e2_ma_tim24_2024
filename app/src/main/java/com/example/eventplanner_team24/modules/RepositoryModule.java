package com.example.eventplanner_team24.modules;

import com.example.eventplanner_team24.authentication.Data.Infrastructure.CompanyRepository;
import com.example.eventplanner_team24.authentication.Data.Infrastructure.UserRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.SubCategoryCallback;
import com.example.eventplanner_team24.event_manager.Data.Infrastructure.EventRepository;
import com.example.eventplanner_team24.model.Category;
import com.google.firebase.firestore.FirebaseFirestore;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {
    @Provides
    public FirebaseFirestore provideFirebaseFirestore() {
        return FirebaseFirestore.getInstance();
    }
    @Provides
    public UserRepository provideUserRepository() {
        return new UserRepository();
    }
    @Provides
    public CompanyRepository provideCompanyRepository() {
        return new CompanyRepository();
    }
    @Provides
    public CategoryRepository provideCategoryRepository() {
        return new CategoryRepository();
    }
    @Provides
    public SubCategoryRepository provideSubCategoryRepository() {
        return new SubCategoryRepository();
    }
    @Provides
    public EventRepository provideEventRepository() {
        return new EventRepository();
    }
}