package com.example.eventplanner_team24.modules;

import com.example.eventplanner_team24.authentication.Data.ICompanyRepository;
import com.example.eventplanner_team24.authentication.Data.Infrastructure.CompanyRepository;
import com.example.eventplanner_team24.authentication.Data.Infrastructure.UserRepository;
import com.example.eventplanner_team24.authentication.Service.CompanyService;
import com.example.eventplanner_team24.authentication.Service.UserService;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.CategoryRepository;
import com.example.eventplanner_team24.category_manager.Data.Infrastructure.SubCategoryRepository;
import com.example.eventplanner_team24.category_manager.Service.CategoryService;
import com.example.eventplanner_team24.category_manager.Service.SubCategoryService;
import com.example.eventplanner_team24.event_manager.Data.Infrastructure.EventRepository;
import com.example.eventplanner_team24.event_manager.Service.EventService;
import com.example.eventplanner_team24.model.Category;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {
    @Provides
    public CompanyService provideCompanyService(CompanyRepository companyRepository) {
        return new CompanyService(companyRepository);
    }
    @Provides
    public UserService provideUserService(UserRepository userRepository) {
        return new UserService(userRepository);
    }
    @Provides
    public CategoryService provideCategoryService(CategoryRepository categoryRepository) {
        return new CategoryService(categoryRepository);
    }
    @Provides
    public SubCategoryService provideSubCategoryService(SubCategoryRepository subCategoryRepository) {
        return new SubCategoryService(subCategoryRepository);
    }
    @Provides
    public EventService provideEventService(EventRepository eventRepository) {
        return new EventService(eventRepository);
    }

}
